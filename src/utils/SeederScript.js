const directAffilliaiteModel = require("./../models/partnershipModel/DirectAffiliationsModel");
const partnershipAffiliateModel = require("./../models/partnershipModel/PartnershipAffiliationsModel");
const agentSupportingModel = require("./../models/AgentsSupportingModel");
const requestAccessModel = require("./../models/RequestingAccessAdmin");
const { BadRequestError } = require("../../lib/appErrors");

exports.seedPartenersIn = async (partnerID, universityID, user) => {
  /**
   * @DESC  check if data exist with the id and university
   */
  let directAffilliaite;
  let partnershipAffiliate;

  directAffilliaite = await directAffilliaiteModel.findOne({
    agentId: partnerID,
    university: universityID,
  });

  partnershipAffiliate = await partnershipAffiliateModel.findOne({
    agentId: user._id,
    partnershipAgentID: partnerID,
  });

  if (!directAffilliaite) {
    directAffilliaite = new directAffilliaiteModel({
      agentId: partnerID,
      university: universityID,
    });
  }

  if (!partnershipAffiliate) {
    partnershipAffiliate = new partnershipAffiliateModel({
      agentId: user._id,
      partnershipAgentID: partnerID,
    });
  }

  await directAffilliaite.save();
  await partnershipAffiliate.save();
};

exports.SeedAgentSupport = async (agentID, universityID, affiliations) => {
  let data;
  let commissionType;

  if (
    affiliations.universityPaymentMethod.isFixed &&
    affiliations.universityPaymentMethod.agentPayout.isFixed
  ) {
    // check if that fixed payout for agent is less than university fixed amount commission
    if (
      affiliations.universityPaymentMethod.received <=
      affiliations.universityPaymentMethod.agentPayout.payout
    )
      throw new BadRequestError(
        "You cannot set agent commission for this University above yours."
      );

    affiliations.commission =
      affiliations.universityPaymentMethod.agentPayout.payout;

    commissionType = "fixed";
    data = { ...affiliations, agentID, universityID };
  }

  if (
    affiliations.universityPaymentMethod.isFixed &&
    !affiliations.universityPaymentMethod.agentPayout.isFixed
  ) {
    // I neeed know the percentage set for the agent. then calculate the percentage for
    // the agent, come add am for the commissions wey dey intop
    if (
      affiliations.universityPaymentMethod.agentPayout.payout >= 100
    )
      throw new BadRequestError(
        "percentage figure cannot be equal to 100 or above 100"
      );
    percentagDecimal =
      affiliations.universityPaymentMethod.agentPayout.payout / 100;
    affiliations.commission =
      percentagDecimal *
      affiliations.universityPaymentMethod.received;
    commissionType = "fixed";
    data = { ...affiliations, agentID, universityID };
  }

  if (
    !affiliations.universityPaymentMethod.isFixed &&
    affiliations.universityPaymentMethod.agentPayout.isFixed
  ) {
    // calculate university percentage then the result, calculate the agent percentage

    affiliations.tuitionFees = 0;

    if (affiliations.universityPaymentMethod.received >= 100)
      throw new BadRequestError(
        "percentage figure cannot be equal to 100 or above 100"
      );

    affiliations.commission =
      affiliations.universityPaymentMethod.agentPayout.payout;
    commissionType = "fixed";
    data = { ...affiliations, agentID, universityID };
  }

  if (
    !affiliations.universityPaymentMethod.isFixed &&
    !affiliations.universityPaymentMethod.agentPayout.isFixed
  ) {
    if (affiliations.universityPaymentMethod.received >= 100)
      throw new BadRequestError(
        "percentage figure cannot be equal to 100 or above 100"
      );

    if (
      affiliations.universityPaymentMethod.agentPayout.payout >= 100
    )
      throw new BadRequestError(
        "percentage figure cannot be equal to 100 or above 100"
      );

    affiliations.commission = affiliations.universityPaymentMethod.agentPayout.payout
    commissionType = "percentage";
    data = { ...affiliations, agentID, universityID };
  }

  const directAffilData = {
    commissionType,
    agentId: agentID,
    commissions: data.commission,
    university: universityID,
  };
  // check if university is already in created
  let createDirectAffil = await directAffilliaiteModel.findOne({
    agentId: agentID,
    university: universityID,
  });

  if (!createDirectAffil) {
    createDirectAffil = new directAffilliaiteModel(directAffilData);
    await createDirectAffil.save();
  } else {
    createDirectAffil.commissionType = commissionType;
    createDirectAffil.commissions = data.commission;
    await createDirectAffil.save();
  }
  // check if support agent is existing
  let newAgent;
  newAgent = await agentSupportingModel.findOneAndUpdate(
    {
      agentID,
      universityID,
    },
    data
  );

  if (!newAgent) {
    newAgent = new agentSupportingModel(data);
    await newAgent.save();
  }

  return newAgent;
};

exports.requestUniversityAccess = async (agentID, universityArrayIDs) => {
  const isExisting = await requestAccessModel.findOne({
    agentRequesting: agentID,
  });

  if (isExisting) {
    for (let uni of universityArrayIDs) {
      const uniIndex = isExisting.universities.findIndex(
        (univ) => String(univ.university) === String(uni.university)
      );

      if (uniIndex === -1) {
        isExisting.universities.push(uni);
      }
    }
    await isExisting.save();
  } else {
    const data = {
      agentRequesting: agentID,
      universities: universityArrayIDs,
    };
    const newAgent = new requestAccessModel(data);
    await newAgent.save();
  }
};

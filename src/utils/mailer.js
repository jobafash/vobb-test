const { sendPersonlizedEmail, requestMail } = require("./MaijetTemplate");

const verificationMail =  (emailData) => {
  sendPersonlizedEmail(emailData);
}

const passwordResetMail =  (emailData) => {
  sendPersonlizedEmail(emailData);
}
const newsletterMail =  (emailData) => {
  sendPersonlizedEmail(emailData);
}

const partnershipRequestMail =  (emailData) => {
  sendPersonlizedEmail(emailData);
}

const requestUpdateMail =  (emailData) => {
  sendPersonlizedEmail(emailData);
}
const invitationLink = (emailData) => {
  sendPersonlizedEmail(emailData)
}
const requestAccessMail =  (emailData) => {
  requestMail(emailData);
}
const requestSuccessMail =  (emailData) => {
  requestMail(emailData);
}
const applicationStatusUpdate = (emailData) => {
  sendPersonlizedEmail(emailData)
}
const newApplication = (emailData) => {
  sendPersonlizedEmail(emailData)
}
const apologymail = (emailData)=> {
  sendPersonlizedEmail(emailData)
}


module.exports = {
  verificationMail,
  passwordResetMail,
  newsletterMail,
  partnershipRequestMail,
  requestUpdateMail,
  requestAccessMail,
  requestSuccessMail,
  invitationLink,
  newApplication,
  applicationStatusUpdate,
  apologymail
};

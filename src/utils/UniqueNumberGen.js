const generateCode = (length) => {
  const alphanNumeric = "abcdefghijklmnopqrstuvwxyz1234567890";
  let result = "";

  for (let i = 0; i < length; i++) {
    result += alphanNumeric.charAt(
      Math.floor(Math.random() * alphanNumeric.length)
    );
  }

  return result;
};

module.exports = { generateCode };

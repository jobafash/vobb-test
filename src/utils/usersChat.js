const users =[]

const addUser = (userID, socketID)=> {
    !users.some(user=>user.userID === userID) &&
        users.push({userID, socketID})

        return users
}

const removeUser = (socketID) => {
    users = users.filter(user=>user.socketID !== socketID)

    return users
}

const getUser = (userID)=> {
    return users.find(user=> user.userID === userID)
}

module.exports = {
    addUser,
    removeUser,
    getUser
}
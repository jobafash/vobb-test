const mailjet = require("node-mailjet").connect(
  process.env.MAILJET_PUBLIC_KEY,
  process.env.MAILJET_PRIVATE_KEY
);

function sendPersonlizedEmail(emailData) {
  const request = mailjet.post("send", { version: "v3.1" }).request({
    Messages: [
      {
        From: {
          Email: process.env.EMAIL_SENDER_EMAIL,
          Name: process.env.EMAIL_SENDER,
        },
        To: [
          {
            Email: emailData.email,
          },
        ],
        TemplateID: Number(emailData.templateId),
        TemplateLanguage: true,
        Subject: emailData.subject,
        Variables: emailData.variableData,
      },
    ],
  });
  request
    .then((result) => {
      console.log("Email sent successfully");
    })
    .catch((err) => {
      console.log(err);
    });
}

function requestMail(emailData) {
  const request = mailjet.post("send", { version: "v3.1" }).request({
    Messages: [
      {
        From: {
          Email: process.env.EMAIL_SENDER_EMAIL,
          Name: process.env.EMAIL_SENDER,
        },
        To: [
          {
            Email: emailData.email,
          },
        ],
        TemplateID: Number(emailData.templateId),
        TemplateLanguage: true,
        Subject: emailData.subject,
        Variables: emailData.variableData,
      },
    ],
  });
  request
    .then((result) => {
      console.log("Done successfully");
    })
    .catch((err) => {
      console.log(err);
    });
}

module.exports = { sendPersonlizedEmail, requestMail };

const express = require("express");
const router = require("express").Router();
const StripeController = require("./../controllers/stripe/");
const multer = require("./../../lib/multer");
const { auth } = require("./../middlewares/Auth");

module.exports = () => {
  router.get("/supported-countries", StripeController.supportedCountries)
  router.post(
    "/agent/billing/create-user",
    auth,
    StripeController.createCustomerAccount
  );
  router.post(
    "/agent/billing/billing-details",
    auth,
    StripeController.addBillingDetails
  );
  router.post(
    "/agent/billing/payout/:invoiceID",
    auth,
    multer.upload.single("prove"),
    StripeController.agentPayout
  );

  // webhooks for stripe
  router.post(
    "/webhook/payment-successful",
    express.raw({ type: "application/json" }),
    StripeController.webhookSuccessfuPayment
  );

  router.get("/agent/sending/billing-history", auth, StripeController.billingHistory)
  return router;
};

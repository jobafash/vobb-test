const router = require("express").Router();
const di = require("../../di");
const adminController = di.get("adminController");
const { auth, admin } = require("./../middlewares/Auth");

module.exports = () => {
  router.get("/admin/requests", auth, admin, (req, res) =>
    adminController.getRequests(req, res)
  );
  router.get("/admin/support/messages", auth, admin, (req, res) =>
    adminController.getMessages(req, res)
  );
  router.patch("/admin/support/message/:id/update", auth, admin, (req, res) =>
    adminController.updateMessage(req, res)
  );
  router.get("/admin/support/message/:id", auth, admin, (req, res) =>
    adminController.getMessage(req, res)
  );
  router.put("/admin/verification", auth, admin, (req, res) =>
    adminController.verifyAgent(req, res)
  );
  router.patch("/admin/universities/update", auth, admin, (req, res) =>
    adminController.updateUniversities(req, res)
  );
  router.get("/admin/agents/verified", auth, admin, (req, res) =>
    adminController.fetchedVerifiedAgents(req, res)
  );
  router.get("/admin/agents/unverified", auth, admin, (req, res) =>
    adminController.fetchUnverifiedAgents(req, res)
  );
  router.delete("/admin/delete/agent/:agentID", auth, admin, (req, res) =>
    adminController.deleteAgent(req, res)
  );
  return router;
};

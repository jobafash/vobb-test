const router = require("express").Router();
const di = require("../../di");
const partnershipController = di.get("partnershipController");
const multer = require("./../../lib/multer");
const { auth } = require("./../middlewares/Auth");
const joiValidator = require("./../validators/index");
const {
  newPartnershipValidation,
  updatePartnershipValidation,
} = require("./../validators/partnershipValidatorSchema");

module.exports = () => {
  router.post(
    "/partnerships/new",
    joiValidator(newPartnershipValidation),
    auth,
    (req, res) => partnershipController.create(req, res)
  );
  router.put(
    "/partnerships/:id/update",
    joiValidator(updatePartnershipValidation),
    auth,
    (req, res) => partnershipController.update(req, res)
  );
  router.get("/partnerships/all", auth, (req, res) =>
    partnershipController.get(req, res)
  );
  router.get("/agent/parntership/search", (req, res) =>
    partnershipController.searchUniversityByCountry(req, res)
  );
  router.get("/agent/partnership/incoming", auth, (req, res) =>
    partnershipController.incomingPartnership(req, res)
  );
  router.get("/agent/partnership/pending", auth, (req, res) =>
    partnershipController.pendingPartnership(req, res)
  );
  router.get("/agent/university/agents/:universityID", auth, (req, res) =>
    partnershipController.getAgentsInUniversity(req, res)
  );
  router.get("/partnerships/universities", auth, (req, res) =>
    partnershipController.getUniversities(req, res)
  );
  router.get("/partnerships/university/:id", auth, (req, res) =>
    partnershipController.getSingleUniversity(req, res)
  );
  router.get("/partnerships/:id", auth, (req, res) =>
    partnershipController.getSingle(req, res)
  );

  router.get("/user/direct_partners", auth, (req, res) =>
    partnershipController.getDirectAffiliates(req, res)
  );

  router.get("/user/parterned_agents", auth, (req, res) =>
    partnershipController.getPartneredAffiliations(req, res)
  );

  router.delete("/user/delete/agent", auth, (req, res) =>
    partnershipController.deletePartnerAffill(req, res)
  );

  router.patch("/user/update/commission", auth, (req, res) =>
    partnershipController.updateCommissions(req, res)
  );
  router.get("/agent/direct_partners/profile/:partnerID", auth, (req, res) =>
    partnershipController.getAgentProfile(req, res)
  );
  router.get("/agent/country/list", (req, res) =>
    partnershipController.listOfCountries(req, res)
  );
  router.post(
    "/agent/partner/invite",
    multer.upload.array("csv"),
    auth,
    (req, res) => {
      partnershipController.sendpartnerAlink(req, res);
    }
  );
  
  return router;
};

const agenterRouter = require("./AgentRouter");
const UniversityRouter = require("./UniversityRouter");
const StudentRouter = require("./StudentRouter");
const NewsletterRouter = require("./NewsletterRoute");
const preLaunchAgents = require("./PreLaunchRouter");
const AdminRouter = require("./AdminRouter");
const ApplicationRouter = require("./ApplicationRouter");
const CommissionRouter = require("./CommissionRouter");
const UserRouter = require("./UserRouter");
const ReviewRouter = require("./ReviewRouter");
const PartnershipRouter = require("./PartnershipRouter");
const ConversationRouter = require("./Chats/ConversationRouter");
const MessageRouter = require("./Chats/MessageRouter")
const HelpDeskRouter = require("./Chats/HelpDeskRoute")
const BillingRouter = require("./BillingRoute")
const InvoiceRouter = require("./InvoiceRouter")

module.exports = (router) => {
  router.use(agenterRouter());
  router.use(UniversityRouter());
  router.use(StudentRouter());
  router.use(NewsletterRouter());
  router.use(preLaunchAgents());
  router.use(AdminRouter());
  router.use(ApplicationRouter());
  router.use(CommissionRouter());
  router.use(UserRouter());
  router.use(ReviewRouter());
  router.use(PartnershipRouter());
  router.use(ConversationRouter());
  router.use(MessageRouter())
  router.use(HelpDeskRouter())
  router.use(BillingRouter())
  router.use(InvoiceRouter())
  return router;
};

const router = require("express").Router();
const agentController = require("./../controllers/AgentController");
const joiValidator = require("./../validators/index");
const xcelUploader = require("./../controllers/Xcelsheet");
const multer = require("./../../lib/multer");
const { auth } = require("./../middlewares/Auth");
const {
  RequestAccessValidationSchema,
  AgentAffiliationWIthUniversities,
} = require("./../validators/AgentValidatorSchema");

module.exports = () => {
  router.post(
    "/agent/new",
    joiValidator(RequestAccessValidationSchema),
    agentController.createAgent
  );
  router.post("/auth/verify-account", agentController.validateAgentToken);
  router.post("/auth/login", agentController.loginVerifiedAgent);
  router.post(
    "/agent/register-university",
    // joiValidator(AgentAffiliationWIthUniversities),
    auth,
    agentController.RegisterAgentUniversityAffiliations
  );
  router.patch(
    "/agent/update/university_commission",
    auth,
    agentController.updateUniversityCommission
  );
  router.get(
    "/vobb/universities",
    agentController.getAllUniversitiesForRegistration
  );
  router.post(
    "/xxcelsheet/upload",
    multer.upload.single("file"),
    xcelUploader.redefineXcell
  );
  router.get("/agents/navbar/statistics", auth, agentController.statisticsNav)

  return router;
};

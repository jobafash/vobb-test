const router = require("express").Router();
const universityController = require("./../controllers/UniversityController");
const joiValidator = require("./../validators/index");
const { auth } = require("./../middlewares/Auth");
const {
    UniversityValidation,
    UniversityLoginValidation,
    ForgotPasswordValidation,
    UniversityUpdateValidation,
    PasswordValidateSchema
} = require("./../validators/UniversityValidatorSchema");

module.exports = () => {
  router.get("/universities", universityController.fetchAllUniversities);
  router.get("universities/agents/approved", universityController.fetchApprovedAgents);
 
  return router;
};

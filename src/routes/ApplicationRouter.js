const router = require("express").Router();
const di = require("../../di");
const applicationController = di.get("applicationController");
const multer = require("./../../lib/multer")
const joiValidator = require("./../validators/index");
const { auth } = require("./../middlewares/Auth");
const {
  newApplication,
  updateApplication,
} = require("./../validators/applicationValidator");
const { upload } = require("./../utils/uploader");

module.exports = () => {
  router.post("/applications/new", auth, multer.upload.array("files", 5), (req, res) =>
    applicationController.create(req, res)
  );
  router.get("/applications/available-universities", auth, (req, res) =>
    applicationController.getUniversities(req, res)
  );
  router.get("/applications", auth, (req, res) =>
    applicationController.get(req, res)
  );
  router.get("/application/:id", auth, (req, res) =>
    applicationController.getSingle(req, res)
  );
  router.patch(
    "/applications/:id/update",
    joiValidator(updateApplication),
    auth,
    (req, res) => applicationController.update(req, res)
  );
  router.get("/agent/application/search", auth, (req, res) =>
    applicationController.searchApplication(req, res)
  );
  return router;
};
//joiValidator(newApplication),

const router = require("express").Router();
const preLaunchController = require("../controllers/PreLaunchAgentController");
const preLaunchController2 = require("../controllers/PreLaunchAgentControllerCopy")
const multer = require("../../lib/multer");
module.exports = () => {
  router.post(
    "/agents/pre-launch",
    multer.upload.single("file"),
    preLaunchController.CreatePrelaunchAgents
  );
  // router.post(
  //   "/agents/pre-launch2",
  //   multer.upload.single("file"),
  //   preLaunchController2.CreatePrelaunchAgents
  // );
  router.get(
    "/agents/fetch-prelunched",
    preLaunchController.fetchAllPreLunchedAgents
  ); 
  router.post("/sendMessage", preLaunchController.sendSms)

  router.post("/send/campaign", preLaunchController.SendCampaignEmails)
  return router;
};

const router = require("express").Router();
const InvoiceController = require("./../controllers/invoices/")
const { auth } = require("./../middlewares/Auth");

module.exports = () => {
  router.post("/agent/invoices/generate", auth, InvoiceController.generateInvoice);
  router.get("/agent/invoice/type", auth, InvoiceController.unprocessedInvoice)
  router.patch("/agent/invoice/edit/:invoiceID", auth, InvoiceController.editInvoice)
  
  return router;
};

const router = require("express").Router();
const newsletterController = require("./../controllers/NewsletterControlller");

module.exports = () => {
  router.post("/agents/subscribe", newsletterController.createNewsletter);
  return router;
};

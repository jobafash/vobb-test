const router = require("express").Router();
const studentController = require("./../controllers/StudentController");
const scholarshipListController = require("./../controllers/ScholarshipListController");
const {
  studentProfileUpdate,
  PasswordValidateSchema,
} = require("../validators/StudentProfileValidator");
const joiValidator = require("./../validators/index");
const { protect } = require("./../middlewares/Auth");
const { auth } = require("./../middlewares/Auth");
const { upload } = require("./../utils/uploader");
const {
  AgentReviewSchema,
} = require("./../validators/AgentReviewValidationSchema");

module.exports = () => {
  // router.post(
  //   "/student/avatar",
  //   protect,
  //   upload.single("avatar"),
  //   studentController.profilePic
  // );
  // router.get("/student/:id/avatar", studentController.getProfilePics);
  // router.delete(
  //   "/student/avatar/delete",
  //   protect,
  //   studentController.deleteProfilePics
  // );
  // router.post("/auth/student-signup/", studentController.createStudent);
  // router.get("/verification/success", VerificationController.verifyUser);
  // router.post("/auth/forgot-password", passwordController.forgotPassword);
  // router.post("/auth/reset-password", passwordController.resetPassword);
  router.get("/agents", studentController.getAgentsByCountry);
  router.get("/agents/top", 
    //protect, 
    studentController.getAgentsByRating
  );
  // router.patch(
  //   "/students/update-profile",
  //   joiValidator(studentProfileUpdate),
  //   protect,
  //   studentController.updateProfile
  // );
  // router.patch(
  //   "/auth/student/update-password",
  //   protect,
  //   joiValidator(PasswordValidateSchema),
  //   studentController.updatePassword
  // );
  // router.get("/student/me", protect, studentController.getMyprofile);
  // router.delete("/auth/delete/me", protect, studentController.deleteStudent);
  // router.post("/student/save-agent", protect, studentController.saveAgent);
  // router.get(
  //   "/student/get/saved-agents",
  //   protect,
  //   studentController.fetchAllSavedAgents
  // );
  // router.delete(
  //   "/stuednt/unsave-agent",
  //   protect,
  //   studentController.unsaveAgent
  // );
  // router.get("/student/get-agent/:id", protect, studentController.fetchAnAGent);
  router.get("/agents/search", studentController.searchForAgent);
  router.post("/student/support", auth, studentController.supportMessage);
  router.post(
    "/student/scholar-list",
    auth,
    scholarshipListController.createScholarship
  );
  router.get(
    "/student/scholarships",
    auth,
    scholarshipListController.getScholarships
  );
  router.post(
    "/report/new",
    auth,
    upload.single("reportContent"),
    studentController.createReport
  );
  router.get("/report/all", auth, studentController.fetchAllReports);
//   router.post(
//     "/student/agent-review",
//     joiValidator(AgentReviewSchema),
//     protect,
//     studentController.leaveAreview
//   );
// router.get("/student/fetch-Review", protect, studentController.fetchAllAgentReviews)

//   router.post("/dummy-insertion", dummyController.dummySeeding);
//   router.post(
//     "/dummy-profile-pics",
//     upload.single("avatar"),
//     dummyController.dummyProfilePics
//   );

//   router.get('/users', userController.allowIfLoggedin, userController.grantAccess('readAny', 'profile'), userController.getUsers);
 
//   router.put('/user/:userId', userController.allowIfLoggedin, userController.grantAccess('updateAny', 'profile'), userController.updateUser);
 
//   router.delete('/user/:userId', userController.allowIfLoggedin, userController.grantAccess('deleteAny', 'profile'), userController.deleteUser);
  return router;
}
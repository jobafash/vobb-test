const router = require("express").Router();
const userController = require("./../controllers/UserController");
const agentController = require("./../controllers/AgentController");
const joiValidator = require("./../validators/index");
const { auth } = require("./../middlewares/Auth");
const {
  AgentForgotPasswordValidation,
  AgentlLoginValidation,
  PasswordValidateSchema,
} = require("./../validators/AgentValidatorSchema");
const { upload } = require("./../utils/uploader");

module.exports = () => {
  //Auth
  router.post(
    "/auth/:userType/new",
    upload.single("avatar"),
    userController.createUser
  );
  router.post("/auth/:userType/resend-mail", userController.resendEmail);
  router.post(
    "/auth/forgot_password",
    joiValidator(AgentForgotPasswordValidation),
    userController.forgotPassword
  );
  router.patch("/auth/reset-password", userController.resetPassword);
  router.patch(
    "/auth/update-password",
    auth,
    joiValidator(PasswordValidateSchema),
    userController.updatePassword
  );
  //Users
  router.patch(
    "/update-profile",
    upload.single("avatar"),
    upload.single("coverPhoto"),
    auth,
    userController.updateProfile
  );
  router.get("/profile", auth, userController.viewProfile);
  router.get("/agent/:public_link", auth, agentController.getAgentByLink);
  router.get("/agent/stats/all", agentController.getAgentStats);
  router.patch("/agent/stats", auth, agentController.updateAgentStats);
  router.delete("/avatar/delete", auth, userController.deleteProfilePics);
  router.post(
    "/avatar/new",
    upload.single("avatar"),
    auth,
    userController.createAvatar
  );
  router.post("/auth/token", auth, userController.checkToken);

  router.get("/saved-agents", auth, userController.fetchsavedAgents);
  router.post("/save/agent", auth, userController.saveAgent);
  router.put("/unsave/agent", auth, userController.unsaveAgent);
  //Support and Delete
  router.post("/support", userController.supportMessage);
  router.delete("/delete", auth, userController.deleteUser);
  router.get(
    "/user/dashboard/statistics",
    auth,
    userController.agentDashBoardStatistics
  );
  router.get(
    "/user/dashboard/applications",
    auth,
    userController.getEightApplication
  );
  return router;
};

const router = require("express").Router();
const reviewController = require("./../controllers/ReviewController");
const { auth } = require("./../middlewares/Auth");

module.exports = () => {
    //Review
    router.post(
        "/review/new",
        auth,
        reviewController.create
    );
    router.put(
        "/review/:id/update",
        auth,
        reviewController.update
    );
    router.get("/reviews/all", auth, reviewController.get);
    router.get(
        "/reviews/:id/all",
        auth,
        reviewController.getByUser
    );
    router.get(
        "/review/:id",
        auth,
        reviewController.getSingle
    );
    router.delete(
        "/review/:id",
        auth,
        reviewController.deleteSingle
    );
  return router;
};

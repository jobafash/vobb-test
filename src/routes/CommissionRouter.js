const router = require("express").Router();
const di = require("../../di");
const commissionController = di.get("commissionController");
const { auth } = require("./../middlewares/Auth");

module.exports = () => {
  router.put("/commissions/:id/update", auth, (req, res) =>
    commissionController.update(req, res)
  );
  router.get("/commissions/all", auth, (req, res) =>
    commissionController.get(req, res)
  );
  router.get("/commissions/:id", auth, (req, res) =>
    commissionController.getSingle(req, res)
  );
  router.get("/agent/commissions/search", auth, (req, res) =>
    commissionController.searchAndFilter(req, res)
  );
  router.patch("/agent/commission-update/:commissionID", auth, (req, res) =>
    commissionController.updateCommissions(req, res)
  );
  return router;
};

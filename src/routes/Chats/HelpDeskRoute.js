const router = require("express").Router();
const helpDeskController = require("../../controllers/chat/HelpDeskController");

module.exports = () => {
  router.get("/helpdesk/create-agent", helpDeskController.createUser);
  router.get("/helpdesk/return-agents", helpDeskController.createTokenForReturningUsers)
  router.get("/helpdesk/all-agents", helpDeskController.getAllUsers)

  return router;
};

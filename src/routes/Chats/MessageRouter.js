const router = require("express").Router();
const messageController = require("./../../controllers/chat/messagesController");

module.exports = () => {
    router.post("/users/createMessage", messageController.createMessage)
    router.get("/users/getMessage/:conversationID", messageController.getMessages)
    router.post("/message/new", messageController.helpdeskMessages)

    return router
}
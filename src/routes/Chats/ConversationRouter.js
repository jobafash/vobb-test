const router = require("express").Router();
const conversationController = require("./../../controllers/chat/conversationController");
const chatPatnerController = require("./../../controllers/chat/ChatPartnersController");
const { auth } = require("./../../middlewares/Auth");

module.exports = () => {
  router.post(
    "/users/createConversation",
    conversationController.createConversation
  );
  router.get(
    "/users/getConversation/:userId",
    conversationController.getConversation
  );
  router.get("/agent/chat/partners", auth, chatPatnerController.allPartners);

  return router;
};

const { addUser, removeUser, getUser } = require("./utils/usersChat");
const messageModel = require("./models/chat/messages");

const chatSystem = (io) => {
  // building socket server
  io.on("connection", (socket) => {
    // add online users
    socket.on("addUser", ({ userID }) => {
      const users = addUser(userID, socket.id);

      io.emit("getUsers", users);
    });

    // recieveing sent messages from the frontend and emitting to the other guy
    socket.on(
      "sendMessage",
      async ({ senderID, receiverID, conversationID, text }) => {
        const user = getUser(receiverID);
        // save the messages to the db.
        const message = new messageModel({ conversationID, senderID, text });
        await message.save();

        io.to(user.socketID).emit("getMessage", { senderID, text });
      }
    );

    socket.on("disconnect", () => {
      const users = removeUser(socket.id);

      io.emit("getUsers", users);
    });
  });
};

module.exports = chatSystem;

const { UnAuthorizedError } = require("./../../lib/appErrors");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const User = require("./../models/UserModel");
require("dotenv").config();

const { roles } = require("./Roles");

exports.grantAccess = (action, resource) => {
  return async (req, res, next) => {
    try {
      const permission = roles.can(req.user.role)[action](resource);
      if (!permission.granted) {
        throw new UnAuthorizedError(
          "You don't have enough permission to perform this action"
        );
      }
      next();
    } catch (error) {
      next(error);
    }
  };
};

const getToken = (req) => req.headers["x-auth-token"];

exports.auth = async function (req, res, next) {
  //Logic for authentication goes in here
  const token = getToken(req);
  if (!token) throw new UnAuthorizedError("Pass a valid token");
  try {
    const decoded = jwt.verify(
      req.headers["x-auth-token"],
      process.env.JWT_SECRET
    );

    const user = await User.findById(decoded._id);
    if (!user || !user.isVerified) {
      throw new UnAuthorizedError("User is not authorized");
    }

    req.user = user;
    next();
  } catch (error) {
    const errors = ["TokenExpiredError", "NotBeforeError", "JsonWebTokenError"];
    if (errors.includes(error?.name)) {
      throw new UnAuthorizedError("Authentication failed, check your token");
    }
    next(error);
  }
};

exports.hashPassword = async function (password) {
  return await bcrypt.hash(password, 10);
};

exports.validatePassword = async function (plainPassword, hashedPassword) {
  return await bcrypt.compare(plainPassword, hashedPassword);
};

exports.admin = async function (req, res, next) {
  if (req.user && req.user.role == "admin") {
    next();
  } else {
    throw new UnAuthorizedError("This user is not authorized for this action");
  }
};

const { Schema, model } = require("mongoose");
const validator = require("validator");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const { v4 } = require("uuid");
const randomUUID = v4;
const { BadRequestError } = require("./../../lib/appErrors");
const options = { discriminatorKey: "userType" };

const agentSchema = new Schema(
  {
    isVerified: {
      type: Boolean,
      default: false,
    },
    isFirstTime: {
      type: Number,
      default: 0,
    },
    avatar: {
      type: String,
      default: "",
    },
    name: {
      type: String,
      required: true,
      lowercase: true,
    },
    agentCountry: {
      type: String,
      trim: true,
    },
    address: {
      type: String,
      trim: true,
      required: true,
    },
    agencyName: {
      type: String,
      lowercase: true,
      required: true,
      trim: true,
    },
    numRatings: {
      type: Number,
    },
    bio: {
      type: String,
      default: "",
      trim: true,
    },
    email: {
      type: String,
      required: true,
      trim: true,
      lowercase: true,
      unique: true,

      validate(value) {
        if (!validator.isEmail(value)) {
          throw new BadRequestError("input must be a valid email address");
        }
      },
    },
    password: {
      type: String,
      trim: true,
      minlength: 7,
      required: true,
      
      validate(value) {
        if (
          value.includes("password") ||
          value.includes("PASSWORD") ||
          value.includes(123)
        )
          throw new BadRequestError("Password too simple");
      },
    },
    role: {
      type: String,
      enum: ["admin", "user"],
      default: "user",
    },
  },
  {
    timestamps: true,
  }
);

agentSchema.methods.toJSON = function () {
  const user = this;
  const userObject = user.toObject();

  delete userObject.password;

  return userObject;
};

agentSchema.methods.generateToken = async function () {
  const agent = this;

  const token = jwt.sign(
    { _id: agent._id.toString(), role: agent.role },
    process.env.JWT_SECRET
  );

  return token;
};

module.exports = model("Agent", agentSchema);

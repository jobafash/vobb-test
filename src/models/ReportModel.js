const { Schema, model } = require("mongoose");

const ReportModelSchema = new Schema(
{
    student: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'Agent'
        // _type: 'Student'
    },
    report: {
        type: String,
        required: true
    },
    reportContent: {
        type: String,
        required: false
    },
},
{
    timestamps: true,
}
);

module.exports = model(
    "Report",
    ReportModelSchema
);
  
const { Schema, model } = require("mongoose");
const validator = require("validator");
const { ExpectationFailedError } = require("../../lib/appErrors");

const newsletterModelSchema = new Schema(
  {
    email: {
      type: String,
      trim: true,
      unique: true,
      lowercase: true,

      validate(value) {
        if (!validator.isEmail(value))
          throw new ExpectationFailedError("Expecting an Email value");
      },
    },
  },
  {
    timestamps: true,
  }
);

const NewsletterModel = model("Newsletter", newsletterModelSchema);

module.exports = NewsletterModel;

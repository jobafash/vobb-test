const { Schema, model } = require("mongoose");

const AgentAffiliatedUniversitySchema = new Schema(
  {
    country: {
      type: String,
      required: true,
      lowercase: true,
      trim: true,
    },
    university: {
      type: String,
      required: true,
      lowercase: true,
      trim: true,
    },
    agents: [
      {
        type: Schema.Types.ObjectId,
        ref: "Agent",
      },
    ],
  },
  { timestamps: true }
);

AgentAffiliatedUniversitySchema.index({ country: 1 });

module.exports = model(
  "AgentAffiliated_University",
  AgentAffiliatedUniversitySchema
);

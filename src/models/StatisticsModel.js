const { Schema, model } = require("mongoose");

const statSchema = new Schema(
    {
        profileViews: {
          type: Number,
          default: 0,
        },
        inquiredRecieved: {
          type: Number,
          default: 0,
        },
        admissions: {
          type: Number,
          default: 0,
        },
        partners: {
          type: Number,
          default: 0,
          max: 5
        },
        totalPayable: {
          type: Number,
          default: 0
        },
        totalReceivable: {
          type: Number,
          default: 0
        },
        sentRequests: {
          type: Number,
          default: 0,
          max: 5
        },
        receivedRequests: {
          type: Number,
          default: 0
        },
        agent: {
          type: Schema.Types.ObjectId
        }
      },
      {
        timestamps: true,
      }
);

const statModel = model("Stats", statSchema);
module.exports = statModel;
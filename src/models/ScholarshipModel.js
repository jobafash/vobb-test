const { Schema, model } = require("mongoose");
const validator = require("validator");
const { InvalidError } = require("./../../lib/appErrors");

const ScholarshipSchema = new Schema({
  email: {
    type: String,
    trim: true,
    required: true,
    unique: true,

    validate(value) {
      if (!validator.isEmail(value)) {
        throw new InvalidError("string must be an email");
      }
    },
  },
  student: {
    type: Schema.Types.ObjectId,
    required: true,
  },
});


module.exports = model("ScholarshipModel", ScholarshipSchema)
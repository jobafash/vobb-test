const { Schema, model } = require("mongoose");
const validator = require("validator");
const { BadRequestError } = require("./../../../lib/appErrors");

const ApplicationSchema = new Schema(
  {
    name: {
      type: String,
      lowercase: true,
      required: true,
    },
    gender: {
      type: String,
      trim: true,
      lowercase: true,
      required: true,
    },
    nationality: {
      type: String,
      trim: true,
      lowercase: true,
      required: true,
    },
    passportNumber: {
      type: String,
      trim: true,
      required: true,
    },
    address: {
      type: String,
      trim: true,
      lowercase: true,
      required: true,
    },
    city: {
      type: String,
      trim: true,
      lowercase: true,
      required: true,
    },
    country: {
      type: String,
      trim: true,
      lowercase: true,
      required: true,
    },
    province: {
      type: String,
      trim: true,
      lowercase: true,
      required: true,
    },
    postalCode: {
      type: String,
      trim: true,
      lowercase: true,
    },
    university: {
      type: Schema.Types.ObjectId,
      ref: "AgentAffiliated_University",
      required: true,
    },
    maritalStatus: {
      type: String,
      trim: true,
      lowercase: true,
      required: true,
    },
    dateOfBirth: {
      type: String,
      trim: true,
      lowercase: true,
      required: true,
    },
    degree: {
      type: String,
      trim: true,
      lowercase: true,
      required: true,
    },
    studentEmail: {
      type: String,
      lowercase: true,
      required: true,

      validate(value) {
        if (!validator.isEmail(value)) {
          throw new BadRequestError("Value must be of type email");
        }
      },
    },
    courseOfStudy: {
      type: String,
      lowercase: true,
      required: true,
    },
    intakeYear: {
      type: String,
      lowercase: true,
      required: true,
    },
    intakeMonth: {
      type: String,
      lowercase: true,
      required: true,
    },
    additionalNote: {
      type: String,
    },
    files: [
      {
        original_name: {
          type: String,
          required: true,
        },
        path: {
          type: String,
          required: true,
        },
      },
    ],
    receivingAgent: {
      _id: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: "Agent",
      },
      status: {
        type: String,
        enum: [
          "new",
          "review",
          "accepted",
          "rejected",
          "submitted",
          "admission",
          "payable",
        ],
        default: "new",
      },
    },
    sendingAgent: {
      _id: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: "Agent",
      },
      status: {
        type: String,
        enum: [
          "sent",
          "review",
          "accepted",
          "submitted",
          "rejected",
          "admission",
        ],
        default: "sent",
      },
    },
  },
  { timestamps: true }
);

module.exports = model("Application", ApplicationSchema);

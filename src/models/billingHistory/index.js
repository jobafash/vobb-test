const { Schema, model } = require("mongoose");

const BillingHistorySchema = new Schema(
  {
    invoiceNumber: {
      type: Schema.Types.ObjectId,
      required: true,
      trim: true,
      ref: "Invoice",
    },
    tranferReceipt: {
      receipt: { type: String, trim: true },
    },
    recievingAgent: {
      type: Schema.Types.ObjectId,
      ref: "Agent",
      required: true,
    },
    amountPaid: { type: Number, trim: true },
    paid: { type: Boolean, default: false },
    sessionArray: [
      {
        sessionID: {
          type: String,
        },
        processed: {
          type: Boolean,
          default: false,
        },
      },
    ],
    modeOfPayment: {
      type: String,
      default: "card",
      enum: ["card", "transfer"],
    },
    agentID: { type: Schema.Types.ObjectId, ref: "Agent", required: true },
  },
  { timestamps: true }
);

const BillingHistory = model("BillingHistory", BillingHistorySchema);

module.exports = BillingHistory;

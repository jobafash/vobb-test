const { Schema, model } = require("mongoose");

const requestAccessSchema = new Schema(
  {
    agentRequesting: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    universities: [
      {
        university: {
          type: Schema.Types.ObjectId,
          ref: "AgentAffiliated_University",
        },
        isVerified: {
          type: String,
          enum: ["pending", "processing", "accepted", "denied"],
          default: "pending",
        },
      },
    ],
    processed: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);


module.exports = model("Request_Access", requestAccessSchema);

const { Schema, model } = require("mongoose");

const messageSchema = new Schema(
  {
    conversationID: {
      type: String,
    },
    senderID: {
      type: String,
    },
    text: {
      type: String,
    },
  },
  { timestamps: true }
);

module.exports = model("Message", messageSchema);

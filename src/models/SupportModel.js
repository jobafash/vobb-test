const { Schema, model} = require("mongoose");
const validator = require("validator");
const { BadRequestError } = require("./../../lib/appErrors");

const contactSupportSchema = new Schema({
    title: {
        type: String,
        trim: true,
        default: 'New message',
        required: false
    },
    message: {
        type: String,
        trim: true,
        required: true
    },
    attended: {
        type: Boolean,
        default: false
    },
    email: {
        type: String,
        required: true,
        trim: true,
        lowercase: true,
        validate(value) {
          if (!validator.isEmail(value)) {
            throw new BadRequestError("Must be a valid email address");
          }
        }
    },
}, { timestamps: true})

module.exports = model("contactSupport", contactSupportSchema)
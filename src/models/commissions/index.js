const { Schema, model } = require("mongoose");

const commissionSchema = Schema(
  {
    sendingAgent: {
      _id: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: "Agent",
      },
      status: {
        type: String,
        enum: ["payable", "paid"],
        default: "payable",
      },
    },
    receivingAgent: {
      _id: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: "Agent",
      },
      status: {
        type: String,
        enum: ["unconfirmed", "confirmed"],
        default: "unconfirmed",
      },
    },
    commission: {
      type: Number,
      required: true,
    },
    tuitionFees: {
      type: Number,
      default: 0,
    },
    payout: {
      payoutType: {
        type: "string",
        default: "fixed",
        enum: ["fixed", "percentage"],
      },
      amountPaid: {
        type: Number,
      },
      agentShare: {
          type: Number,
      }
    },
    application: {
      type: Schema.Types.ObjectId,
      required: true,
      ref: "Application",
    },
  },
  {
    timestamps: true,
  }
);

const commissionModel = model("Commission", commissionSchema);
module.exports = commissionModel;

const { Schema, model } = require("mongoose");

const AgentSupportingSchema = new Schema(
  {
    agentID: {
      type: Schema.Types.ObjectId,
      ref: "Agent",
      index: true,
      required: true,
    },
    isVerified: {
      type: Boolean,
      default: false,
    },
    universityID: {
      type: Schema.Types.ObjectId,
      ref: "AgentAffiliated_University",
      index: true,
    },
    commission: {
      type: Number,
      default: 0,
    },
    admission: {
      type: Number,
      default: 0,
    },
    tuitionFees: {
      type: Number,
      default: 0
    },
    universityPaymentMethod: {
      isFixed: {
        type: Boolean,
        default: false,
      },
      received: {
        type: Number,
        default: 0
      },
      agentPayout: {
        isFixed: {
          type: Boolean,
          default: false,
        },
        payout: {
          type: Number,
          default: 0,
        },
      },
    },
  },
  { timestamps: true }
);

module.exports = model("AgentSupport", AgentSupportingSchema);

const { Schema, model } = require("mongoose");

const InvoiceSchema = new Schema(
  {
    companyName: {
      type: String,
      required: true,
      trim: true,
    },
    companyAddress: {
      type: String,
      required: true,
      trim: true,
    },
    unixCreationDate: {
      type: Number,
      required: true,
      trim: true,
    },
    invoiceDueDate: {
      type: Number,
      default: 5,
      trim: true,
    },
    totalAmount: {
      type: Number,
      required: true,
      trim: true,
    },
    invoiceUniqueCode: {
      type: String,
      required: true,
      trim: true,
    },
    vobbTotalAmount: {
      type: Number,
      required: true,
      trim: true,
    },
    commissionList: [
      {
        title: {
          type: String,
          trim: true,
          required: true,
        },
        quantity: {
          type: Number,
          required: true,
          trim: true,
        },
        vobbAmount: {
          type: Number,
          required: true,
          trim: true,
        },
        price: {
          type: Number,
          required: true,
          trim: true,
        },
        reciever: {
          type: Schema.Types.ObjectId,
          ref: "Agent",
          required: true,
        },
      },
    ],

    agentID: {
      type: Schema.Types.ObjectId,
      ref: "Agent",
      required: true,
    },
    paid: {
      type: Boolean,
      default: false,
    },
    modeOfPayment: {
      type: String,
      default: "card",
      enum: ["card", "transfer"],
    },
    totalPeopleToPay: {
      type: Number,
      default: 0
    },
  },
  { timestamps: true }
);

const invoice = model("Invoice", InvoiceSchema);

module.exports = invoice;

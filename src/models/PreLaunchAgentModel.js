const { Schema, model } = require("mongoose");

const preLaunchAgentSchema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true,
  },
  phone: {
    type: String,
    required: true,
    unique: true,
    required: true,
  },
});

const PreLaunchAgent = model("PreLaunchAgent", preLaunchAgentSchema);

module.exports = PreLaunchAgent;
const { Schema, model } = require("mongoose");

const ParnterhipAffiliationsSchema = new Schema(
  {
    universityIDs: [
      {
        university: {
          type: Schema.Types.ObjectId,
          ref: "AgentAffiliated_University",
        },
        commission: {
          type: Number,
          default: 0,
          trim: true,
        },
      },
    ],
    partnershipAgentID: {
      type: Schema.Types.ObjectId,
      ref: "Agent",
    },

    agentId: {
      type: Schema.Types.ObjectId,
      required: true,
      ref: "Agent",
    },
  },
  { timestamps: true }
);

module.exports = model("partner_Affiliation", ParnterhipAffiliationsSchema);

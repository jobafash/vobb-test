const { Schema, model } = require("mongoose");

const directAffiliationsSchema = new Schema(
  {
    commissions: {
      type: Number,
      default: 0,
      trim: true,
    },
    commissionType: {
      type: String,
      required: true,
    },
    isverified: {
      type: String,
      enum: ["pending", "accepted", "rejected"],
      default: "pending",
    },
    university: {
      type: Schema.Types.ObjectId,
      ref: "AgentAffiliated_University",
    },
    agents: [
      {
        type: Schema.Types.ObjectId,
        ref: "Agent",
      },
    ],
    agentId: {
      type: Schema.Types.ObjectId,
      required: true,
      ref: "Agent",
    },
  },
  { timestamps: true }
);

module.exports = model("Direct_Affiliation", directAffiliationsSchema);

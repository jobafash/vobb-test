const { Schema, model } = require("mongoose");

const partnershipSchema = Schema(
    {
      agentCountry: {
        type: String,
        trim: true,
      },
      university: {
        type: Schema.Types.ObjectId,
        ref: "AgentAffiliated_University",
        required: true,
      },
      note: {
        type: String,
        trim: true
      },
      sendingAgent: {
        _id: {
            type: Schema.Types.ObjectId,
            required: true,
            ref: "Agent",
        },
        status: {
            type: String,
            enum: ["sent", "accepted", "rejected"],
            default: 'sent',
        }
      },
      receivingAgent: {
        _id: {
            type: Schema.Types.ObjectId,
            required: true,
            ref: "Agent",
        },
        status: {
            type: String,
            enum: ["new", "accepted", "rejected"],
            default: 'new',
        }
      },
    },
    {
      timestamps: true,
    }
);

const partnershipModel = model("Partnership", partnershipSchema);
module.exports = partnershipModel;
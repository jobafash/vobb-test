const { Schema, model } = require("mongoose");

const preLaunchAgentSchema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
   
  },
  country: {
    type: String,
    trim: true,
  }
});

const PreLaunchAgent = model("PreLaunch_Agent2", preLaunchAgentSchema);

module.exports = PreLaunchAgent;
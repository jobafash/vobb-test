const { Schema, model } = require("mongoose");

const StripeSchema = new Schema(
  {
    customerID: { type: String, trim: true },
    cardID: { type: String, trim: true },
    amountEarned: { type: Number, default: 0 },
    amountSpent: { type: Number, default: 0 },
    agentID: { type: Schema.Types.ObjectId, required: true, ref: "Agent" },
  },
  { timestamps: true }
);

const stripeAccount = model("StripeCustomer", StripeSchema);

module.exports = stripeAccount;

const {Schema, model} = require("mongoose");

const vobbAdminPanelSchema = new Schema({
    numberOfAgents: {
        type: Number,
        default: 0
    },
    numberOfStudents: {
        type: Number,
        default: 0
    },
    numberOfUniversities: {
        type: Number,
        default: 0
    },
    numberOfSearches: {
        type: Number,
        default: 0
    }
})

module.exports = model("vobbAdminPanel", vobbAdminPanelSchema)
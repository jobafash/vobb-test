const { Schema, model } = require("mongoose");

const reviewSchema = Schema(
    {
      rating: { type: Number, required: true },
      comment: { type: String },
      student: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'User'
        // _type: 'Student'
      },
      agent: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'Agent'
        // _type: 'Agent'
      },
    },
    {
      timestamps: true,
    }
);

const reviewModel = model("Review", reviewSchema);
module.exports = reviewModel;
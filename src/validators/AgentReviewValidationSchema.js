const Joi = require("joi");

const AgentReviewSchema = Joi.object({
  admissions: Joi.boolean().required(),
  rating: Joi.number().required(),
  reviews: Joi.string().required(),
});

module.exports = {
  AgentReviewSchema,
};

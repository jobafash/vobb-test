const Joi = require("joi");

const newPartnershipValidation = Joi.object({
  universityID: Joi.string().required(),
  partnerID: Joi.string().required(),
  note: Joi.string().required()
});

const updatePartnershipValidation = Joi.object({
  status: Joi.string().required(),
});


module.exports = {
  newPartnershipValidation,
  updatePartnershipValidation
};

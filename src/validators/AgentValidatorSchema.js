const Joi = require("joi");

const AgentSignUpValidation = Joi.object({
  organizationType: Joi.string().required(),
  name: Joi.string().required(),
  email: Joi.string().required().email(),
  bio: Joi.string().optional(),
  password: Joi.string().required(),
  avatar: Joi.string().optional(),
  agentCountry: Joi.string().required(),
  AffiliatedCountry: Joi.array().items(Joi.string()).optional(),
});

const AgentlLoginValidation = Joi.object({
  email: Joi.string().required(),
  password: Joi.string().required(),
});

const AgentForgotPasswordValidation = Joi.object({
  email: Joi.string().email().required(),
});

const allowableUpdatesSchema = Joi.object({
  email: Joi.string().allow("", null).email(),
  bio: Joi.string().allow("", null),
  password: Joi.string().allow("", null),
  name: Joi.string().allow("", null),
  AffiliatedCountry: Joi.array().items(Joi.string()).allow("", null),
  agentCountry: Joi.string().allow("", null),
});

const PasswordValidateSchema = Joi.object({
  currentPassword: Joi.string().required(),
  newPassword: Joi.string().required(),
  confirmPassword: Joi.string().required(),
});

const RequestAccessValidationSchema = Joi.object({
  agencyName: Joi.string().required(),
  address: Joi.string().required(),
  name: Joi.string().required(),
  password:Joi.string().required(),
  email: Joi.string()
    .email({ tlds: { allow: false } })
    .required(),
});

const AgentAffiliationWIthUniversities = Joi.object({
  affiliations: Joi.array().items(
    Joi.object({
      country: Joi.string().required(),
      university: Joi.string().required(),
      commission: Joi.number().required()
    })
  ),
});

module.exports = {
  AgentSignUpValidation,
  AgentlLoginValidation,
  AgentForgotPasswordValidation,
  allowableUpdatesSchema,
  PasswordValidateSchema,
  RequestAccessValidationSchema,
  AgentAffiliationWIthUniversities,
};

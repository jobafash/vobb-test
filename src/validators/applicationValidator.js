const Joi = require("joi");

const newApplication = Joi.object({
  name: Joi.string().required(),
  gender: Joi.string().required(),
  nationality: Joi.string().required(),
  passportNumber: Joi.string().required(),
  fathersName: Joi.string().required(),
  mothersName: Joi.string().required(),
  university: Joi.string().required(),
  degree: Joi.string().required(),
  courseOfStudy: Joi.string().required(),
  additionalNote: Joi.string().optional(),
  //if this fails, try files as name
  uploadedDocs: Joi.array().items(Joi.string()).required(),
});

const updateApplication = Joi.object({
  status: Joi.string().required()
});

module.exports = {
  newApplication,
  updateApplication
};

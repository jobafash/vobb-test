const Joi = require("joi");

const UniversityValidation = Joi.object({
  name: Joi.string().required(),
  email: Joi.string().required().email(),
  bio: Joi.string().optional(),
  country: Joi.string().required(),
  contactName: Joi.string().required(),
  phone: Joi.string().min(7).required(),
  comment: Joi.string().optional(),
  title: Joi.string().optional(),
});

const UniversityLoginValidation = Joi.object({
  universityName: Joi.string().required(),
  password: Joi.string().required(),
});

const ForgotPasswordValidation = Joi.object({
  universityName: Joi.string().required(),
});

const UniversityUpdateValidation = Joi.object({
    name: Joi.string().optional(),
    email: Joi.string().optional().email(),
    bio: Joi.string().optional(),
    country: Joi.string().optional(),
    contactName: Joi.string().optional(),
    avatar: Joi.any().meta({swaggerType: 'file'}).optional().allow(''),
    coverPhoto: Joi.any().meta({swaggerType: 'file'}).optional().allow(''),
    phone: Joi.string().min(8).optional(),
    comment: Joi.string().optional(),
    title: Joi.string().optional(),
});

const PasswordValidateSchema = Joi.object({
  currentPassword: Joi.string().required(),
  newPassword: Joi.string().required(),
  confirmPassword: Joi.string().required(),
})

module.exports = {
  UniversityValidation,
  UniversityLoginValidation,
  ForgotPasswordValidation,
  UniversityUpdateValidation,
  PasswordValidateSchema
};

const Joi = require("joi");

const SignUpValidation = Joi.object({
  email: Joi.string().required().email(),
  password: Joi.string().required(),
  avatar: Joi.string().optional(),
  name: Joi.string().required(),
});

const studentProfileUpdate = Joi.object({
  name: Joi.string().allow("", null),
  email: Joi.string().allow("", null).email(),
});

const PasswordValidateSchema = Joi.object({
  currentPassword: Joi.string().required(),
  newPassword: Joi.string().required(),
  confirmPassword: Joi.string().required(),
})

module.exports = {
  SignUpValidation,
  studentProfileUpdate,
  PasswordValidateSchema
}
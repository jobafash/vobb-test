const express = require("express");
const socketio = require("socket.io");
const colors = require("colors");
const http = require("http");
const cors = require("cors");
const router = require("express").Router();
require("dotenv").config();
require("./db/DB");
require("express-async-errors");
const { ErrorHandler } = require("./middlewares/ErrorHandlers");
const { NotFoundError } = require("./../lib/appErrors");
const chatSystem = require("./sockets");
const rootRouter = require("./routes/index")(router);

const app = express();
const port = process.env.PORT;

// middlewares():
app.use((req, res, next) => {
  if (req.originalUrl === "/api/v1/webhook/payment-successful") {
    next();
  } else {
    express.json()(req, res, next);
  }
});
app.use(cors());

app.use((req, res, next) => {
  console.log(`request made to: ${req.url}`.white.bgMagenta);
  next();
});
const server = http.createServer(app);
const io = socketio(server);

chatSystem(io);

//route loaders():
app.set("trust proxy", true);
app.use("/api/v1", rootRouter);

// static page to very connection on live
app.get("/status", (req, res) => {
  res.send({ message: "server is hot and live" });
});

// if route is not handled up till this point
app.use((req, res, next) => {
  next(new NotFoundError());
});

// app Errors middleware handlers
app.use(ErrorHandler);

server.listen(port, () => {
  console.log("server is up on port: " + port);
});

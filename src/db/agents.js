const bcrypt = require("bcrypt");

const agents = [
  {
    email: 'user1@gmail.com',
    password: bcrypt.hashSync('123456', 10),
    isVerified: true,
    agentCountry: "nga",
    AffiliatedCountry :[
         "jpn" ,
         "chn" ,
         "deu" ,
         "gha" 
    ],
    organizationType : "agent",
    firstName : "John",
    organizationName : "John's Org"
  },
  {
    email: 'user2@gmail.com',
    password: bcrypt.hashSync('123456', 10),
    isVerified: true,
    agentCountry: "deu",
    AffiliatedCountry :[
         "jpn" ,
         "nga" ,
         "deu" ,
         "gha" 
    ],
    organizationType : "agency",
    firstName : "Jane",
    organizationName : "Jane's Org"
  },
  {
    email: 'user3@gmail.com',
    password: bcrypt.hashSync('123456', 10),
    isVerified: true,
    agentCountry: "gha",
    AffiliatedCountry :[
         "jpn" ,
         "chn" ,
         "nga"
    ],
    organizationType : "agent",
    firstName : "John",
    organizationName : "John's Org"
  },
  {
    email: 'user4@gmail.com',
    password: bcrypt.hashSync('123456', 10),
    isVerified: true,
    agentCountry: "jpn",
    AffiliatedCountry :[
         "chn" ,
         "deu" ,
         "gha" 
    ],
    organizationType : "agency",
    firstName : "Jane",
    organizationName : "Jane's Org"
  },
  {
    email: 'user5@gmail.com',
    password: bcrypt.hashSync('123456', 10),
    isVerified: true,
    agentCountry: "nga",
    AffiliatedCountry :[
         "jpn" ,
         "chn" ,
         "deu" ,
         "gha" 
    ],
    organizationType : "agent",
    firstName : "John",
    organizationName : "John's Org"
  },
  {
    email: 'user6@gmail.com',
    password: bcrypt.hashSync('123456', 10),
    isVerified: true,
    agentCountry: "deu",
    AffiliatedCountry :[
         "jpn" ,
         "nga" ,
         "deu" ,
         "gha" 
    ],
    organizationType : "agency",
    firstName : "Jane",
    organizationName : "Jane's Org"
  },
  {
    email: 'user7@gmail.com',
    password: bcrypt.hashSync('123456', 10),
    isVerified: true,
    agentCountry: "gha",
    AffiliatedCountry :[
         "jpn" ,
         "chn" ,
         "nga"
    ],
    organizationType : "agent",
    firstName : "John",
    organizationName : "John's Org"
  },
  {
    email: 'user8@gmail.com',
    password: bcrypt.hashSync('123456', 10),
    isVerified: true,
    agentCountry: "jpn",
    AffiliatedCountry :[
         "chn" ,
         "deu" ,
         "gha" 
    ],
    organizationType : "agency",
    firstName : "Jane",
    organizationName : "Jane's Org"
  },
  {
    email: 'user9@gmail.com',
    password: bcrypt.hashSync('123456', 10),
    isVerified: true,
    agentCountry: "nga",
    AffiliatedCountry :[
         "jpn" ,
         "chn" ,
         "deu" ,
         "gha" 
    ],
    organizationType : "agent",
    firstName : "John",
    organizationName : "John's Org"
  },
  {
    email: 'user10@gmail.com',
    password: bcrypt.hashSync('123456', 10),
    isVerified: true,
    agentCountry: "deu",
    AffiliatedCountry :[
         "jpn" ,
         "nga" ,
         "deu" ,
         "gha" 
    ],
    organizationType : "agency",
    firstName : "Jane",
    organizationName : "Jane's Org"
  },
  {
    email: 'user11@gmail.com',
    password: bcrypt.hashSync('123456', 10),
    isVerified: true,
    agentCountry: "gha",
    AffiliatedCountry :[
         "jpn" ,
         "chn" ,
         "nga"
    ],
    organizationType : "agent",
    firstName : "John",
    organizationName : "John's Org"
  },
  {
    email: 'user12@gmail.com',
    password: bcrypt.hashSync('123456', 10),
    isVerified: true,
    agentCountry: "jpn",
    AffiliatedCountry :[
         "chn" ,
         "deu" ,
         "gha" 
    ],
    organizationType : "agency",
    firstName : "Jane",
    organizationName : "Jane's Org"
  },
  {
    email: 'user13@gmail.com',
    password: bcrypt.hashSync('123456', 10),
    isVerified: true,
    agentCountry: "nga",
    AffiliatedCountry :[
         "jpn" ,
         "chn" ,
         "deu" ,
         "gha" 
    ],
    organizationType : "agent",
    firstName : "John",
    organizationName : "John's Org"
  },
  {
    email: 'user14@gmail.com',
    password: bcrypt.hashSync('123456', 10),
    isVerified: true,
    agentCountry: "deu",
    AffiliatedCountry :[
         "jpn" ,
         "nga" ,
         "deu" ,
         "gha" 
    ],
    organizationType : "agency",
    firstName : "Jane",
    organizationName : "Jane's Org"
  },
  {
    email: 'user15@gmail.com',
    password: bcrypt.hashSync('123456', 10),
    isVerified: true,
    agentCountry: "gha",
    AffiliatedCountry :[
         "jpn" ,
         "chn" ,
         "nga"
    ],
    organizationType : "agent",
    firstName : "John",
    organizationName : "John's Org"
  },
  {
    email: 'user16@gmail.com',
    password: bcrypt.hashSync('123456', 10),
    isVerified: true,
    agentCountry: "jpn",
    AffiliatedCountry :[
         "chn" ,
         "deu" ,
         "gha" 
    ],
    organizationType : "agency",
    firstName : "Jane",
    organizationName : "Jane's Org"
  },
  {
    email: 'user17@gmail.com',
    password: bcrypt.hashSync('123456', 10),
    isVerified: true,
    agentCountry: "nga",
    AffiliatedCountry :[
         "jpn" ,
         "chn" ,
         "deu" ,
         "gha" 
    ],
    organizationType : "agent",
    firstName : "John",
    organizationName : "John's Org"
  },
  {
    email: 'user18@gmail.com',
    password: bcrypt.hashSync('123456', 10),
    isVerified: true,
    agentCountry: "deu",
    AffiliatedCountry :[
         "jpn" ,
         "nga" ,
         "deu" ,
         "gha" 
    ],
    organizationType : "agency",
    firstName : "Jane",
    organizationName : "Jane's Org"
  },
  {
    email: 'user19@gmail.com',
    password: bcrypt.hashSync('123456', 10),
    isVerified: true,
    agentCountry: "gha",
    AffiliatedCountry :[
         "jpn" ,
         "chn" ,
         "nga"
    ],
    organizationType : "agent",
    firstName : "John",
    organizationName : "John's Org"
  },
  {
    email: 'user20@gmail.com',
    password: bcrypt.hashSync('123456', 10),
    isVerified: true,
    agentCountry: "jpn",
    AffiliatedCountry :[
         "chn" ,
         "deu" ,
         "gha" 
    ],
    organizationType : "agency",
    firstName : "Jane",
    organizationName : "Jane's Org"
  },
  {
    email: 'user21@gmail.com',
    password: bcrypt.hashSync('123456', 10),
    isVerified: true,
    agentCountry: "nga",
    AffiliatedCountry :[
         "jpn" ,
         "chn" ,
         "deu" ,
         "gha" 
    ],
    organizationType : "agent",
    firstName : "John",
    organizationName : "John's Org"
  },
  {
    email: 'user22@gmail.com',
    password: bcrypt.hashSync('123456', 10),
    isVerified: true,
    agentCountry: "deu",
    AffiliatedCountry :[
         "jpn" ,
         "nga" ,
         "deu" ,
         "gha" 
    ],
    organizationType : "agency",
    firstName : "Jane",
    organizationName : "Jane's Org"
  },
  {
    email: 'user23@gmail.com',
    password: bcrypt.hashSync('123456', 10),
    isVerified: true,
    agentCountry: "gha",
    AffiliatedCountry :[
         "jpn" ,
         "chn" ,
         "nga"
    ],
    organizationType : "agent",
    firstName : "John",
    organizationName : "John's Org"
  },
  {
    email: 'user24@gmail.com',
    password: bcrypt.hashSync('123456', 10),
    isVerified: true,
    agentCountry: "jpn",
    AffiliatedCountry :[
         "chn" ,
         "deu" ,
         "gha" 
    ],
    organizationType : "agency",
    firstName : "Jane",
    organizationName : "Jane's Org"
  },
  {
    email: 'user25@gmail.com',
    password: bcrypt.hashSync('123456', 10),
    isVerified: true,
    agentCountry: "nga",
    AffiliatedCountry :[
         "jpn" ,
         "chn" ,
         "deu" ,
         "gha" 
    ],
    organizationType : "agent",
    firstName : "John",
    organizationName : "John's Org"
  },
  {
    email: 'user26@gmail.com',
    password: bcrypt.hashSync('123456', 10),
    isVerified: true,
    agentCountry: "deu",
    AffiliatedCountry :[
         "jpn" ,
         "nga" ,
         "deu" ,
         "gha" 
    ],
    organizationType : "agency",
    firstName : "Jane",
    organizationName : "Jane's Org"
  },
  {
    email: 'user27@gmail.com',
    password: bcrypt.hashSync('123456', 10),
    isVerified: true,
    agentCountry: "gha",
    AffiliatedCountry :[
         "jpn" ,
         "chn" ,
         "nga"
    ],
    organizationType : "agent",
    firstName : "John",
    organizationName : "John's Org"
  },
  {
    email: 'user28@gmail.com',
    password: bcrypt.hashSync('123456', 10),
    isVerified: true,
    agentCountry: "jpn",
    AffiliatedCountry :[
         "chn" ,
         "deu" ,
         "gha" 
    ],
    organizationType : "agency",
    firstName : "Jane",
    organizationName : "Jane's Org"
  },
  {
    email: 'user29@gmail.com',
    password: bcrypt.hashSync('123456', 10),
    isVerified: true,
    agentCountry: "nga",
    AffiliatedCountry :[
         "jpn" ,
         "chn" ,
         "deu" ,
         "gha" 
    ],
    organizationType : "agent",
    firstName : "John",
    organizationName : "John's Org"
  },
  {
    email: 'user30@gmail.com',
    password: bcrypt.hashSync('123456', 10),
    isVerified: true,
    agentCountry: "deu",
    AffiliatedCountry :[
         "jpn" ,
         "nga" ,
         "deu" ,
         "gha" 
    ],
    organizationType : "agency",
    firstName : "Jane",
    organizationName : "Jane's Org"
  },
  {
    email: 'user31@gmail.com',
    password: bcrypt.hashSync('123456', 10),
    isVerified: true,
    agentCountry: "gha",
    AffiliatedCountry :[
         "jpn" ,
         "chn" ,
         "nga"
    ],
    organizationType : "agent",
    firstName : "John",
    organizationName : "John's Org"
  },
  {
    email: 'user32@gmail.com',
    password: bcrypt.hashSync('123456', 10),
    isVerified: true,
    agentCountry: "jpn",
    AffiliatedCountry :[
         "chn" ,
         "deu" ,
         "gha" 
    ],
    organizationType : "agency",
    firstName : "Jane",
    organizationName : "Jane's Org"
  },
]

module.exports = agents;

const appResponse = require("./../../../lib/appResponse");
const { BadRequestError } = require("./../../../lib/appErrors");
class CommissionController {
  constructor(logger, commissionService) {
    this.logger = logger;
    this.commissionService = commissionService;
  }

  async get(req, res) {
    return await this.commissionService
      .get(req.user, req.query)
      .then(({ totalCommission, pageNo, availablePages, results }) => {
        return res.send(
          appResponse("Commissions retrieved successfully", {
            totalCommission,
            currentPage: pageNo,
            availablePages,
            results,
          })
        );
      })
      .catch((error) => {
        this.logger.log(error);
        res.status(500).json(error);
      });
  }

  async getSingle(req, res) {
    return await this.commissionService
      .getSingle(req.params)
      .then((data) => {
        return res.send(appResponse("Commission retrieved successfully", data));
      })
      .catch((error) => {
        this.logger.log(error);
        res.status(500).json(error);
      });
  }

  async update(req, res) {
    return await this.commissionService
      .update(req.params, req.body)
      .then((data) => {
        return res.send(appResponse("Commission updated successfully", data));
      })
      .catch((error) => {
        this.logger.log(error);
        res.status(500).json(error);
      });
  }

  async searchAndFilter(req, res) {
    const param = req.query;

    if (!param)
      throw new BadRequestError(
        "Missing required field(s). rquired (payable, receivable"
      );

    const user = req.user;

    const collectionSearch = await this.commissionService.searchAndFilter(
      user,
      param
    );

    res.send(appResponse("fetched result successfully", collectionSearch));
  }

  async updateCommissions(req, res) {
    const commissionID = typeof req.params.commissionID !== "string" ? false : req.params.commissionID;
    const tuitionFees = typeof req.body.tuitionFees !== "number" ? false : req.body.tuitionFees;

    if(!commissionID || !tuitionFees) throw new BadRequestError("Missing required field(s)")

    return await this.commissionService
    .updateCommissions(req.user, commissionID, tuitionFees)
    .then((data) => {
      return res.send(appResponse("Commission updated successfully", data));
    })
    .catch((error) => {
      this.logger.log(error);
      res.send(appResponse(error.message));
    });
  }
}

module.exports = CommissionController;

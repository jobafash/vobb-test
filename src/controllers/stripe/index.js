const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);
const axios = require("axios");
const stripeBillingService = require("./../../services/stripe/");
const billingHistoryModel = require("./../../models/billingHistory/");
const invoiceModel = require("./../../models/invoices/");
const appResponse = require("./../../../lib/appResponse");
const { BadRequestError } = require("./../../../lib/appErrors");

class StripeAccount {
  async supportedCountries(req, res) {
    const listData = [
      "AU",
      "AT",
      "BE",
      "BR",
      "BG",
      "CA",
      "CY",
      "CZ",
      "DK",
      "EE",
      "FI",
      "FR",
      "DE",
      "GR",
      "HK",
      "HU",
      "IN",
      "IE",
      "IT",
      "JP",
      "LV",
      "LT",
      "LU",
      "MT",
      "MX",
      "NL",
      "NZ",
      "NO",
      "PL",
      "PT",
      "RO",
      "SG",
      "SK",
      "SI",
      "ES",
      "SE",
      "CH",
      "GB",
      "US",
    ];
    res.send(appResponse("fetched list of supported Countries", listData));
  }
  async createCustomerAccount(req, res) {
    const { country } = req.body;
    const createdUser = await stripeBillingService.createCustomer(
      req.user,
      country
    );

    res.send(appResponse("created user successfully", createdUser));
  }

  async addBillingDetails(req, res) {
    const cardDetails = req.body;
    const billingDetails = await stripeBillingService.createToken(
      req.user,
      cardDetails
    );

    res.send(
      appResponse("created billing details successfully", billingDetails)
    );
  }

  async agentPayout(req, res) {
    const { paymentType } = req.query;
    const { invoiceID } = req.params;

    if (typeof paymentType === "undefined" && !invoiceID)
      throw new BadRequestError("Please Provide a payment type or invoice id");

    let successfulPayment;

    if (paymentType && paymentType === "credit_card" && invoiceID) {
      successfulPayment = await stripeBillingService.agentPayout(
        req.user,
        invoiceID
      );
    } else if (paymentType && paymentType === "bank_transfer" && invoiceID) {
      if (!req?.file?.path)
        throw new BadRequestError(
          "Missing required fields for the Evidence of payment"
        );
      const file = req.file.path;
      successfulPayment = await stripeBillingService.payoutBankTransfer(
        req.user,
        invoiceID,
        file
      );
    }

    res.send(appResponse("Made Payments successfully", successfulPayment));
  }

  // webhooks
  async webhookSuccessfuPayment(request, response) {
    let event;
    let endpointSecret = process.env.STRIPE_WEBHOOK_ENDPOINT_SECRET; // remember to tidy up here later
    if (endpointSecret) {
      // Get the signature sent by Stripe
      const signature = request.headers["stripe-signature"];
      try {
        event = stripe.webhooks.constructEvent(
          request.body,
          signature,
          endpointSecret
        );
      } catch (err) {
        console.log(`⚠️Webhook signature verification failed.`, err.message);
        return response.sendStatus(400);
      }
    }

    // Handle the event
    let session;
    let slackBody;
    switch (event.type) {
      case "checkout.session.completed":
        session = event.data.object;

        // now I want to find the billing infornation of that sessions _id
        const billingInfo = await billingHistoryModel.find({});
        for (let eachBill of billingInfo) {
          const billSessionIndex = eachBill.sessionArray.findIndex(
            (sessionEvent) => sessionEvent.sessionID === session.id
          );
          if (billSessionIndex > -1) {
            // we need to find the invoice
            const invoice = await invoiceModel.findOne({
              agentID: eachBill.agentID,
            });
            invoice.totalPeopleToPay -= 1;

            if (invoice.totalPeopleToPay === 0) {
              invoice.paid = true;
            }
            eachBill.paid = true;
            await eachBill.save();
            await invoice.save();
          }
        }

        slackBody = {
          text: "successful payments",
          attachments: [
            {
              color: "good",
              text: `${session.customer_details.email} made a payment of USD${
                session.amount_total / 100
              }. payment status: ${session.payment_status}`,
            },
          ],
        };
        // posting to slack
        axios
          .post(process.env.SLACK_STRIPE_COMPLETED_PAYMENTS_URL, slackBody)
          .then(() => {
            console.log("ok");
          })
          .catch((err) => {
            console.log(err);
          });
        break;
      // ... handle other event types
      case "capability.updated":
        session = event.data.object;
        if (session.status === "inactive") {
          slackBody = {
            text: "Payment capabilities",
            attachments: [
              {
                color: "warning",
                text: `Account on Stripe: || ${session.account}||=> payment status is ${session.status}`,
              },
            ],
          };
          // posting to slack
          axios
            .post(process.env.SLACK_STRIPE_COMPLETED_PAYMENTS_URL, slackBody)
            .then(() => {
              console.log("ok");
            })
            .catch((err) => {
              console.log(err);
            });
        }

        break;
      default:
        console.log(`Unhandled event type ${event.type}`);
    }

    // Return a 200 response to acknowledge receipt of the event
    response.send();
  }

  async billingHistory(req, res) {
    const param = req.query;
    let billReciept;
    const { agentType, billType } = req.query;
    if (agentType && agentType === "sender") {
      if (billType) {
        billReciept = await stripeBillingService.senderBillHistory(
          req.user,
          param,
          billType
        );
      } else if (!billType) {
        billReciept = await stripeBillingService.senderBillHistory(
          req.user,
          param
        );
      }
    } else if (agentType && agentType === "reciever") {
      if (billType) {
        billReciept = await stripeBillingService.RecieverBillHistory(
          req.user,
          param,
          billType
        );
      } else if (!billType) {
        billReciept = await stripeBillingService.RecieverBillHistory(
          req.user,
          param
        );
      }
    }

    res.send(appResponse("fetched Bill History successfully", billReciept));
  }
}

module.exports = new StripeAccount();

const { InvalidError } = require("../../../lib/appErrors");
const conversationModel = require("./../../models/chat/conversation");
const appResponse = require("./../../../lib/appResponse")

class ChatConversation {
  async createConversation(req, res) {
    const { senderID, receiverID } = req.body;
    const conversation = new conversationModel({
      members: [senderID.toString(), receiverID.toString()],
    });

    const savedConversation = await conversation.save();

    res.send(appResponse('saved conversation successfully', savedConversation));
  }

  async getConversation(req, res) {
    const { userId } = req.params;
    try {
        const conversation = await conversationModel.find({
            members: { $in: [userId] },
          });

          res.send(appResponse("fetched conversation succesfully", conversation));
    }catch(e) {
        throw new InvalidError("Invalid conversation")
    }
    
  }
}

module.exports = new ChatConversation()

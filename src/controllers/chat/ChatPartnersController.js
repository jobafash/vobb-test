const chatPartnerService = require("./../../services/chat/ChatPartners")
const appResponse = require("./../../../lib/appResponse")

class ChatPartners {
    async allPartners(req, res) {
        const user = req.user
        const allPartners = await chatPartnerService.allPartners(user)

        res.send(appResponse("Fetched partners successfully", allPartners))
    }
}

module.exports = new ChatPartners
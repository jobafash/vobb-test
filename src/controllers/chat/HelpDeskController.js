const helpDeskService = require("../../services/chat/HelpDeskService");
const { BadRequestError } = require("./../../../lib/appErrors");
const appResponse = require("../../../lib/appResponse");

class HelpDesk {
  async createUser(req, res) {
    const { user } = req.body;
    const data = {
      uid: new Date().getTime(),
      name: user,
    };

    const createdToken = await helpDeskService.createUser(data);

    res.send(appResponse("created token successfully", createdToken));
  }

  async createTokenForReturningUsers(req, res) {
    const { uid } = req.query;
    if (!uid) throw new BadRequestError("Missing required field(s)");

    const authToken = await helpDeskService.createTokenForReturningUsers(uid);

    res.send(appResponse("created token successfully", authToken));
  }

  async getAllUsers(req, res) {
    const allUsers = await helpDeskService.getAllUsers();

    res.send(appResponse("fetched users successfully", allUsers));
  }
}

module.exports = new HelpDesk();

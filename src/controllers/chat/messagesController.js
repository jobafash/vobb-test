const MessageModel = require("./../../models/chat/messages");
const { InternalServerError } = require("./../../../lib/appErrors");
const appResponse = require("./../../../lib/appResponse");
const slack_config = {
  'slack': {
    webhook_url: process.env.SLACK_SUPPORT_WEBHOOK,
    channel: process.env.SLACK_SUPPORT_CHANNEL
  }
};
const slack_env = {
  params: ['slack']
};
const slack_logger = require('turbo-logger').createStream(slack_config, slack_env.params);

class ChatMessages {
  async createMessage(req, res) {
    try {
      const newMessage = new MessageModel({ ...req.body });
      const savedMessage = await newMessage.save();

      res.send(appResponse("saved messsages successfully", savedMessage));
    } catch (e) {
      throw new InternalServerError("Something went wrong");
    }
  }

  async getMessages(req, res) {
    const { conversationID } = req.params;
    try {
      const messsage = await MessageModel.find({ conversationID });

      res.send(appResponse("fetched conversation successfully", messsage));

    } catch (e) {
      throw new InternalServerError("Something went wrong");
    }
  }

  async helpdeskMessages(req, res) {
    const data  = req.body;
    try {
      
      //console.log(data);
      slack_logger.log('New ticket: ', data);

      res.send(appResponse("fetched message successfully", data));

    } catch (e) {
      throw new InternalServerError("Something went wrong");
    }
  }
}

module.exports = new ChatMessages();

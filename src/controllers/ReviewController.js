const reviewModel = require("./../models/ReviewModel");
const { User } = require("./../models/UserModel");
const {
    BadRequestError,
    NotFoundError,
} = require("./../../lib/appErrors");
const appResponse = require("./../../lib/appResponse");

class Review {
  async create(req, res) {
      const payload = req.body;
      if (!payload.rating || !payload.agent){
        throw new BadRequestError("Rating and agent id are required");
      }
      const user = await User.findById(req.user._id);
      if (!user || user.userType !== 'Student') throw new NotFoundError("Invalid user. User must be a student");
      const data = {
        ...payload,
        student: req.user._id,
      };
      const agent = await User.findById(payload.agent);
      if (!agent || agent.userType !== 'Agent') throw new NotFoundError("Agent not found.");
      const review = await reviewModel.findOne({agent: payload.agent});
      if(review){
        if (review.student === req.user._id){
          throw new BadRequestError("You can only rate an agent once");
        }
      }
      const newReview = new reviewModel(data);
      await newReview.save();
      const total = ((agent.rating) * Number(agent.numRatings)) + payload.rating;
      const quotient = agent.numRatings + 1;
      const avgScore = total/quotient;
      agent.rating = avgScore;
      agent.numRatings = quotient;
      await agent.save();
      return res.send(appResponse("Review created successfully", newReview));
  }
  async update(req, res) {
      const { id } = req.params;
      const payload = req.body;
      const user = await User.findById(req.user._id);
      if (!user || user.userType !== 'Student') throw new NotFoundError("Invalid user. User must be a student");
      const review = await reviewModel.findById(id);
      if (!review){
        throw new NotFoundError("Review not found");
      }
      if (!payload.rating || !payload.agent){
        throw new BadRequestError("Rating and agent id are required,");
      }
      const agent = await User.findById(payload.agent);
      if (!agent || agent.userType !== 'Agent') throw new NotFoundError("Agent not found.");
      const total = ((agent.rating) * Number(agent.numRatings)) + payload.rating - review.rating;
      const quotient = agent.numRatings;
      const avgScore = total/quotient;
      agent.rating = avgScore;
      agent.numRatings = quotient;
      await agent.save();
      const updatedData = await reviewModel.findByIdAndUpdate(id, payload);
      await updatedData.save();
      return res.send(appResponse("Review updated successfully"));
  }
  async get(req, res) {
    let { pageNo, noOfReviews } = req.query;
    pageNo = pageNo ? Number(pageNo) : 1;
    noOfReviews = noOfReviews ? Number(noOfReviews) : 10;
    let Reviews;
    let reviewCount;
    let availablePages;

    let reviewList = await reviewModel
      .find({ }, "-__v -updatedAt")
      .sort({ createdAt: -1 })

      Reviews = await reviewModel
      .find({ }, "-__v -updatedAt")
      .sort({ createdAt: -1 })
      .limit(noOfReviews)
      .skip((pageNo - 1) * noOfReviews)

    reviewCount = reviewList.length;
    availablePages = Math.ceil(reviewCount / noOfReviews);
    return res.send(appResponse("Reviews retrieved successfully", {
        currentPage: pageNo,
        availablePages,
        Reviews,
      }));
  }
  async getByUser(req, res) {
    const { id } = req.params;
    if (!id){
      throw new BadRequestError("id is required,");
    }
    let { pageNo, noOfReviews } = req.query;
    pageNo = pageNo ? Number(pageNo) : 1;
    noOfReviews = noOfReviews ? Number(noOfReviews) : 10;
    let Reviews;
    let reviewCount;
    let availablePages;

    let reviewList = await reviewModel
      .find({ agent: id }, "-__v -updatedAt")
      .sort({ createdAt: -1 })

      Reviews = await reviewModel
      .find({ agent: id }, "-__v -updatedAt")
      .sort({ createdAt: -1 })
      .limit(noOfReviews)
      .skip((pageNo - 1) * noOfReviews)
    //   .populate({
    //     path: "student",
    //     model: "Student",
    //     select: { __v: 0, createdAt: 0, updatedAt: 0 },
    //   });

    reviewCount = reviewList.length;
    availablePages = Math.ceil(reviewCount / noOfReviews);

    return res.send(appResponse("Reviews for agent retrieved successfully", {
        currentPage: pageNo,
        availablePages,
        Reviews,
    }));
  }
  async getSingle(req, res) {
    const { id } = req.params;
    const review = await reviewModel.findById(id);
    if (!review){
      throw new NotFoundError("Review not found");
    }
    return res.send(appResponse("Review retrieved successfully", review));
  }
  async deleteSingle(req, res) {
    const { id } = req.params;
    const user = await User.findById(req.user._id);
    if (!user || user.userType !== 'Student') throw new NotFoundError("Invalid user. User must be a student");
    const review = await reviewModel.findById(id);
    if (!review){
      throw new NotFoundError("Review not found");
    }
    await reviewModel.findByIdAndDelete(id);
    return res.send(appResponse("Review deleted successfully"));
  }
}
module.exports = new Review();

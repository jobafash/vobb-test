const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const UniversityService = require("./../services/UniversityService");
const supportService = require("./../services/SupportService");
const partnershipModel = require('./../models/PartnershipModel');
const {
  DuplicateError,
  BadRequestError,
  NotFoundError,
  UnAuthorizedError,
  InvalidError,
  InternalServerError,
} = require("./../../lib/appErrors");
const { User } = require("./../models/UserModel");
const {
  verificationMailPrivate,
  passwordResetMailPrivate,
} = require("./../utils/mailer");
const appResponse = require("./../../lib/appResponse");
const {
  UniversityValidation
} = require("./../validators/UniversityValidatorSchema");
const {
  verificationMail,
  partnershipRequestMail,
  requestUpdateMail
} = require("./../utils/mailer");

class University {
  async createUniversity(req, res) {
    const payload = req.body;
    const result = await UniversityValidation.validateAsync(payload);

    const Existing = await User.findOne({ email: payload.email });
    if (Existing) throw new DuplicateError("University exists already");

    const data = {
      ...payload
    };

    const University = await UniversityService.createUniversity(data);
    if (!University) throw new InternalServerError("Something went wrong while creating this university");

    // const emailData = {
    //   email: University.email,
    //   name: University.firstName,
    //   templateId: process.env.EMAIL_VERIFICATION_ID,
    //   subject: "ACCOUNT CREATION SUCCESSFUL",
    // };
    // verificationMail(emailData);

    res.send(appResponse("University created successfully.", University));
  }

  // async login(req, res) {
  //   const University = await UniversityService.findUniversityByName(req.body.universityName);
  //   if (!University) throw new NotFoundError("University not found");

  //   const generateToken = (id) => {
  //     return jwt.sign({ id }, process.env.JWT_SECRET, {
  //       expiresIn: '6h',
  //     })
  //   }

  //   const token = await generateToken(University._id);

  //   if (!University.isVerified) throw new UnAuthorizedError("University is not verified.");

  //   const isMatch = await bcrypt.compare(req.body.password, University.password);
  //   if (!isMatch) throw new UnAuthorizedError("Invalid sign in details");

  //   res.send(appResponse(`Logged in ${University.universityName} successfully`, { token }));
  // }

  // async sendEmail(req, res) {
  //   const { universityName } = req.body;
  //   const University = await UniversityService.findUniversityByName(universityName);
  //   if (!University) throw new NotFoundError("University does not exist");
  //   const EmailData = {
  //     name: University.universityName
  //   };
  //   verificationMailPrivate(
  //     University.contactEmail,
  //     EmailData,
  //     process.env.EMAIL_VERIFICATION_ID
  //   );

  //   res.send(appResponse("Email sent successfully"));
  // }

  // async forgotPassword(req, res) {
  //   const { universityName } = req.body;
  //   const University = await UniversityService.findUniversityByName(universityName);
  //   if (!University) throw new NotFoundError("University does not exist");

  //   const token = jwt.sign(
  //     { _id: University._id.toString() },
  //     process.env.PASSWORD_TOKEN,
  //     { expiresIn: '1d' }
  //   );

  //   // console.log(token);

  //   passwordResetMailPrivate(University, token);

  //   await University.save();

  //   res.send(appResponse("Email sent successfully"));
  // }

  // async verifyToken(req, res) {
  //   const { token } = req.query;
  //   const decoded = await jwt.verify(token, process.env.PASSWORD_TOKEN);

  //   const University = await UniversityService.findUniversityById(decoded._id);
  //   if (!University) throw new NotFoundError("University does not exist");

  //   res.send(appResponse("University can update password successfully"));
  // }

  // async resetPassword(req, res) {
  //   if (!req?.query?.token) throw new BadRequestError("No token was passed");
  //   const { token } = req.query;
  //   const { newPassword, confirmPassword } = req.body;

  //   if (newPassword !== confirmPassword)
  //     throw new InvalidError("Passwords do not match");

  //   const decoded = await jwt.verify(token, process.env.PASSWORD_TOKEN);
  //   if (!decoded) throw new InvalidError("Invalid credentials.");

  //   const user = await UniversityService.findUniversityById(decoded._id);

  //   if (!user) throw new NotFoundError("University does not exist");
  //   user.password = await bcrypt.hash(newPassword, 10);
  //   await user.save();

  //   res.send(appResponse("Your password has been updated successfully"));
  // }

  // async update(req, res) {
  //   const updatedUniversity = await UniversityService.update(req.University, req.body);
  //   res.send(appResponse("University updated successfully", updatedUniversity));
  // }
  
  // async adminUpdate(req, res) {
  //   const university = req.params.id;
  //   const updatedUniversity = await UniversityService.adminUpdate(university, req.body);
  //   res.send(appResponse("University updated successfully", updatedUniversity));
  // }

  // async updatePassword(req, res) {
  //   const passwordUpdate = await UniversityService.updatePassword(
  //     req.University,
  //     req.body
  //   );
  //   if (!passwordUpdate) throw new InternalServerError("Something went wrong");
  //   res.send(appResponse("Password updated successfully"));
  // }

  async requestPartnership(req, res) {
    const { sender, receiver, code } = req.body;
    if (!code) throw new BadRequestError("No code was passed");
    const user = await User.findOne({ code: code });
    const uni = await User.findOne({ _id: receiver });
    const agent = await User.findOne({ _id: sender });
    if (!agent || agent.userType !== 'Agent') throw new BadRequestError("Invalid user. User must be a signed in agent");
    if (String(req.user._id) != String(agent._id)) throw new BadRequestError("The signed in agent must be the one sending the request");
    
    if (String(user._id) !== String(uni._id)) throw new BadRequestError("The university with this code doesn't match user");
    const data = {
      ...req.body
    }
    const checkReq = await partnershipModel.findOne({ sender });
    if (String(checkReq.receiver) == String(receiver)) throw new BadRequestError("The agent has already sent a request to this university");
    if (agent.sentRequests == 10) throw new BadRequestError("The agent has already sent 10 requests");
    agent.sentRequests += 1;
    await agent.save();

    const newRequest = new partnershipModel(data);
    if (!newRequest) throw new InternalServerError("Error occured while creating this request");
    await newRequest.save();
    const emailData = {
      email: user.email,
      name: user.name,
      templateId: process.env.PARTNERSHIP_RESET,
      subject: "NEW PARTNERSHIP REQUEST",
    };

    partnershipRequestMail(emailData);

    res.send(appResponse("Partnership request successfully created"));
  }

  async updatePartnership(req, res) {
    const { receiver, status } = req.body;
    const { id } = req.params;

    const request = await partnershipModel.findById(id);
    if (!request) throw new NotFoundError("Partnership request not found");

    const agent = await User.findOne({ _id: request.sender });
    if (!agent || agent.userType !== 'Agent') throw new BadRequestError("Invalid agent");
    
    const uni = await User.findOne({ _id: receiver });
    if (!uni || uni.userType !== 'University') throw new BadRequestError("Invalid university");
    if (String(req.user._id) != String(uni._id)) throw new BadRequestError("The signed in university must be the one updating the request");
    const data = {
      ...req.body
    }
    const update = await partnershipModel.findByIdAndUpdate(id, data);
    if (!update) throw new InternalServerError("Error occured while updating the request");
    if(status === 'accepted'){
      agent.universities.push(receiver);
      uni.verifiedAgents.push(request.sender);
      await agent.save();
      await uni.save();
    }

    const emailData = {
      email: agent.email,
      name: agent.name,
      templateId: process.env.PARTNERSHIP_REQUEST_STATUS,
      subject: "PARTNERSHIP REQUEST UPDATE",
      variableData: {
        status
      },
    };

    requestUpdateMail(emailData);

    res.send(appResponse("Partnership request successfully updated"));
  }

  // async getPendingRequests(req, res) {
  //   const pendingRequests = await UniversityService.getPendingRequests(req.University);
  //   res.send(
  //     appResponse("Fetched Pending requests successfully", pendingRequests)
  //   );
  // }

  // async viewUniversity(req, res) {
  //   const { id } = req.params;
  //   const viewUniversity = await UniversityService.findUniversityById(id);
  //   if (!viewUniversity) throw new NotFoundError("University not found");

  //   let { agentData, _doc} = viewUniversity;
  //   if (req.query.country){
  //     let country = req.query.country;
  //     agentData = agentData.filter((element) => {
  //         return element.agentCountry == country;
  //     });
  //     res.send(appResponse("Returned successfully", { agentData, universityData: _doc }));
  //   }
  //   else if (req.query.name){
  //     let name = req.query.name;
  //     agentData = agentData.filter((element) => {
  //         return element.name == name;
  //     });
  //     res.send(appResponse("Returned successfully", { agentData, universityData: _doc }));
  //   }
  //   else {
  //     res.send(appResponse("Found University successfully", { agentData, universityData: _doc }));
  //   }
  // }

  async updateRequest(req, res) {
    const { universityId, code, status } = req.body;
    const request = await UniversityService.updateRequest(
      req.agent,
      status,
      universityId,
      code
    );
    res.send(appResponse("Updated request succesfully", request));
  }

  async supportMessage(req, res) {
    const data = {
      ...req.body,
      university: req.University._id,
    };

    const newSupport = await supportService.supportMessage(data);

    res.send(appResponse("Support message sent successfully", newSupport));
  }

  // async deleteUniversity(req, res) {
  //   try {
  //     req.University.remove();
  //     res.send(appResponse("University deleted successfully"));
  //   } catch (e) {
  //     throw new InternalServerError("something went wrong");
  //   }
  // }

  // interaction with Universities
  async fetchAllUniversities(req, res) {
    const user = req.user;
    const { accepted, name, pending, pageNo, noOfUnis, country } = req.query
    const allUniversities = await UniversityService.fetchAllUniversities(accepted, name, user, pending, pageNo, noOfUnis, country);
    
    res.send(appResponse("Universities returned successfully", allUniversities));
  }
  
  async fetchApprovedAgents(req, res) {
    let agents = await UniversityService.fetchApprovedAgents(req.user);
    if (req.query.country){
      let country = req.query.country;
      agents = agents.filter((element) => {
          return element.agentCountry == country;
      });
      res.send(appResponse("Approved agents returned successfully", agents));
    }
    if (req.query.name){
      let name = req.query.name;
      agents = agents.filter((element) => {
          return element.name == name;
      });
      res.send(appResponse("Approved agents returned successfully", agents));
    }
    res.send(appResponse("Approved agents returned successfully", agents));
  }
}

module.exports = new University();

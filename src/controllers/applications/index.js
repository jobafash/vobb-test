const appResponse = require("./../../../lib/appResponse");

class AppController {
  constructor(logger, applicationService) {
    this.logger = logger;
    this.applicationService = applicationService;
  }

  async create(req, res) {
    return await this.applicationService
      .create(req.files, req.user, req.body)
      .then((data) => {
        return res.send(appResponse("Application created successfully", data));
      })
      .catch((error) => {
        this.logger.log(error);
        res.status(400).send(appResponse(error.message));
      });
  }
  async getUniversities(req, res) {
    return await this.applicationService
      .getUniversities(req.user)
      .then((universities) => {
        return res.send(
          appResponse(
            "Available universities retrieved successfully",
            universities
          )
        );
      })
      .catch((error) => {
        this.logger.log(error);
        res.status(500).json({ error });
      });
  }
  async get(req, res) {
    return await this.applicationService
      .get(req.user, req.query)
      .then(({ count, pageNo, availablePages, fetchedApps }) => {
        return res.send(
          appResponse("Applications retrieved successfully", {
            count,
            currentPage: pageNo,
            availablePages,
            fetchedApps,
          })
        );
      })
      .catch((error) => {
        this.logger.log(error);
        res.status(500).json({ error });
      });
  }

  async getSingle(req, res) {
    return await this.applicationService
      .getSingle(req.params)
      .then((data) => {
        return res.send(
          appResponse("Application retrieved successfully", data)
        );
      })
      .catch((error) => {
        this.logger.log(error);
        res.status(500).json({ error });
      });
  }

  async update(req, res) {
    return await this.applicationService
      .update(req.user, req.params, req.body)
      .then((data) => {
        return res.send(
          appResponse("Agent's application updated successfully", data)
        );
      })
      .catch((error) => {
        this.logger.log(error);
        res.status(400).json(error);
      });
  }

  async searchApplication(req, res) {
    const param = req.query;
    const user = req.user;

    const fetchedFilteredData = await this.applicationService.searchApplication(
      user,
      param
    );

    res.send(appResponse(" fetched Filters successfully", fetchedFilteredData));
  }
}

module.exports = AppController;

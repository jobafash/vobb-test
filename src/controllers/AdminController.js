const appResponse = require("./../../lib/appResponse");
const { InternalServerError } = require("./../../lib/appErrors");

class AdminController {
  constructor(logger, adminService) {
    this.logger = logger;
    this.adminService = adminService;
  }

  async verifyAgent(req, res) {
    return await this.adminService
      .verifyAgent(req.body)
      .then(() => {
        return res.send(appResponse("Agent verified successfully"));
      })
      .catch((error) => {
        this.logger.log(error);
        throw new InternalServerError("Something went wrong.", error);
      });
  }

  async getRequests(req, res) {
    return await this.adminService
      .getRequests(req.query)
      .then(({ pageNo, availablePages, fetchedRequests }) => {
        return res.send(
          appResponse("Requests retrieved successfully", {
            currentPage: pageNo,
            availablePages,
            fetchedRequests,
          })
        );
      })
      .catch((error) => {
        this.logger.log(error);
        throw new InternalServerError("Something went wrong.", error);
      });
  }

  async getMessages(req, res) {
    return await this.adminService
      .getMessages(req.query)
      .then((messages) => {
        return res.send(
          appResponse("Messages retrieved successfully", messages)
        );
      })
      .catch((error) => {
        this.logger.log(error);
        throw new InternalServerError("Something went wrong.", error);
      });
  }

  async getMessage(req, res) {
    return await this.adminService
      .getMessage(req.params.id)
      .then((messages) => {
        return res.send(
          appResponse("Message retrieved successfully ", messages)
        );
      })
      .catch((error) => {
        this.logger.log(error);
        throw new InternalServerError("Something went wrong.", error);
      });
  }

  async updateMessage(req, res) {
    return await this.adminService
      .updateMessage(req.params.id)
      .then((messages) => {
        return res.send(
          appResponse("Message updated successfully", messages)
        );
      })
      .catch((error) => {
        this.logger.log(error);
        throw new InternalServerError("Something went wrong.", error);
      });
  }

  async updateUniversities(req, res) {
    return await this.adminService
      .updateUniversities(req.body)
      .then((data) => {
        return res.send(
          appResponse("Agent's universities updated successfully", data)
        );
      })
      .catch((error) => {
        this.logger.log(error);
        throw new InternalServerError("Something went wrong.", error);
      });
  }

  async fetchedVerifiedAgents(req, res) {
    const param = req.query;
    return await this.adminService
      .fetchedVerifiedAgents(param)
      .then((data) => {
        return res.send(
          appResponse("Retrieved verified agents successfully", data)
        );
      })
      .catch((error) => {
        this.logger.log(error);
        throw new InternalServerError(`something went Wrong ${error}`);
      });
  }

  async fetchUnverifiedAgents(req, res) {
    const param = req.query;
    return await this.adminService
      .fetchUnverifiedAgents(param)
      .then((data) => {
        return res.send(
          appResponse("Retrieved verified agents successfully", data)
        );
      })
      .catch((error) => {
        this.logger.log(error);
        throw new InternalServerError(`something went Wrong ${error}`);
      });
  }

  async deleteAgent(req, res) {
    const { agentID } = req.params;

    return await this.adminService
      .deleteAgent(agentID)
      .then((data) => {
        return res.send(appResponse("deleted Agent successfully", data));
      })
      .catch((error) => {
        this.logger.log(error);
        throw new InternalServerError(error.message);
      });
  }
}

module.exports = AdminController;

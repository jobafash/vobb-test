const appResponse = require("./../../../lib/appResponse");
const { BadRequestError } = require("./../../../lib/appErrors");

class PartnershipController {
  constructor(logger, partnershipService) {
    this.logger = logger;
    this.partnershipService = partnershipService;
  }

  async create(req, res) {
    return await this.partnershipService
      .create(req.user, req.body)
      .then((data) => {
        return res.send(appResponse("Request created successfully", data));
      })
      .catch((error) => {
        this.logger.log(error);
        res.status(400).json({ error });
      });
  }

  // search by country and fetch all the universities with their agents
  async searchUniversityByCountry(req, res) {
    const param = req.query;
    const foundUniversities =
      await this.partnershipService.searchUniversityByCountry(param);

    res.send(appResponse("found university succesfully", foundUniversities));
  }

  async getAgentsInUniversity(req, res) {
    const { universityID } = req.params;
    const param = req.query;
    const foundUniversityAgents =
      await this.partnershipService.getAgentsInUniversity(
        req.user,
        universityID,
        param
      );

    res.send(appResponse("fethed agents successfully", foundUniversityAgents));
  }

  async getUniversities(req, res) {
    return await this.partnershipService
      .getUniversities(req.user, req.query)
      .then(({ pageNo, availablePages, fetchedUniversities }) => {
        return res.send(
          appResponse("Available universities retrieved successfully", {
            currentPage: pageNo,
            availablePages,
            fetchedUniversities,
          })
        );
      })
      .catch((error) => {
        this.logger.log(error);
        res.status(500).json(error);
      });
  }

  async get(req, res) {
    return await this.partnershipService
      .get(req.user, req.query)
      .then(({ pageNo, availablePages, fetchedApps }) => {
        return res.send(
          appResponse("Partnerships retrieved successfully", {
            currentPage: pageNo,
            availablePages,
            fetchedApps,
          })
        );
      })
      .catch((error) => {
        this.logger.log(error);
        res.status(500).json(error);
      });
  }

  async getSingleUniversity(req, res) {
    return await this.partnershipService
      .getSingleUniversity(req.params)
      .then((data) => {
        return res.send(appResponse("University retrieved successfully", data));
      })
      .catch((error) => {
        this.logger.log(error);
        res.status(500).json(error);
      });
  }

  async getSingle(req, res) {
    return await this.partnershipService
      .getSingle(req.params)
      .then((data) => {
        return res.send(
          appResponse("Partnership retrieved successfully", data)
        );
      })
      .catch((error) => {
        this.logger.log(error);
        res.status(500).json(error);
      });
  }

  async update(req, res) {
    return await this.partnershipService
      .update(req.user, req.params, req.body)
      .then((data) => {
        return res.send(
          appResponse("Agent's partnership updated successfully", data)
        );
      })
      .catch((error) => {
        this.logger.log(error);
        res.status(500).json(error);
      });
  }

  /**
   *
   * @DESC Managing partnership
   *
   * **/

  async getDirectAffiliates(req, res) {
    const allAffiliates = await this.partnershipService.getDirectAffiliations(
      req.user
    );

    res.send(
      appResponse("fetched Direct Affiliates successfully", allAffiliates)
    );
  }

  async getPartneredAffiliations(req, res) {
    const allAffiliates =
      await this.partnershipService.getPartneredAffiliations(req.user);

    res.send(
      appResponse("fetched Partnered Affiliates successfully", allAffiliates)
    );
  }

  async deletePartnerAffill(req, res) {
    const { type, agentId, university } = req.query;
    let deletedPartner;
    if (type === "directAffiliate") {
      deletedPartner = await this.partnershipService.deleteDirectAffil(
        req.user,
        agentId,
        university
      );
    } else{
      deletedPartner = await this.partnershipService.deleteParnterAffili(
        req.user,
        agentId,
        university
      );
    }

    res.send(appResponse("deleted partnership successfully", deletedPartner));
  }

  async updateCommissions(req, res) {
    const { commission } = req.body;
    const { university } = req.query;

    // check if university exist for the user
    const updatedCommission = await this.partnershipService.updateCommission(
      req.user,
      commission,
      university
    );

    res.send(
      appResponse("updated commissions successfully", updatedCommission)
    );
  }

  async incomingPartnership(req, res) {
    const user = req.user;
    const param = req.query;
    const agentIncomingPartnership =
      await this.partnershipService.agentIncomingPartnership(user, param);

    res.send(
      appResponse(
        "fetched incoming partnership successfully",
        agentIncomingPartnership
      )
    );
  }

  async pendingPartnership(req, res) {
    const user = req.user;
    const param = req.query;
    const agentPendingPartnership =
      await this.partnershipService.agentPendingPartnership(user, param);

    res.send(
      appResponse(
        "fetched Pending partnership successfully",
        agentPendingPartnership
      )
    );
  }

  async getAgentProfile(req, res) {
    const user = req.user;
    const { partnerID } = req.params;
    const agentStatistics = await this.partnershipService.getAgentProfile(
      user,
      partnerID
    );
    res.send(
      appResponse("fetched Agent Profile Successfully", agentStatistics)
    );
  }

  async listOfCountries(req, res) {
    const countryList = await this.partnershipService.listOfCountries();

    res.send(appResponse("fetched country successfully", countryList));
  }

  async sendpartnerAlink(req, res) {
    let invitedPersons;
    const user = req.user;

    if (!req?.files) {
      if (req?.body && req.body.emails.length > 0) {
        const { emails } = req.body;
        invitedPersons = await this.partnershipService.sendpartnerAlink(
          user,
          emails
        );
      }
    } else if (req?.files) {
      
      invitedPersons = await this.partnershipService.sendMultiplePartnerLinks(
        user,
        req.files
      );
    }

    if (!invitedPersons) throw new InternalServerError("somehting went Wrong");

    res.send(appResponse("sent invitation Link successfully", invitedPersons));
  }
}

module.exports = PartnershipController;

const ScholarshipService = require("./../services/ScholarshipService");
const { NotFoundError, InternalServerError } = require("./../../lib/appErrors");
const appResponse = require("./../../lib/appResponse");
const { User } = require("./../models/UserModel");

class Scholarship {
  async createScholarship(req, res) {
    const { _id } = req.user;
    const user = await User.findById(_id);

    if (!user || user.userType !== 'Student') throw new NotFoundError("Invalid user. User must be a registered student");
    const data = {
      email:req.user.email,
      student: req.user._id,
    };

    const createSholarshipList = await ScholarshipService.createScholarshipList(
      data
    );
    if (!createSholarshipList)
      throw new InternalServerError("somehting went wrong");
    
    res.send(appResponse("added to list successfully"))
  }
  async getScholarships(req, res) {
    const SholarshipList = await ScholarshipService.findAllScholarships();
    
    res.send(appResponse("Retrieved list successfully", SholarshipList));
  }
}

module.exports = new Scholarship()
const partnershipModel = require("./../models/PartnershipModel");
const { User } = require("./../models/UserModel");
const {
    BadRequestError,
    NotFoundError,
} = require("./../../lib/appErrors");
const appResponse = require("./../../lib/appResponse");
//const agentController = require("./../controllers/AgentController");
const universityController = require("./../controllers/UniversityController");

class Partnership {
  async create(req, res) {
      const { userType } = req.params;
      switch (userType) {
        // case 'agent':
        //     await agentController.createPartnership(req, res);
        //     break;
        case 'university':
            await universityController.requestPartnership(req, res);
            break;
        default:
          throw new BadRequestError("Invalid request params");
    }
      
  }
  async update(req, res) {
    const { userType } = req.params;
    switch (userType) {
      // case 'agent':
      //     await agentController.createPartnership(req, res);
      //     break;
      case 'university':
          await universityController.updatePartnership(req, res);
          break;
      default:
        throw new BadRequestError("Invalid request params");
    }
  }
  async getAll(req, res) {
    let { pageNo, noOfRequests } = req.query;
    pageNo = pageNo ? Number(pageNo) : 1;
    noOfRequests = noOfRequests ? Number(noOfRequests) : 10;
    let Requests;
    let reqCount;
    let availablePages;
    const user = await User.findOne({ _id: req.user._id });
    if (!user || user.userType === 'Student') throw new BadRequestError("Invalid user");

    if (user.userType === 'University') {
      let reqList = await partnershipModel
      .find({receiver: req.user._id }, "-__v -updatedAt")
      .sort({ createdAt: -1 })

      Requests = await partnershipModel
      .find({receiver: req.user._id }, "-__v -updatedAt")
      .sort({ createdAt: -1 })
      .limit(noOfRequests)
      .skip((pageNo - 1) * noOfRequests)

      reqCount = reqList.length;
      availablePages = Math.ceil(reqCount / noOfRequests);
      return res.send(appResponse("Requests retrieved successfully", {
          currentPage: pageNo,
          availablePages,
          Requests,
      }));
    };
    if (user.userType === 'Agent') {
      let reqList = await partnershipModel
      .find({sender: req.user._id }, "-__v -updatedAt")
      .sort({ createdAt: -1 })

      Requests = await partnershipModel
      .find({sender: req.user._id }, "-__v -updatedAt")
      .sort({ createdAt: -1 })
      .limit(noOfRequests)
      .skip((pageNo - 1) * noOfRequests)

      reqCount = reqList.length;
      availablePages = Math.ceil(reqCount / noOfRequests);
      return res.send(appResponse("Requests retrieved successfully", {
          currentPage: pageNo,
          availablePages,
          Requests,
      }));
    };

  }
  async getOne(req, res) {
    const { id } = req.params;
    if (!id){
      throw new BadRequestError("id is required");
    }
    const user = await User.findOne({ _id: req.user._id });
    if (!user || user.userType === 'Student') throw new BadRequestError("Invalid user");

    const request = await partnershipModel.findById(id);
    if (!request) throw new NotFoundError("Partnership request not found");

    return res.send(appResponse("Request retrieved successfully", request));
  }
}
module.exports = new Partnership();

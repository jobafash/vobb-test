const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { hashPassword } = require("./../middlewares/Auth");
const agentModel = require("./../models/UserModel");
const studentController = require("./../controllers/StudentController");
const agentController = require("./../controllers/AgentController");
const universityController = require("./../controllers/UniversityController");
const agentService = require("./../services/AgentService");
const userService = require("./../services/UserService");
const supportService = require("./../services/SupportService");
const agentSupportModel = require("./../models/AgentsSupportingModel");
const { checkUploadFileType } = require("../utils/uploader");
const {
  BadRequestError,
  NotFoundError,
  InvalidError,
  UnAuthorizedError,
  InternalServerError,
} = require("./../../lib/appErrors");
const { passwordResetMail } = require("./../utils/mailer");
const appResponse = require("./../../lib/appResponse");

class UserC {
  async createUser(req, res) {
    const { userType } = req.params;

    switch (userType) {
      case "student":
        await studentController.createStudent(req, res);
        break;
      case "agent":
        await agentController.createAgent(req, res);
        break;
      case "university":
        await universityController.createUniversity(req, res);
        break;
      default:
        throw new BadRequestError("Invalid request params");
    }
  }

  async resendEmail(req, res) {
    const { userType } = req.params;

    switch (userType) {
      case "student":
        await studentController.resendEmail(req, res);
        break;
      case "agent":
        await agentController.resendEmail(req, res);
        break;
      default:
        throw new BadRequestError("Invalid request params");
    }
  }

  async forgotPassword(req, res) {
    const user = await agentModel.findOne({ email: req.body.email });
    if (!user) throw new NotFoundError("invalid user ");

    const token = jwt.sign(
      { _id: user._id.toString() },
      process.env.PASSWORD_TOKEN,
      { expiresIn: 300 }
    );
    const emailData = {
      email: user.email,
      name: user.name,
      templateId: process.env.RESET_PASSWORD,
      variableData: {
        token,
      },
      subject: "PASSWORD RESET",
    };

    passwordResetMail(emailData);
    await user.save();

    res.send(appResponse("Email sent successfully!"));
  }

  async verifyAccount(req, res) {
    if (!req?.query?.token) throw new BadRequestError("No token was passed");
    const { token } = req.query;

    const decoded = await jwt.verify(token, process.env.PASSWORD_TOKEN);

    const user = await agentModel.findById(decoded._id);

    if (!user) throw new NotFoundError("Invalid user");

    user.isVerified = true;

    await user.save();

    res.send(appResponse("User verified successfully"));
  }
  async resetPassword(req, res) {
    if (!req?.query?.token) throw new BadRequestError("No token was passed");
    const { token } = req.query;
    const { newPassword, confirmPassword } = req.body;

    if (newPassword !== confirmPassword)
      throw new InvalidError("Passwords do not match");

    const decoded = await jwt.verify(token, process.env.PASSWORD_TOKEN);

    const user = await agentModel.findById(decoded._id);

    if (!user) throw new NotFoundError("Invalid user");

    user.password = await hashPassword(newPassword);

    await user.save();

    res.send(appResponse("password updated successfully"));
  }

  async deleteProfilePics(req, res) {
    req.user.avatar = null;

    await req.user.save();

    res.send(appResponse("Deleted avatar"));
  }
  async createAvatar(req, res) {
    const user = req.user;
    if (req.file) {
      const image = await checkUploadFileType(req.file.mimetype);
      if (image === "invalidImageType") {
        throw new BadRequestError("Invalid image type");
      }
      user.avatar = req.file.path;
      await user.save();
    } else {
      throw new BadRequestError("No image was uploaded");
    }

    res.send(appResponse("Uploaded avatar", { avatar: user.avatar }));
  }
  async checkToken(req, res) {
    res.send(appResponse("Verification successful", req.user));
  }
  //   async verifyToken(req, res) {
  //     const { token } = req.query;
  //     const decoded = await jwt.verify(token, process.env.PASSWORD_TOKEN);

  //     console.log(decoded, "###############");
  //     const user = await agentService.findAgentById(decoded._id);
  //     if (!user) throw new NotFoundError("Invalid user");

  //     res.send(appResponse("user can update password successfully"));
  //   }
  async updateProfile(req, res) {
    const update = req.body;
    const updates = Object.keys(update);

    const allowedUpdates = [
      "avatar",
      "name",
      "agentCountry",
      "address",
      "agencyName",
      "bio",
      "password",
    ];
    const isValidOPerations = updates.every((update) =>
      allowedUpdates.includes(update)
    );
    if (!isValidOPerations) {
      throw new BadRequestError("invalid parameter to update");
    }

    try {
      updates.forEach((update) => (req.user[update] = req.body[update]));

      await req.user.save();
    } catch (e) {
      throw new InternalServerError("Something went wrong");
    }

    res.send(appResponse("User field updated successfully", req.user));
  }

  async updatePassword(req, res) {
    const data = req.body;
    const user = await agentModel.findById(req.user._id);
    const passwordMatch = await bcrypt.compare(
      data.currentPassword,
      user.password
    );
    if (!passwordMatch) throw new InvalidError("invalid password");
    if (data.newPassword !== data.confirmPassword)
      throw new InvalidError("new passwords do not match");
    // user can change password here now
    if (data.newPassword === data.confirmPassword) {
      req.user.password = await hashPassword(data.newPassword);
      await req.user.save();
    }
    res.send(appResponse("User password updated successfully"));
  }

  async viewProfile(req, res) {
    const { id } = req.params;
    const user = await agentModel.findById(id).select("-password");

    if (!user) throw new NotFoundError("Invalid user");

    res.send(appResponse("User returned successfully", user));

    //General
  }

  async saveAgent(req, res) {
    const { _id } = req.user;
    const user = await agentModel.findById(_id);
    const { agentId } = req.body;

    if (!user || user.userType !== "Student")
      throw new NotFoundError(
        "Invalid user. User must be a registered student"
      );
    if (!agentId) throw new BadRequestError("No agent id was passed.");
    const agent = await User.findById(agentId);
    if (!agent) throw new NotFoundError("Agent not found.");
    if (user.savedAgents.includes(agentId))
      throw new BadRequestError("Agent already saved.");
    await user.savedAgents.push(agentId);
    await user.save();

    res.send(appResponse("Agent saved successfully"));
  }
  async unsaveAgent(req, res) {
    const { _id } = req.user;
    const user = await agentModel.findById(_id);
    const { agentId } = req.body;

    if (!user || user.userType !== "Student")
      throw new NotFoundError("Invalid user. User must be a student");
    if (!agentId) throw new BadRequestError("No agent id was passed.");
    const agent = await agentModel.findById(agentId);
    if (!agent) throw new NotFoundError("Agent not found.");
    if (!user.savedAgents.includes(agentId))
      throw new BadRequestError("Agent not saved yet.");

    const index = user.savedAgents.indexOf(agentId);
    if (index > -1) {
      user.savedAgents.splice(index, 1);
    }
    await user.save();

    res.send(appResponse("Agent unsaved successfully"));
  }
  async fetchsavedAgents(req, res) {
    const { _id } = req.user;
    const user = await agentModel.findById(_id);

    if (!user || user.userType !== "Student")
      throw new NotFoundError("Invalid user. User must be a student");
    let savedAgents = [];
    for (let id = 0; id < user.savedAgents.length; id++) {
      let agent = await agentModel
        .findById(user.savedAgents[id])
        .select("-password");
      savedAgents.push(agent);
    }

    res.send(appResponse("Agents returned successfully", savedAgents));
  }

  async supportMessage(req, res) {
    if (!req.body.email) throw new BadRequestError("Email is required");

    const data = {
      ...req.body,
    };

    const newSupport = await supportService.supportMessage(data);

    res.send(appResponse("Support message sent successfully", newSupport));
  }

  async deleteUser(req, res) {
    try {
      // const completelyDeletedDatails = await userService.completelyEraseDetails(
      //   req.user
      // );

      await req.user.remove();

      res.send(appResponse("User deleted successfully", req.user));
    } catch (err) {
      throw new InternalServerError("Unable to delete user", err);
    }
  }

  async fetchActiveUniversities(req, res) {
    const Universities = await agentService.fetchActiveUniversities(req.user);
    res.send(appResponse("Universities returned successfully", Universities));
  }

  async agentDashBoardStatistics(req, res) {
    if (!req?.query?.sortBy)
      throw new BadRequestError("Missing required field(s)");
    const { sortBy } = req.query;
    let fetchStatisticsData;
    if (sortBy.toLowerCase() === "week") {
      const time = 1000 * 60 * 60 * 24 * 7;
      fetchStatisticsData = await userService.FetchWeeklyStatistics(
        req.user,
        time
      );
    } else if (sortBy.toLowerCase() === "month") {
      const time = 1000 * 60 * 60 * 24 * 7 * 4;
      fetchStatisticsData = await userService.fetchMonthlyStatistics(
        req.user,
        time
      );
    } else if (sortBy.toLowerCase() === "year") {
      const time = 1000 * 60 * 60 * 24 * 7 * 4 * 365;
      fetchStatisticsData = await userService.FetchYearlyStatistics(
        req.user,
        time
      );
    } else {
      fetchStatisticsData = await userService.FetchAllStatistics(req.user);
    }

    res.send(
      appResponse("fetched statistics successfully", fetchStatisticsData)
    );
  }

  async getEightApplication(req, res) {
    const allowedData = await userService.fetchEightApplications(req.user);

    res.send(appResponse("fetched application successfully", allowedData));
  }
}

module.exports = new UserC();

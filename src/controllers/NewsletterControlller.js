const newsletterService = require("./../services/NewsletterService");
const { InternalServerError } = require("./../../lib/appErrors");
const appResponse = require("./../../lib/appResponse");
const { newsletterMail } = require("./../utils/mailer");

class Newsletter {
  async createNewsletter(req, res) {
    const newsletter = await newsletterService.createNewsletter(req.body);
    if (!newsletter)
      throw new InternalServerError("something went wrong in saving the data");
    const data = {
      email: newsletter.email,
      name: "VOBB Support",
      templateId: process.env.NEWSLETTER_MAIL,
      subject: "We are happy to have you!",
      variableData: {},
    };
    newsletterMail(data);

    res.send(appResponse("You have successfully subscribed.", newsletter));
  }
}

module.exports = new Newsletter();

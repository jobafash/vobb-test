const jwt = require("jsonwebtoken");
const sharp = require("sharp");
const agentModel = require("./../models/UserModel");
const agentService = require("./../services/AgentService");
const statsModel = require("./../models/StatisticsModel");
const supportService = require("./../services/SupportService");
const AgentUniversityService = require("./../services/AgentUniversityService");
const {
  DuplicateError,
  BadRequestError,
  NotFoundError,
  InvalidError,
  UnAuthorizedError,
  InternalServerError,
} = require("./../../lib/appErrors");
const {
  AgentSignUpValidation,
} = require("./../validators/AgentValidatorSchema");
const {
  verificationMail,
  requestAccessMail,
  invitationLink,
} = require("./../utils/mailer");
const appResponse = require("./../../lib/appResponse");
const { checkUploadFileType } = require("../utils/uploader")

// slack configuration call
const slack_config = {
  slack: {
    webhook_url: process.env.SLACK_WEBHOOK_URL,
    channel: process.env.SLACK_CHANNEL,
  },
};
const slack_env = {
  params: ["slack"],
};
const slack_logger = require("turbo-logger").createStream(
  slack_config,
  slack_env.params
);




class Agent {
  async createAgent(req, res) {
    const { agentID } = req.query;
    const payload = req.body;

    const isExisting = await agentModel.findOne({ email: payload.email });
    if (isExisting) throw new BadRequestError("User exists already");

    const agent = await agentService.createAgent(payload);
    if (!agent)
      throw new InternalServerError(
        "Agent couldn't be created. Something went wrong"
      );

    const agency = agent.newAgent.name;
    const AgencyName = agent.newAgent.agencyName;
    const link = `https://www.vobb.io/verification/success?token=${agent.token}`;

    const emailData = {
      email: agent.newAgent.email,
      templateId: process.env.REQUEST_ACCESS_ID,
      variableData: {
        agency,
        AgencyName,
        token: agent.token,
        link,
      },
      subject: "Verify your Account",
    };
    requestAccessMail(emailData);
    if (agentID) {
      const ExistinAgent = await agentModel.findById(agentID);
      if (ExistinAgent) {
        if (agent) {
          const emailData = {
            email: ExistinAgent.email,
            templateId: process.env.ACCEPTED_INVITATION,
            variableData: {
              agencyName: agent.newAgent.agencyName,
            },
            subject: "Accepted Invitation",
          };
          invitationLink(emailData);
        }
      }
    }
    slack_logger.log("New signup from ", agent.newAgent.agencyName);

    res.send(appResponse("Agent created successfully", agent.newAgent));
  }

  async validateAgentToken(req, res) {
    const { token } = req.query;
    const validatedToken = await agentService.validateAgentToken(token);

    if (!validatedToken) throw new UnAuthorizedError("validation failed");

    res.send(appResponse("validated Agent successfully", validatedToken));
  }

  async loginVerifiedAgent(req, res) {
    const data = req.body;
    const loggedInAgent = await agentService.loginVerifiedAgent(data);

    res.send(appResponse("Logged in successfully", loggedInAgent));
  }

  async RegisterAgentUniversityAffiliations(req, res) {
    const data = req.body;
    const universityAffiliation =
      await agentService.CreateUniversityAffiliations(req.user, data);

    res.send(
      appResponse("successfully added instituition(s)", universityAffiliation)
    );
  }

  async getAllUniversitiesForRegistration(req, res) {
    const fetchedUniversities =
      await AgentUniversityService.getAllUniversitiesForRegistration();

    res.send(
      appResponse("fetched Universities successfully", fetchedUniversities)
    );
  }

  async resendEmail(req, res) {
    const { email } = req.body;
    const agent = await agentService.findAgentByEmail(email);
    if (!agent) throw new NotFoundError("User does not exist");

    const token = jwt.sign(
      { _id: agent._id.toString() },
      process.env.JWT_SECRET,
      { expiresIn: 300 }
    );

    const emailData = {
      email: agent.email,
      name: agent.name,
      templateId: process.env.EMAIL_VERIFICATION_ID,
      variableData: {
        token,
      },
      subject: "EMAIL VERIFICATION",
    };
    verificationMail(emailData);

    res.send(appResponse("Email sent successfully"));
  }

  async getAgentByLink(req, res) {
    const { public_link } = req.params;

    const agent = await agentModel.findOne({ public_link });
    if (!agent) throw new NotFoundError("Agent doesn't exist");

    res.send(appResponse("Agent retrieved successfully", agent));
  }

  async updateAgentStats(req, res) {
    const data = req.body;
    const agent = await User.findById(data.agent);
    if (!data)
      throw new BadRequestError("Pass at least one property to be updated");
    if (!agent || agent.userType !== "Agent")
      throw new NotFoundError("Invalid user. User must be a registered agent");
    const stats = await statsModel.findOne({ agent: data.agent });
    if (!stats) throw new NotFoundError("Stats don't exist");
    const updatedData = await statsModel.findByIdAndUpdate(stats._id, data);
    res.send(appResponse("Agent stats updated successfully", updatedData));
  }

  async getAgentStats(req, res) {
    const { agentId } = req.query;
    const agent = await User.findById(agentId);
    if (!agentId) throw new BadRequestError("Pass at least one property");
    if (!agent || agent.userType !== "Agent")
      throw new NotFoundError("Invalid user. User must be a registered agent");

    const stats = await statsModel.findOne({ agent: agentId });
    res.send(appResponse("Agent stats retrieved successfully", stats));
  }

  async updateUniversityCommission(req, res) {
    const neededData = req.body;
    // the body is an array.
    const updatedCommissions = await agentService.updateCommission(
      req.user,
      neededData
    );

    res.send(
      appResponse("updated Commissions successfully", updatedCommissions)
    );
  }
  async statisticsNav(req, res) {
    const navbarStats = await agentService.statisticsNav(req.user);

    res.send(appResponse("fetched statistics successfully", navbarStats));
  }
}

module.exports = new Agent();

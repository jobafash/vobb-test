const preLaunchService = require("./../services/PreLauncAgentService");
const campaign = require("./../services/PreLauncAgentServiceCopy");
const appResponse = require("./../../lib/appResponse");
const { campaignEmails } = require("./../utils/mailer");
// const AgentsStatistics = require("../models/AgentsStatistics");

class PreLaunchAgents {
  async CreatePrelaunchAgents(req, res) {
    if (!req?.file?.path)
      throw new BadRequestError(
        "invalid file path. please that the file is an excel sheet"
      );

    const result = await preLaunchService.CreatePrelaunchAgents(req.file.path);
    if (!result)
      throw new InternalServerError(
        "Something went wrong when uploading all Employee data"
      );

    res.status(200).send(appResponse("inserted successfully", result));
  }

  async fetchAllPreLunchedAgents(req, res) {
    const allPrelaunchedAgents = await preLaunchService.fetchAllAgents();
    res.send(appResponse("Fetched agents successfully", allPrelaunchedAgents));
  }

  async SendCampaignEmails(req, res) {
    const preAgents = await campaign.fetchAllAgents();
    let count = 0;
    for (let i = 0; i<preAgents.length; i++) {
      if (preAgents[i].email && preAgents[i].email.includes("|")) {
        preAgents[i].email.split("|").map((email) => {
          const data = {
            country: preAgents[i].country || "your Region",
          };
          campaignEmails(email, data, "d-665d955308bc44aa8d3ade13e3b19a57");
        });
      }

      if (preAgents[i].email && !preAgents[i].email.includes("|")) {
        const data = {
          country: preAgents[i].country || "your Region",
        };
        campaignEmails(preAgents[i].email, data, "d-665d955308bc44aa8d3ade13e3b19a57")
      }
      if(count === 100){
        break
      };
      
      count += 1
    }
    res.send(appResponse("campaign started successfully", count))
  }

  async sendSms() {
    sms();
  }
}

module.exports = new PreLaunchAgents();

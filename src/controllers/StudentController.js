const bcrypt = require("bcrypt");
const sharp = require("sharp");
const jwt = require("jsonwebtoken");
const reportModel = require("./../models/ReportModel");
const { studentModel, User } = require("./../models/UserModel");
const { hashPassword } = require("./../middlewares/Auth");
const axios = require("axios");
const agentService = require("./../services/AgentService");
const supportService = require("./../services/SupportService");
const studentService = require("./../services/StudentService");
//const studentSavedAgentService = require("./../services/StudentSavedAgentService");
const agentReviewsService = require("./../services/AgentReviewService");
const appResponse = require("./../../lib/appResponse");
const {
  BadRequestError,
  InvalidError,
  InternalServerError,
  UnAuthorizedError,
  DuplicateError,
  NotFoundError,
} = require("./../../lib/appErrors");
const {
  verificationMail,
} = require("./../utils/mailer");
const {
  SignUpValidation,
} = require("./../validators/StudentProfileValidator");
const { checkUploadFileType } = require('../utils/uploader');

class Student {
  async createStudent(req, res) {
    if (req.file) {
      const image = await checkUploadFileType(req.file.mimetype);
      if (image === 'invalidImageType') {
        throw new BadRequestError("Invalid image type");
      }
      req.body.avatar = req.file.path;
    }
    const payload = req.body;
    const result = await SignUpValidation.validateAsync(payload);
    const data = {
      ...payload,
    };
    const isExisting = await User.findOne({ email: payload.email });
    if (isExisting) throw new DuplicateError("User exists already");

    data.password = await hashPassword(data.password);

    const savedUser = await studentService.create(data);
    if (!savedUser) throw new InternalServerError("something went wrong in saving the user");

    const token = jwt.sign(
      { _id: savedUser._id.toString() },
      process.env.PASSWORD_TOKEN,
      { expiresIn: 300 }
    );
    const emailData = {
      email: data.email,
      name: data.name,
      templateId: process.env.EMAIL_VERIFICATION_ID,
      variableData: {
        token
      },
      subject: "EMAIL VERIFICATION",
    };
    
    verificationMail(emailData);

    return res.send(appResponse("student created successfully", { savedUser }));
  }

  // async profilePic(req, res) {
  //   // if (!req.user.isVerified)
  //   //   throw new UnAuthorizedError("User is not Verified");
  //   if (!req?.file) throw new BadRequestError("No file path was recognised");
  //   const buffer = await sharp(req.file.buffer)
  //     .resize({ width: 250, height: 250 })
  //     .png()
  //     .toBuffer();
  //   req.user.avatar = buffer;
  //   await req.user.save();

  //   return res.send(appResponse("profile image saved successfully"));
  // }

  async resendEmail(req, res) {
    const { email } = req.body;
    const student = await studentService.findStudentByEmail(email);
    if (!student) throw new NotFoundError("User does not exist");

    const token = jwt.sign(
      { _id: student._id.toString() },
      process.env.PASSWORD_TOKEN,
      { expiresIn: 300 }
    );

    const emailData = {
      email: student.email,
      name: student.name,
      templateId: process.env.EMAIL_VERIFICATION_ID,
      variableData: {
        token
      },
      subject: "EMAIL VERIFICATION",
    };

    verificationMail(emailData);

    res.send(appResponse("email sent successfully"));
  }

  // async getProfilePics(req, res) {
  //   const { id } = req.params;

  //   try {
  //     const student = await studentService.findStudentById(id);

  //     if (!student) throw new NotFoundError("Agent does not exist");

  //     res.set("Content-Type", "image/jpg");
  //     res.send(student.avatar);
  //   } catch (e) {}
  // }

  // async deleteProfilePics(req, res) {
  //   req.user.avatar = undefined;

  //   await req.user.save();

  //   res.send(appResponse("deleted avatar"));
  // }
  // async forgotPassword(req, res) {
  //   const user = await studentModel.findOne({ email });
  //   if (!user) throw new NotFoundError("invalid user ");

  //   const token = jwt.sign(
  //     { _id: user._id.toString() },
  //     process.env.PASSWORD_TOKEN,
  //     { expiresIn: 300 }
  //   );

  //   const emailData = {
  //     email: email,
  //     name: user.name,
  //     templateId: process.env.EMAIL_VERIFICATION_ID,
  //     subject: "EMAIL VERIFICATION",
  //   };
  //   passwordResetMail(emailData);

  //   await user.save();

  //   res.send(appResponse("Email sent successfully"));
  // }
  // async resetPassword(req, res) {
  //   const { token } = req.query;
  //   const { newPassword, confirmPassword } = req.body;
  //   if (newPassword !== confirmPassword)
  //     throw new InvalidError("Passwords do not match");
  //   const decoded = await jwt.verify(token, process.env.PASSWORD_TOKEN);
  //   const user = await studentService.findStudentById(decoded._id);
  //   if (!user) throw new NotFoundError("Invalid user");

  //   user.password = newPassword;
  //   await user.save();

  //   res.send(appResponse("password updated successfully", user));
  // }

  // async getMyprofile(req, res) {
  //   res.send(appResponse("fetched profile successfully", req.user));
  // }

  async getAgentsByCountry(req, res) {
    const { agents, availablePages } = await agentService.findAgents(req.query);
    let fetchedAgents = [];
    const ip = req.ip;
  
    const location = await axios.get(
      `http://ip-api.com/json/${ip}`
    );

    for (let i = 0; i < agents.length; i++) {
      if (agents[i].agentCountry == location.country) {
        fetchedAgents.push(agents[i]);
      }
    }
    for (let i = 0; i < agents.length; i++) {
      if (!(agents[i].agentCountry == location.country)) {
        fetchedAgents.push(agents[i]);
      }
    }
    const AgentCount = fetchedAgents.length;

    res.send(
      appResponse("fetched Agents successfully", { availablePages, AgentCount, fetchedAgents })
    );
  }
  async getAgentsByRating(req, res) {
    const {
      pageNo,
      availablePages,
      fetchedAgents,
    } = await agentService.updateAgents(req.query);

    fetchedAgents.sort((a, b) => (a.score > b.score ? -1 : b.score > a.score ? 1 : 0));

    res.send({
      data: "Agents data ",
      currentPage: pageNo,
      availablePages: availablePages,
      fetchedAgents
    });
  }

  // async updateProfile(req, res) {
  //   // if (!req.user.isVerified)
  //   //   throw new UnAuthorizedError("User is not Verified");
  //   const updatedProfile = await studentService.updateProfile(
  //     req.user._id,
  //     req.body
  //   );
  //   res.send(appResponse("updated User Profile successfully", updatedProfile));
  // }

  // async updatePassword(req, res) {
  //   const passwordUpdate = await studentService.checkUpdatePassword(
  //     req.user,
  //     req.body
  //   );
  //   if (!passwordUpdate) throw new InternalServerError("something went wrong");
  //   res.send(appResponse("agent updated successfully"));
  // }

  // async deleteStudent(req, res) {
  //   // if (!req.user.isVerified)
  //   //   throw new UnAuthorizedError("User is not Verified");
  //   try {
  //     req.user.remove();
  //     res.send(appResponse("User deleted successfully", req.user));
  //   } catch (e) {
  //     throw new InternalServerError("something went wrong");
  //   }
  // }

  // async saveAgent(req, res) {
  //   // check if the agent exist.
  //   // then create the agent with the data gotten
  //   const { agentId } = req.query;
  //   const isExisting = await agentService.findAgentById(agentId);
  //   if (!isExisting) throw new NotFoundError("Agents with Id does not exist");

  //   const ifSaved = await studentSavedAgentService.CheckExistingSavedAgents(
  //     req.user,
  //     agentId
  //   );
  //   if (ifSaved) throw new DuplicateError("Agent is already saved");

  //   const data = {
  //     agent: agentId,
  //     student: req.user._id,
  //   };
  //   const saveAnAgent = await studentSavedAgentService.saveAgent(data);

  //   res.send(appResponse("saved agent successfully", saveAnAgent));
  // }

  // async fetchAllSavedAgents(req, res) {
  //   const params = req.query;
  //   const savedAgents = await studentSavedAgentService.fetchAllAgents(
  //     req.user._id,
  //     params
  //   );

  //   res.send(appResponse("fetched all saved successf0lly", savedAgents));
  // }

  // async unsaveAgent(req, res) {
  //   const { agentId } = req.query;
  //   const deletedAgent = await studentSavedAgentService.unsaveAgent(
  //     req.user,
  //     agentId
  //   );

  //   res.send(appResponse("Deleted Agent successfully", deletedAgent));
  // }

  // async fetchAnAGent(req, res) {
  //   const { id } = req.params;
  //   const agent = await agentService.findAgentById(id);
  //   if (!agent) throw new NotFoundError("Agent not found");

  //   res.send(appResponse("fetched agent successfully", agent));
  // }

  async searchForAgent(req, res) {
    const params = req.query;
    const agentSearch = await studentService.searchForAgent(params);

    res.send(appResponse("fetched agents successfully", agentSearch));
  }

  async supportMessage(req, res) {
    const data = {
      ...req.body,
      user: req.user._id,
    };

    const newSupport = await supportService.supportMessage(data);

    res.send(appResponse("Support send successfully", newSupport));
  }
  async createReport(req, res) {
    const user = req.user;
    const reportContent = req.file.path;
    const { report } = req.body;
    const userexist = await studentModel.findById(user._id);
    if (!userexist)
      return res.status(400).send({ message: "User does not exist" });
    const Report = new reportModel({
      student: user._id,
      report: report,
      reportContent: reportContent,
    });
    Report.save()
      .then(() => {
        return res.json({ message: `Reported successfully` });
      })
      .catch((err) => {
        res.status(400).json({ message: "Failed to report agent", err });
      });
  }

  async fetchAllReports(req, res) {
    const reports = await reportModel.find({});

    res.send(appResponse("fetched reports successfully", reports));
  }

  // async leaveAreview(req, res) {
  //   const { agentId } = req.query;
  //   const Requireddata = { ...req.body, agent: agentId, student: req.user._id };
  //   const createReview = await agentReviewsService.createAReview(Requireddata);

  //   res.send(appResponse("Sent Review Successfully", createReview));
  // }

  // async fetchAllAgentReviews(req, res) {
  //   const params = req.query;
  //   const reviews = await agentReviewsService.fetchAllAgentReviews(params);

  //   res.send(reviews)
  // }
}

module.exports = new Student();

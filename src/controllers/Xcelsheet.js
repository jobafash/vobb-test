const XLSX = require("xlsx");
const Excel = require("exceljs");

class xcelsheet {
  async redefineXcell(req, res) {
    const workbook = XLSX.readFile(req.file.path);
    const worksheet = workbook.Sheets[workbook.SheetNames[0]];
    const header = [
      "ID",
      "frim",
      "phone",
      "address",
      "website",
      "email",
      "category",
      "search",
      "country",
    ];

    let result = XLSX.utils.sheet_to_json(worksheet, {
      header,
      raw: true,
    });

    result = result.slice(1);

    let withoutEmail = [];
    let nonMulti = [];
    let multiCorrected = [];
    let agentsWithPlus = [];
    let agentsWithoutPlus = [];

    for (let agents of result) {
      if (!agents.email) {
        withoutEmail.push({
          frim: agents.frim,
          phone: agents.phone,
          website: agents.website,
          email: "null emaill",
          country: agents.country,
        });
      } else if (agents.email && agents.email.includes("|")) {
        let newEmail = agents.email.split("|");

        if (agents.phone && agents.phone.includes("+")) {
          const data = {
            frim: agents.frim,
            email: newEmail[0],
            phone: agents.phone,
            country: agents.country,
          };
          agentsWithPlus.push(data);
        } else if (agents.phone && !agents.phone.includes("+")) {
          const data = {
            frim: agents.frim,
            email: newEmail[0],
            phone: "+" + agents.phone,
            country: agents.country,
          };
          agentsWithoutPlus.push(data);
        }
        const data = {
          frim: agents.frim,
          email: newEmail[0],
          phone: agents.phone,
          country: agents.country,
        };
      } else {
        if (agents.phone && agents.phone.includes("+")) {
          const data = {
            frim: agents.frim,
            email: agents.email,
            phone: agents.phone,
            country: agents.country,
          };
          agentsWithPlus.push(data);
        } else if (agents.phone && !agents.phone.includes("+")) {
          const data = {
            frim: agents.frim,
            email: agents.email,
            phone: "+" + agents.phone,
            country: agents.country,
          };
          agentsWithoutPlus.push(data);
        }
        const data = {
          frim: agents.frim,
          phone: agents.phone,
          website: agents.website,
          email: agents.email,
          country: agents.country,
        };
        nonMulti.push(data);
      }
    }

    const mainList = agentsWithPlus.concat(agentsWithoutPlus);

    // ############################################################33
    const newWorkbook = new Excel.Workbook();
    let newWorksheet = newWorkbook.addWorksheet("Agents");

    newWorksheet.columns = [
      { header: "Agency name", key: "frim" },
      { header: "email", key: "email" },
      { header: "phone", key: "phone" },
      //   { header: "website", key: "website" },
      { header: "st", key: "country" },
    ];

    newWorksheet.columns.forEach((column) => {
      column.width = column.header.length < 50 ? 50 : column.header.length;
    });

    // Dump all the data into Excel
    mainList.forEach((e, index) => {
      // row 1 is the header.
      const rowIndex = index + 2;

      // By using destructuring we can easily dump all of the data into the row without doing much
      // We can add formulas pretty easily by providing the formula property.
      newWorksheet.addRow({
        ...e,
        amountRemaining: {
          formula: `=C${rowIndex}-D${rowIndex}`,
        },
        percentRemaining: {
          formula: `=E${rowIndex}/C${rowIndex}`,
        },
      });
    });

    const totalNumberOfRows = newWorksheet.rowCount;

    newWorksheet.addRow([
      "",
      "Total",
      {
        formula: `=sum(C2:C${totalNumberOfRows})`,
      },
      {
        formula: `=sum(D2:D${totalNumberOfRows})`,
      },
      {
        formula: `=sum(E2:E${totalNumberOfRows})`,
      },
      {
        formula: `=E${totalNumberOfRows + 1}/C${totalNumberOfRows + 1}`,
      },
    ]);

    newWorkbook.xlsx.writeFile("NoEmail.xlsx");

    res.send({
      message: "Created excel file successfully",
      success: true
    })
  }
}

module.exports = new xcelsheet();

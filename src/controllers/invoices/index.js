const InvoiceServices = require("./../../services/invoices/");
const appResponse = require("./../../../lib/appResponse");

class Invoices {
  async generateInvoice(req, res) {
    // Commissions list: title: passport number-name-university, quantity, price
    const agent = req.user;
    const listOfCommissions = req.body.commissions;

    const createdInvoice = await InvoiceServices.generateInvoice(
      agent,
      listOfCommissions
    );

    res.send(appResponse("Generated Invoices Successfully", createdInvoice))
  }

  async unprocessedInvoice(req, res) {
    const param = req.query
    let invoices;
    if (param.type === "unprocessed") {
      invoices = await InvoiceServices.unprocessedInvoice(req.user, param)
    }else {
      invoices = await InvoiceServices.processInvoices(req.user, param)
    }
   

    res.send(appResponse("fetched Invoices successfully", invoices))
  }

  async editInvoice(req, res) {

  }
}

module.exports = new Invoices()
const { User, agentModel } = require("./models/UserModel");
require("dotenv").config();
require("./db/DB");
const agents = require("./db/agents");


const importData = async () => {
  try {
    await agentModel.deleteMany();

    const createdUsers = await agentModel.insertMany(agents)

    console.log('Data Imported!')
    process.exit()
  } catch (error) {
    console.error(`${error}`)
    process.exit(1)
  }
}

const destroyData = async () => {
  try {
    await agentModel.deleteMany();
    await User.deleteMany();

    console.log('Data Destroyed!');
    process.exit()
  } catch (error) {
    console.error(`${error}`)
    process.exit(1)
  }
}

if (process.argv[2] === '-d') {
  destroyData()
} else {
  importData()
}

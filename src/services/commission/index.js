const commissionModel = require("./../../models/commissions/index");
const { InvalidError, NotFoundError } = require("./../../../lib/appErrors");
const agentSupportModel = require("./../../models/AgentsSupportingModel");
class CommissionService {
  constructor(logger) {
    this.logger = logger;
  }

  /**
   * Get all commissions
   */
  async get(user, params) {
    //Paginate
    return new Promise(async (resolve, reject) => {
      let { pageNo, noOfResults, type } = params;
      pageNo = pageNo ? Number(pageNo) : 1;
      noOfResults = noOfResults ? Number(noOfResults) : 10;

      let results, count, availablePages, reqList, reqListCommissionTotal;
      let totalCommission = 0;

      if (type == "payable") {
        reqList = await commissionModel
          .find({ "sendingAgent._id": user._id })
          .populate({
            path: "sendingAgent._id",
            model: "Agent",
          })
          .populate({
            path: "application",
            model: "Application",
            populate: {
              path: "university",
              model: "AgentAffiliated_University",
            },
          })
          .populate({
            path: "receivingAgent._id",
            model: "Agent",
          });
        reqListCommissionTotal = await commissionModel.find({
          "sendingAgent._id": user._id,
          "sendingAgent.status": "payable",
        });
        count = reqList.length;
        availablePages = Math.ceil(count / noOfResults);
        // if(req.query.status){} Query w/ status here or do it in final arr
        results = await commissionModel
          .find({ "sendingAgent._id": user._id })
          .populate({
            path: "sendingAgent._id",
            model: "Agent",
          })
          .populate({
            path: "application",
            model: "Application",
            populate: {
              path: "university",
              model: "AgentAffiliated_University",
            },
          })
          .populate({
            path: "receivingAgent._id",
            model: "Agent",
          })
          .limit(noOfResults)
          .skip((pageNo - 1) * noOfResults);

        reqListCommissionTotal.forEach((commission) => {
          totalCommission = totalCommission + commission.commission;
        });

        return resolve({ totalCommission, pageNo, availablePages, results });
      } else if (type == "receivable") {
        reqList = await commissionModel
          .find({
            "receivingAgent._id": user._id,
          })
          .populate({
            path: "sendingAgent._id",
            model: "Agent",
          })
          .populate({
            path: "application",
            model: "Application",
            populate: {
              path: "university",
              model: "AgentAffiliated_University",
            },
          })
          .populate({
            path: "receivingAgent._id",
            model: "Agent",
          });
        reqListCommissionTotal = await commissionModel.find({
          "receivingAgent._id": user._id,
          "receivingAgent.status": "unconfirmed",
        });
        count = reqList.length;
        availablePages = Math.ceil(count / noOfResults);
        results = await commissionModel
          .find({ "receivingAgent._id": user._id })
          .populate({
            path: "sendingAgent._id",
            model: "Agent",
          })
          .populate({
            path: "application",
            model: "Application",
            populate: {
              path: "university",
              model: "AgentAffiliated_University",
            },
          })
          .populate({
            path: "receivingAgent._id",
            model: "Agent",
          })
          .limit(noOfResults)
          .skip((pageNo - 1) * noOfResults);

        reqListCommissionTotal.forEach((commission) => {
          totalCommission += commission.commission;
        });
        return resolve({ totalCommission, pageNo, availablePages, results });
      } else {
        return reject({
          message:
            "Invalid type specified for partnerships. Allowed types are payable and receivable",
        });
      }
    });
  }

  /**
   * Get single commission
   */
  async getSingle(params) {
    return new Promise(async (resolve, reject) => {
      const id = params.id;
      const partnership = await commissionModel.findById(id);
      if (!partnership)
        return reject({
          message: "Invalid ID",
        });
      return resolve(partnership);
    });
  }

  /**
   * Update commission status
   */
  async update(params, payload) {
    return new Promise(async (resolve, reject) => {
      const id = params.id;
      const commission = await commissionModel.findOne({ _id: id });

      if (!commission)
        return reject({
          message: "Invalid ID",
        });
      if (payload.status === "payable" || payload.status === "unconfirmed") {
        return reject({
          message: "Cannot update to selected status",
          status: false,
        });
      }

      if (payload.status === "paid") {
        commission.sendingAgent.status = payload.status;
        await commission.save();
      }
      if (payload.status === "confirmed") {
        if (commission.sendingAgent.status !== "paid")
          return reject({
            message: "you can not confirm. Sender has not paid yet",
            status: false,
          });

        commission.receivingAgent.status = payload.status;
        await commission.save();
      }
      return resolve({
        commission,
      });
    });
  }

  async searchAndFilter(user, param) {
    let { pageNo, noOfAgents, search, status, type } = param;
    pageNo = pageNo ? Number(pageNo) : 1;
    noOfAgents = noOfAgents ? Number(noOfAgents) : 10;

    const query =
      typeof search !== "undefined" ? search.trim().toLowerCase() : "";

    const rgx = (pattern) => new RegExp(`.*${pattern}.*`);
    const searchRgx = rgx(query);

    let commissionTotal;
    let availablePages;
    let fetchedResult;
    let findCommissionByStatus;
    let totalCommission = 0;

    if (search && !status) {
      if (!["payable", "receivable"].includes(type))
        throw new InvalidError(
          "Invalid type passed. can be either payable or receivable"
        );
      if (type === "payable") {
        let clientRequiredArray = [];
        fetchedResult = await commissionModel.aggregate([
          {
            $lookup: {
              from: "applications",
              as: "agentApplication",
              let: { applicationID: "$application" },
              pipeline: [
                {
                  $match: {
                    $expr: { $and: [{ $eq: ["$_id", "$$applicationID"] }] },
                  },
                },
                {
                  $lookup: {
                    from: "agentaffiliated_universities",
                    as: "agentUniversity",
                    let: { universityID: "$university" },
                    pipeline: [
                      {
                        $match: {
                          $expr: {
                            $and: [{ $eq: ["$_id", "$$universityID"] }],
                          },
                        },
                      },
                    ],
                  },
                },
              ],
            },
          },
          {
            $lookup: {
              from: "agents",
              localField: "receivingAgent._id",
              foreignField: "_id",
              as: "agent",
            },
          },
          { $unwind: "$agent" },
          { $unwind: "$agentApplication" },
          { $unwind: "$agentApplication.agentUniversity" },
          {
            $match: {
              $and: [
                { "sendingAgent._id": user._id },
                {
                  $or: [
                    { "agentApplication.name": searchRgx },
                    { "agentApplication.passportNumber": search },
                    {
                      "agentApplication.agentUniversity.university": searchRgx,
                    },
                    { "agent.name": searchRgx },
                  ],
                },
              ],
            },
          },
        ]);

        for (let agent of fetchedResult) {
          clientRequiredArray.push({
            sendingAgent: {
              _id: agent.sendingAgent._id,
              status: agent.sendingAgent.status,
            },
            application: {
              ...agent.agentApplication,
              university: agent.agentApplication.agentUniversity,
            },
            commission: agent.commission,
            receivingAgent: {
              _id: {
                ...agent.agent,
              },
              status: agent.receivingAgent.status,
            },
          });
        }

        fetchedResult = clientRequiredArray;
      } else {
        let clientRequiredArray = [];
        fetchedResult = await commissionModel.aggregate([
          {
            $lookup: {
              from: "applications",
              as: "agentApplication",
              let: { applicationID: "$application" },
              pipeline: [
                {
                  $match: {
                    $expr: { $and: [{ $eq: ["$_id", "$$applicationID"] }] },
                  },
                },
                {
                  $lookup: {
                    from: "agentaffiliated_universities",
                    as: "agentUniversity",
                    let: { universityID: "$university" },
                    pipeline: [
                      {
                        $match: {
                          $expr: {
                            $and: [{ $eq: ["$_id", "$$universityID"] }],
                          },
                        },
                      },
                    ],
                  },
                },
              ],
            },
          },

          {
            $lookup: {
              from: "agents",
              localField: "sendingAgent._id",
              foreignField: "_id",
              as: "agent",
            },
          },
          { $unwind: "$agent" },
          { $unwind: "$agentApplication" },
          { $unwind: "$agentApplication.agentUniversity" },
          {
            $match: {
              $and: [
                { "receivingAgent._id": user._id },
                {
                  $or: [
                    { "agentApplication.name": searchRgx },
                    { "agentApplication.passportNumber": search },
                    {
                      "agentApplication.agentUniversity.university": searchRgx,
                    },
                    { "agent.name": searchRgx },
                  ],
                },
              ],
            },
          },
        ]);
        for (let agent of fetchedResult) {
          clientRequiredArray.push({
            sendingAgent: {
              _id: { ...agent.agent },
              status: agent.sendingAgent.status,
            },
            application: {
              ...agent.agentApplication,
              university: agent.agentApplication.agentUniversity,
            },
            commission: agent.commission,
            receivingAgent: {
              _id: agent.receivingAgent._id,
              status: agent.receivingAgent.status,
            },
          });
        }

        fetchedResult = clientRequiredArray;
      }
    } else if (status && search) {
      if (!["payable", "receivable"].includes(type))
        throw new InvalidError(
          "Invalid type passed. can be either payable or receivable"
        );
      if (type === "payable") {
        let clientRequiredArray = [];
        fetchedResult = await commissionModel.aggregate([
          {
            $lookup: {
              from: "applications",
              as: "agentApplication",
              let: { applicationID: "$application" },
              pipeline: [
                {
                  $match: {
                    $expr: { $and: [{ $eq: ["$_id", "$$applicationID"] }] },
                  },
                },
                {
                  $lookup: {
                    from: "agentaffiliated_universities",
                    as: "agentUniversity",
                    let: { universityID: "$university" },
                    pipeline: [
                      {
                        $match: {
                          $expr: {
                            $and: [{ $eq: ["$_id", "$$universityID"] }],
                          },
                        },
                      },
                    ],
                  },
                },
              ],
            },
          },
          {
            $lookup: {
              from: "agents",
              localField: "receivingAgent._id",
              foreignField: "_id",
              as: "agent",
            },
          },
          { $unwind: "$agent" },
          { $unwind: "$agentApplication" },
          { $unwind: "$agentApplication.agentUniversity" },
          {
            $match: {
              $and: [
                { "sendingAgent._id": user._id },
                { "sendingAgent.status": status },
                {
                  $or: [
                    { "agentApplication.name": searchRgx },
                    { "agentApplication.passportNumber": search },
                    {
                      "agentApplication.agentUniversity.university": searchRgx,
                    },
                    { "agent.name": searchRgx },
                  ],
                },
              ],
            },
          },
        ]);

        for (let agent of fetchedResult) {
          clientRequiredArray.push({
            sendingAgent: {
              _id: agent.sendingAgent._id,
              status: agent.sendingAgent.status,
            },
            application: {
              ...agent.agentApplication,
              university: agent.agentApplication.agentUniversity,
            },
            commission: agent.commission,
            receivingAgent: {
              _id: {
                ...agent.agent,
              },
              status: agent.receivingAgent.status,
            },
          });
        }

        fetchedResult = clientRequiredArray;
      } else {
        let clientRequiredArray = [];
        fetchedResult = await commissionModel.aggregate([
          {
            $lookup: {
              from: "applications",
              as: "agentApplication",
              let: { applicationID: "$application" },
              pipeline: [
                {
                  $match: {
                    $expr: { $and: [{ $eq: ["$_id", "$$applicationID"] }] },
                  },
                },
                {
                  $lookup: {
                    from: "agentaffiliated_universities",
                    as: "agentUniversity",
                    let: { universityID: "$university" },
                    pipeline: [
                      {
                        $match: {
                          $expr: {
                            $and: [{ $eq: ["$_id", "$$universityID"] }],
                          },
                        },
                      },
                    ],
                  },
                },
              ],
            },
          },
          {
            $lookup: {
              from: "agents",
              localField: "sendingAgent._id",
              foreignField: "_id",
              as: "agent",
            },
          },
          { $unwind: "$agent" },
          { $unwind: "$agentApplication" },
          { $unwind: "$agentApplication.agentUniversity" },
          {
            $match: {
              $and: [
                { "receivingAgent._id": user._id },
                { "receivingAgent.status": status },
                {
                  $or: [
                    { "agentApplication.name": searchRgx },
                    { "agentApplication.passportNumber": search },
                    {
                      "agentApplication.agentUniversity.university": searchRgx,
                    },
                    { "agent.name": search },
                  ],
                },
              ],
            },
          },
        ]);
        for (let agent of fetchedResult) {
          clientRequiredArray.push({
            sendingAgent: {
              _id: { ...agent.agent },
              status: agent.sendingAgent.status,
            },
            application: {
              ...agent.agentApplication,
              university: agent.agentApplication.agentUniversity,
            },
            commission: agent.commission,
            receivingAgent: {
              _id: agent.receivingAgent._id,
              status: agent.receivingAgent.status,
            },
          });
        }

        fetchedResult = clientRequiredArray;
      }
    } else if (status && !search) {
      if (["payable", "paid"].includes(status)) {
        findCommissionByStatus = await commissionModel.find({
          "sendingAgent._id": user._id,
          "sendingAgent.status": status,
        });

        fetchedResult = await commissionModel
          .find({
            "sendingAgent._id": user._id,
            "sendingAgent.status": status,
          })
          .populate({
            path: "sendingAgent._id",
            model: "Agent",
          })
          .populate({
            path: "receivingAgent._id",
            model: "Agent",
          })
          .populate({
            path: "application",
            model: "Application",
            populate: {
              path: "university",
              model: "AgentAffiliated_University",
            },
          })
          .limit(noOfAgents)
          .skip((pageNo - 1) * noOfAgents);
      } else if (["confirmed", "unconfirmed"].includes(status)) {
        findCommissionByStatus = await commissionModel.find({
          "receivingAgent._id": user._id,
          "receivingAgent.status": status,
        });

        fetchedResult = await commissionModel
          .find({
            "receivingAgent._id": user._id,
            "receivingAgent.status": status,
          })
          .populate({
            path: "receivingAgent._id",
            model: "Agent",
          })
          .populate({
            path: "sendingAgent._id",
            model: "Agent",
          })
          .populate({
            path: "application",
            model: "Application",
            populate: {
              path: "university",
              model: "AgentAffiliated_University",
            },
          })
          .limit(noOfAgents)
          .skip((pageNo - 1) * noOfAgents);
      }
    } else {
      if (!["payable", "receivable"].includes(type))
        throw new InvalidError(
          "Invalid type passed. can be either payable or receivable"
        );

      let reqListCommissionTotal;
      if (type === "receivable") {
        findCommissionByStatus = await commissionModel.find({
          "receivingAgent._id": user._id,
        });

        fetchedResult = await commissionModel
          .find({
            "receivingAgent._id": user._id,
          })
          .populate({
            path: "receivingAgent._id",
            model: "Agent",
          })
          .populate({
            path: "sendingAgent._id",
            model: "Agent",
          })
          .populate({
            path: "application",
            model: "Application",
            populate: {
              path: "university",
              model: "AgentAffiliated_University",
            },
          })
          .limit(noOfAgents)
          .skip((pageNo - 1) * noOfAgents);

        reqListCommissionTotal = await commissionModel.find({
          "receivingAgent._id": user._id,
          "receivingAgent.status": "unconfirmed",
        });
        reqListCommissionTotal.forEach((commission) => {
          totalCommission = totalCommission + commission.commission;
        });
      } else {
        findCommissionByStatus = await commissionModel.find({
          "sendingAgent._id": user._id,
        });

        fetchedResult = await commissionModel
          .find({
            "sendingAgent._id": user._id,
          })
          .populate({
            path: "receivingAgent._id",
            model: "Agent",
          })
          .populate({
            path: "sendingAgent._id",
            model: "Agent",
          })
          .populate({
            path: "application",
            model: "Application",
            populate: {
              path: "university",
              model: "AgentAffiliated_University",
            },
          })
          .limit(noOfAgents)
          .skip((pageNo - 1) * noOfAgents);

        reqListCommissionTotal = await commissionModel.find({
          "sendingAgent._id": user._id,
          "sendingAgent.status": "payable",
        });
        reqListCommissionTotal.forEach((commission) => {
          totalCommission = totalCommission + commission.commission;
        });
      }
    }

    if (findCommissionByStatus) {
      commissionTotal = findCommissionByStatus.length;
      availablePages = Math.ceil(commissionTotal / noOfAgents);
    }

    return { totalCommission, availablePages, fetchedResult };
  }

  async updateCommissions(agent, commissionID, tuitionFees) {
    // first thing, let us find the commission to be paid.
    const commission = await commissionModel.findById(commissionID).populate({
      path: "application",
      model: "Application",
    });
    if (!commission)
      throw new NotFoundError(
        "Could not get file for commissions. Please provide a valid ID"
      );

    // we need to find the support and check for percentages for the particular university
    const supportingModel = await agentSupportModel.findOne({
      agentID: agent._id,
      universityID: commission.application.university,
    });
    if (!supportingModel)
      throw new NotFoundError("cannot find the university you are looking for");

    // at this point, we need to deal with the commissions update
    if (
      !supportingModel.universityPaymentMethod.isFixed &&
      !supportingModel.universityPaymentMethod.agentPayout.isFixed
    ) {
      const agentAmountCollected =
        (tuitionFees * supportingModel.universityPaymentMethod.received) / 100;
      const amountForPayout =
        (agentAmountCollected *
          supportingModel.universityPaymentMethod.agentPayout.payout) /
        100;

      commission.commission = amountForPayout;
      commission.tuitionFees = tuitionFees;
      commission.payout.payoutType = "percentage";
      commission.payout.amountPaid =
        supportingModel.universityPaymentMethod.agentPayout.payout;
      commission.payout.agentShare =
        supportingModel.universityPaymentMethod.received;

      await commission.save();
    } else {
      commission.payout.payoutType =
        supportingModel.universityPaymentMethod.agentPayout.isFixed === false
          ? "percentage"
          : "fixed";
      commission.commission =
        supportingModel.universityPaymentMethod.agentPayout.payout;
      commission.tuitionFees = tuitionFees;
      commission.payout.amountPaid =
        supportingModel.universityPaymentMethod.agentPayout.payout;
      commission.payout.agentShare = supportingModel.universityPaymentMethod
        .isFixed
        ? supportingModel.universityPaymentMethod.received
        : supportingModel.universityPaymentMethod.received;

      await commission.save();
    }

    return commission;
  }
}

module.exports = CommissionService;

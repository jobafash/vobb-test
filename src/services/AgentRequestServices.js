// const agentRequestModel = require("./../models/AgentRequestModel");
// const { InvalidError } = require("./../../lib/appErrors");

// class AgentRequest {
//   async createAgentRequest(data) {
//     const newAgentRequestData = new agentRequestModel(data);
//     await newAgentRequestData.save();

//     return newAgentRequestData;
//   }

//   async findAgentRequested(agentRequestId, agent) {
//     return await agentRequestModel.findOne({
//       agent: agent._id,
//       agentInAgreement: agentRequestId,
//     });
//   }

//   async findPendingAgentRequest(agentId, param) {
//     let { pageNo, noOfAgents } = param;
//     pageNo = pageNo ? Number(pageNo) : 1;
//     noOfAgents = noOfAgents ? Number(noOfAgents) : 10;
//     let fetchedAgents;
//     let agentCount;
//     let availablePages;

//     let agentList = await agentRequestModel
//       .find({ agent: agentId, status: "pending" }, "-__v -updatedAt")
//       .sort({ createdAt: -1 })
//       .populate({
//         path: "agentInAgreement",
//         model: "Agent",
//         select: { __v: 0, createdAt: 0, updatedAt: 0 },
//       });

//     fetchedAgents = await agentRequestModel
//       .find({ agent: agentId, status: "pending" }, "-__v -updatedAt")
//       .sort({ createdAt: -1 })
//       .limit(noOfAgents)
//       .skip((pageNo - 1) * noOfAgents)
//       .populate({
//         path: "agentInAgreement",
//         model: "Agent",
//         select: { __v: 0, createdAt: 0, updatedAt: 0 },
//       });

//     agentCount = agentList.length;
//     availablePages = Math.ceil(agentCount / noOfAgents);

//     return {
//       currentPage: pageNo,
//       availablePages,
//       fetchedAgents,
//     };
//   }

//   async fetchAllAgents(agentId, param) {
//     let { pageNo, noOfAgents } = param;
//     pageNo = pageNo ? Number(pageNo) : 1;
//     noOfAgents = noOfAgents ? Number(noOfAgents) : 10;
//     let fetchedAgents;
//     let agentCount;
//     let availablePages;

//     let agentList = await agentRequestModel
//       .find({ agent: agentId, status: "accepted" }, "-__v -updatedAt")
//       .sort({ createdAt: -1 })
//       .populate({
//         path: "agentInAgreement",
//         model: "Agent",
//         select: { __v: 0, createdAt: 0, updatedAt: 0 },
//       });

//     fetchedAgents = await agentRequestModel
//       .find({ agent: agentId, status: "accepted" }, "-__v -updatedAt")
//       .sort({ createdAt: -1 })
//       .limit(noOfAgents)
//       .skip((pageNo - 1) * noOfAgents)
//       .populate({
//         path: "agentInAgreement",
//         model: "Agent",
//         select: { __v: 0, createdAt: 0, updatedAt: 0 },
//       });

//     agentCount = agentList.length;
//     availablePages = Math.ceil(agentCount / noOfAgents);

//     return {
//       currentPage: pageNo,
//       availablePages,
//       fetchedAgents,
//     };
//   }

//   async parnterAgent(agent, partnerId) {
//     return await agentRequestModel.findOne({
//       agent: agent._id,
//       agentInAgreement: partnerId,
//     });
//   }

//   async deletePartner(agent, parnterAvailble) {
//     const removePartner = await agentRequestModel.findOneAndDelete({
//       agent: agent._id,
//       agentInAgreement: parnterAvailble._id,
//     });

//     return true;
//   }
// }

// module.exports = new AgentRequest();

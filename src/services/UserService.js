const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const agentModel = require("./../models/UserModel");
const statisticsService = require("./../services/AgentStatisticsService");
const partnershipModel = require("./../models/partnershipModel/");
const partnerAffiliationsModel = require("./../models/partnershipModel/PartnershipAffiliationsModel");
const directAffiliationsModel = require("./../models/partnershipModel/DirectAffiliationsModel");
const applicationModel = require("./../models/applications/");
const agentRequestService = require("./AgentRequestServices");
const agentSupportingModel = require("./../models/AgentsSupportingModel");
const agentAffiliatedUniversity = require("./../models/AgentUniversityModel");
const requestAccessModel = require("./../models/RequestingAccessAdmin");
const commissionsModel = require("./../models/commissions/index");
const {
  NotFoundError,
  BadRequestError,
  InvalidError,
  ForbiddenError,
  InternalServerError,
  DuplicateError,
} = require("./../../lib/appErrors");
const {
  partnershipRequestMail,
  NewPartnerShipRequestMail,
} = require("./../utils/mailer");

class UserService {
  async createAgent(data) {
    const newAgent = new agentModel(data);
    await newAgent.save();
    return newAgent;
  }
  async createStudent(data) {
    const newUser = new studentModel(data);
    await newUser.save();
    return newUser;
  }
  async createUniversity(data) {
    const newUniversity = new universityModel(data);
    await newUniversity.save();
    return newUniversity;
  }

  async getAll() {
    const agents = await agentModel.find({});
    return agents;
  }

  async updateAgents(param) {
    const agents = await agentModel.find({});

    // for (let i = 0; i < agents.length; i++) {
    //   let numRating;
    //   let averageRating;
    //   let score;
    //   let totalScore =
    //     parseInt(7 * agents[i].profileViews) +
    //     parseInt(5 * agents[i].inquiredRecieved) +
    //     parseInt(10 * agents[i].admission);
    //   if (!Array.isArray(agents[i])) {
    //     score = parseInt(totalScore) / 6;
    //     numRating = 0;
    //     averageRating = 0;
    //   } else {
    //     numRating = agents[i].rating.length;
    //     averageRating =
    //       parseInt(agents[i].rating.reduce((a, b) => a + b, 0)) /
    //       parseInt(numRating);
    //     totalScore =
    //       totalScore + parseInt(10 * numRating) + parseInt(10 * averageRating);
    //     score = parseInt(totalScore) / 6;
    //   }

    //   const update = {
    //     score: score,
    //     numRating: numRating,
    //     averageRating: averageRating,
    //   };
    //   const updates = await agentModel.findOneAndUpdate(
    //     { _id: agents[i]._id },
    //     update,
    //     { new: true }
    //   );
    // }

    let { pageNo, noOfAgents } = param;
    pageNo = pageNo ? Number(pageNo) : 1;
    noOfAgents = noOfAgents ? Number(noOfAgents) : 10;
    let fetchedAgents;
    let agentCount;
    let availablePages;

    let agentList = await agentModel.find({}, "-__v -updatedAt -password");

    fetchedAgents = await agentModel
      .find({}, "-__v -updatedAt -password")
      .limit(noOfAgents)
      .skip((pageNo - 1) * noOfAgents);

    agentCount = agentList.length;
    availablePages = Math.ceil(agentCount / noOfAgents);
    return {
      currentPage: pageNo,
      availablePages,
      fetchedAgents,
    };
  }
  async findAgents(param) {
    let { pageNo, noOfAgents } = param;
    pageNo = pageNo ? Number(pageNo) : 1;
    noOfAgents = noOfAgents ? Number(noOfAgents) : 10;
    let agents;
    let agentCount;
    let availablePages;

    let agentList = await agentModel.find({}, "-__v -updatedAt -password");

    if (!param) {
      agents = await agentModel.find({}, "-__v -updatedAt -password");
      availablePages = 0;
      return { agents, availablePages };
    }

    agents = await agentModel
      .find({}, "-__v -updatedAt")
      .limit(noOfAgents)
      .skip((pageNo - 1) * noOfAgents);

    agentCount = agentList.length;
    availablePages = Math.ceil(agentCount / noOfAgents);
    return { agents, availablePages };
  }
  async findAgentByEmail(email) {
    return await agentModel.findOne({ email });
  }

  async findAgentById(id) {
    return await agentModel.findById(id);
  }

  async findUser(emailVerifyToken) {
    return await agentModel.findOne({ emailVerifyToken });
  }

  async paramsToupdate(agent, data) {
    if (data.name) {
      data.organizationName = "";
    } else {
      data.name = "";
    }

    if (data.bio) {
      const bio = data.bio;
      let emailsArray = bio.match(
        /([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi
      );
      if (emailsArray != null && emailsArray.length) {
        throw new BadRequestError("Email cannot be included in bio. ");
      }

      let link = new RegExp(
        "([a-zA-Z0-9]+://)?([a-zA-Z0-9_]+:[a-zA-Z0-9_]+@)?([a-zA-Z0-9.-]+\\.[A-Za-z]{2,4})(:[0-9]+)?(/.*)?"
      );
      let arr = bio.match(link);
      if (arr != null) {
        arr = arr.filter((element) => {
          return element !== undefined;
        });
        if (arr.length) {
          throw new BadRequestError("Links cannot be included in bio. ");
        }
      }

      let phone_number_array = bio.match(/\d{9}/);
      if (phone_number_array != null && phone_number_array.length) {
        throw new BadRequestError(
          "Contact details like phone numbers cannot be included in bio. "
        );
      }
    }

    const updates = await agentModel.findOneAndUpdate({ _id: agent._id }, data);

    if (!updates) throw new NotFoundError("Cannot update");

    return updates;
  }

  async verifyAuthToken(data) {
    const decoded = jwt.verify(data, process.env.JWT_SECRET);

    return decoded;
  }

  async checkUpdatePassword(agent, data) {
    const passwordMatch = await bcrypt.compare(
      data.currentPassword,
      agent.password
    );
    if (!passwordMatch) throw new InvalidError("invalid password");
    if (data.newPassword !== data.confirmPassword)
      throw new ForbiddenError("new passwords do not match");
    // user can change password here now
    if (data.newPassword === data.confirmPassword) {
      agent.password = data.newPassword;
      await agent.save();
    }

    return agent;
  }

  async findPendingRequest(agent, params) {
    const PendingAgents = await agentRequestService.findPendingAgentRequest(
      agent._id,
      params
    );

    return PendingAgents;
  }

  async findStatistis(id) {
    // find theuser
    const viewAgent = await agentModel.findById(id);
    if (!viewAgent) throw new NotFoundError("Invalid Agent");

    const isStatisticsExisting = await statisticsService.findStatistics(id);
    if (isStatisticsExisting) {
      isStatisticsExisting.profileViews += 1;
      await isStatisticsExisting.save();
    } else {
      const agentStatistics = await statisticsService.createStatistics(id);

      agentStatistics.profileViews += 1;
      await agentStatistics.save();
    }

    return viewAgent;
  }

  async acceptOrRejectAgent(agent, payload, agentRequestingId) {
    const agentRequesting = await agentModel.findById(agentRequestingId);
    const ifPendingRequest = await agentRequestService.findAgentRequested(
      agentRequestingId,
      agent
    );
    const agentRequestingLog = await agentRequestService.findAgentRequested(
      agent._id,
      agentRequesting
    );

    if (!agentRequesting && !ifPendingRequest && !agentRequestingLog)
      throw new NotFoundError(
        "Agent does not exist or agent is not in the list"
      );

    let agentName =
      agent.organizationType === "agent" ? agent.name : agent.organizationName;
    let receiverName =
      agentRequesting.organizationType === "agent"
        ? agentRequesting.name
        : agentRequesting.organizationName;

    if (payload.response === true) {
      ifPendingRequest.status = "accepted";
      await ifPendingRequest.save();

      agentRequestingLog.status = "accepted";
      await agentRequestingLog.save();

      const data = {
        agent: agentName,
        receiver: receiverName,
        status: "accepted",
      };

      partnershipRequestMail(
        agentRequesting.email,
        data,
        process.env.PARTNERSHIP_REQUEST_MAIL
      );
    } else {
      const data = {
        agent: agentName,
        receiver: receiverName,
        status: "rejected",
      };
      partnershipRequestMail(
        agentRequesting.email,
        data,
        process.env.PARTNERSHIP_REQUEST_MAIL
      );
    }

    return "true";
  }

  async requestPartnership(agent, newAgentId, note) {
    const searchAgent = await agentModel.findById(newAgentId);
    const sendingAgent = await agentModel.findById(agent._id);
    if (!searchAgent) throw new NotFoundError("Invalid Agent");
    let sender =
      agent.organizationType === "agent" ? agent.name : agent.organizationName;
    let agentName =
      searchAgent.organizationType === "agent"
        ? searchAgent.name
        : searchAgent.organizationName;

    const AgentRequestData = await agentRequestService.findAgentRequested(
      searchAgent,
      agent
    );
    if (sendingAgent.sentRequests >= 10)
      throw new InvalidError(
        "Number of requests that can be sent by agent already exceeds 10"
      );
    if (searchAgent.receivedRequests >= 10)
      throw new InvalidError(
        "Number of requests that can be received by agent already exceeds 10"
      );

    if (
      AgentRequestData &&
      (AgentRequestData.status === "pending" ||
        AgentRequestData.status === "requested" ||
        AgentRequestData.status === "accepted")
    ) {
      throw new DuplicateError(
        "Either request is pending or the agent has verified"
      );
    }
    const requiredRequestData = {
      status: "requested",
      agentInAgreement: searchAgent._id,
      agent: agent._id,
      note: note,
    };

    const requiredPendingtData = {
      status: "pending",
      agentInAgreement: agent._id,
      agent: searchAgent._id,
    };

    const saveRequestAgentData = await agentRequestService.createAgentRequest(
      requiredPendingtData
    );
    const savePendingAgentData = await agentRequestService.createAgentRequest(
      requiredRequestData
    );

    if (!saveRequestAgentData)
      throw new InternalServerError("something went wrong");
    const data = {
      agent: agentName,
      sender,
      id: agent._id,
    };
    NewPartnerShipRequestMail(
      searchAgent.email,
      data,
      process.env.NEW_PARTNERSHIP_REQUEST
    );
    return saveRequestAgentData;
  }

  async agentOverviewStatistics(agent) {
    var staticsOverview = await statisticsService.findStatistics(agent._id);

    if (!staticsOverview) {
      staticsOverview = await statisticsService.createStatistics(agent._id);
    }

    return staticsOverview;
  }

  async fetchsavedAgents(agent, params) {
    const neededData = await agentRequestService.fetchAllAgents(
      agent._id,
      params
    );
    return neededData;
  }

  async findAllAgents() {
    return await agentModel.find({});
  }

  async DeletePartner(agent, parnterId) {
    const parnterAvailble = await agentModel.findById(parnterId);
    const ifParnters = await agentRequestService.parnterAgent(agent, parnterId);

    if (!ifParnters || !parnterAvailble)
      throw new NotFoundError(
        "Either user deoes not exist or user is not a partner"
      );
    const deletePartner = await agentRequestService.deletePartner(
      agent,
      parnterAvailble
    );
    if (!deletePartner)
      throw new InvalidError("Something went wrong deleting partner");

    return deletePartner;
  }

  // Relationship with universities
  async fetchActiveUniversities(agent) {
    let approvedAgent = await agentModel.findOne({ _id: agent._id });
    const allUnis = [];
    approvedAgent.universities.forEach((id) => {
      allUnis.push(universityModel.findById(id));
    });

    //Work on req.query.filter by country and search by name
    approvedAgent.push(allUnis);
    return approvedAgent;
  }

  async fetchSingleAgent(id) {
    const agent = await agentModel.findById(id);
    if (!agent) throw new NotFoundError("agent not found");

    let agentStatistics = await statisticsService.findStatistics(id);

    if (agentStatistics) {
      agentStatistics.profileViews += 1;
      await agentStatistics.save();
    } else {
      agentStatistics = await statisticsService.createStatistics(id);

      agentStatistics.profileViews += 1;
      await agentStatistics.save();
    }

    return agent;
  }

  async FetchWeeklyStatistics(user, time) {
    // partnerShip
    let DirectAgents = [];
    const weeklyPartnerData1 = await partnerAffiliationsModel.aggregate([
      {
        $match: {
          agentId: user._id,
          createdAt: {
            $gte: new Date(new Date().getTime() - time),
          },
        },
      },
    ]);

    const weeklyPartnerData2 = await directAffiliationsModel.aggregate([
      {
        $match: {
          agentId: user._id,
          createdAt: {
            $gte: new Date(new Date().getTime() - time),
          },
        },
      },
    ]);

    for (let agents of weeklyPartnerData2) {
      DirectAgents.push(...agents.agents);
    }
    // application
    const weeklyApplicationData1 = await applicationModel.aggregate([
      {
        $match: {
          "receivingAgent._id": user._id,
          createdAt: {
            $gte: new Date(new Date().getTime() - time),
          },
        },
      },
    ]);
    const weeklyApplicationData2 = await applicationModel.aggregate([
      {
        $match: {
          "sendingAgent._id": user._id,
          createdAt: {
            $gte: new Date(new Date().getTime() - time),
          },
        },
      },
    ]);

    // admissions
    let letTotalAdmissions = 0;
    const weeklyAdmissionData = await agentSupportingModel.aggregate([
      {
        $match: {
          agentID: user._id,
          createdAt: {
            $gte: new Date(new Date().getTime() - time),
          },
        },
      },
    ]);
    weeklyAdmissionData.forEach(
      (agent) => (letTotalAdmissions += agent.admission)
    );

    const AdmissionStats = letTotalAdmissions;
    const PartnerStats = [...new Set(weeklyPartnerData1.concat(DirectAgents))]
      .length;
    const ApplicationStats = [
      ...new Set(weeklyApplicationData1.concat(weeklyApplicationData2)),
    ].length;

    return { AdmissionStats, PartnerStats, ApplicationStats };
  }

  async fetchMonthlyStatistics(user, time) {
    // partnerShip
    let DirectAgents = [];
    const monthlyPartnerData1 = await partnerAffiliationsModel.aggregate([
      {
        $match: {
          agentId: user._id,
          createdAt: {
            $gte: new Date(new Date().getTime() - time),
          },
        },
      },
    ]);
    const monthlyPartnerData2 = await directAffiliationsModel.aggregate([
      {
        $match: {
          agentId: user._id,
          createdAt: {
            $gte: new Date(new Date().getTime() - time),
          },
        },
      },
    ]);

    for (let agents of monthlyPartnerData2) {
      DirectAgents.push(...agents.agents);
    }

    // application
    const monthlyApplicationData1 = await applicationModel.aggregate([
      {
        $match: {
          "receivingAgent._id": user._id,
          createdAt: {
            $gte: new Date(new Date().getTime() - time),
          },
        },
      },
    ]);
    const monthlyApplicationData2 = await applicationModel.aggregate([
      {
        $match: {
          "sendingAgent._id": user._id,
          createdAt: {
            $gte: new Date(new Date().getTime() - time),
          },
        },
      },
    ]);

    // admissions
    let letTotalAdmissions = 0;
    const monthlyAdmissionData = await agentSupportingModel.aggregate([
      {
        $match: {
          agentID: user._id,
          createdAt: {
            $gte: new Date(new Date().getTime() - time),
          },
        },
      },
    ]);
    monthlyAdmissionData.forEach(
      (agent) => (letTotalAdmissions += agent.admission)
    );

    const AdmissionStats = letTotalAdmissions;
    const PartnerStats = [...new Set(monthlyPartnerData1.concat(DirectAgents))]
      .length;
    const ApplicationStats = [
      ...new Set(monthlyApplicationData1.concat(monthlyApplicationData2)),
    ].length;

    return {
      AdmissionStats,
      PartnerStats,
      ApplicationStats,
    };
  }

  async FetchYearlyStatistics(user, time) {
    // partnerShip
    let DirectAgents = [];
    const yearlyPartnerData1 = await partnerAffiliationsModel.aggregate([
      {
        $match: {
          agentId: user._id,
          createdAt: {
            $gte: new Date(new Date().getTime() - time),
          },
        },
      },
    ]);

    const yearlyPartnerData2 = await directAffiliationsModel.aggregate([
      {
        $match: {
          agentId: user._id,
          createdAt: {
            $gte: new Date(new Date().getTime() - time),
          },
        },
      },
    ]);

    for (let agents of yearlyPartnerData2) {
      DirectAgents.push(...agents.agents);
    }

    // application
    const yearlyApplicationData1 = await applicationModel.aggregate([
      {
        $match: {
          "receivingAgent._id": user._id,
          createdAt: {
            $gte: new Date(new Date().getTime() - time),
          },
        },
      },
    ]);
    const yearlyApplicationData2 = await applicationModel.aggregate([
      {
        $match: {
          "sendingAgent._id": user._id,
          createdAt: {
            $gte: new Date(new Date().getTime() - time),
          },
        },
      },
    ]);

    // admissions
    let letTotalAdmissions = 0;
    const yearlyAdmissionData = await agentSupportingModel.aggregate([
      {
        $match: {
          agentID: user._id,
          createdAt: {
            $gte: new Date(new Date().getTime() - time),
          },
        },
      },
    ]);
    yearlyAdmissionData.forEach(
      (agent) => (letTotalAdmissions += agent.admission)
    );

    const AdmissionStats = letTotalAdmissions;
    const PartnerStats = [...new Set(yearlyPartnerData1.concat(DirectAgents))]
      .length;
    const ApplicationStats = [
      ...new Set(yearlyApplicationData1.concat(yearlyApplicationData2)),
    ].length;

    return { AdmissionStats, PartnerStats, ApplicationStats };
  }

  async FetchAllStatistics(user, time) {
    // partnerShip
    let DirectAgents = [];
    const allPartnerData1 = await partnerAffiliationsModel.aggregate([
      {
        $match: {
          agentId: user._id,
        },
      },
    ]);
    const allPartnerData2 = await directAffiliationsModel.aggregate([
      {
        $match: {
          agentId: user._id,
        },
      },
    ]);

    for (let agents of allPartnerData2) {
      DirectAgents.push(...agents.agents);
    }

    // application
    const allApplicationData1 = await applicationModel.aggregate([
      {
        $match: {
          "receivingAgent._id": user._id,
        },
      },
    ]);
    const allApplicationData2 = await applicationModel.aggregate([
      {
        $match: {
          "sendingAgent._id": user._id,
        },
      },
    ]);

    // admissions
    let letTotalAdmissions = 0;
    const allAdmissionData = await agentSupportingModel.aggregate([
      {
        $match: {
          agentID: user._id,
          createdAt: {
            $gte: new Date(new Date().getTime() - time),
          },
        },
      },
    ]);
    allAdmissionData.forEach(
      (agent) => (letTotalAdmissions += agent.admission)
    );

    const AdmissionStats = letTotalAdmissions;
    const PartnerStats = [...new Set(allPartnerData1.concat(DirectAgents))]
      .length;
    const ApplicationStats = [
      ...new Set(allApplicationData1.concat(allApplicationData2)),
    ].length;

    return { AdmissionStats, PartnerStats, ApplicationStats };
  }

  async fetchEightApplications(user) {
    let receivedData = [];
    let sentData = [];
    const recievedApplicationData = await applicationModel
      .find({ "receivingAgent._id": user._id })
      .populate({
        path: "university",
        model: "AgentAffiliated_University",
      })
      .populate({
        path: "sendingAgent._id",
        model: "Agent",
      })
      .limit(4)
      .sort({ createdAt: -1 });

    const sentAplicationData = await applicationModel
      .find({ "sendingAgent._id": user._id })
      .populate({
        path: "university",
        model: "AgentAffiliated_University",
      })
      .populate({
        path: "receivingAgent._id",
        model: "Agent",
      })
      .limit(4)
      .sort({ createdAt: -1 });

    for (let agent of recievedApplicationData) {
      receivedData.push({ agent, populate: "sendingAgent" });
    }
    for (let agent of sentAplicationData) {
      sentData.push({ agent, populate: "receivingAgent" });
    }
    const total = receivedData.concat(sentData);

    return total;
  }

  async completelyEraseDetails(user) {
    // deleting from agent support model
    const checkforAgentSupport = await agentSupportingModel.find({
      agentID: user._id,
    });

    if (checkforAgentSupport && checkforAgentSupport.length > 0) {
      for (let data of checkforAgentSupport) {
        await data.remove();
      }
    }

    // deleting from agent university affiliations
    const agentInUniversity = await agentAffiliatedUniversity.find({});

    if (agentInUniversity && agentInUniversity.length > 0) {
      for (let everyUni of agentInUniversity) {
        if (everyUni.agents.length > 0) {
          const agentIndex = everyUni.agents.findIndex(
            (eachAgent) => String(eachAgent) === String(eachAgent)
          );
          if (agentIndex > 0) {
            everyUni.agents.splice(agentIndex, 1);
          }
        }

        await everyUni.save();
      }
    }

    // deleting all direct access.
    const userDirectAffil = await directAffiliationsModel.find({
      agentId: user._id,
    });

    if (userDirectAffil && userDirectAffil.length > 0) {
      for (let agents of userDirectAffil) {
        await agents.remove();
      }
    }

    // removing user from other direct affiliates.
    const usrInDirectAffil = await directAffiliationsModel.find({});

    if (usrInDirectAffil && usrInDirectAffil.length > 0) {
      for (let agents of usrInDirectAffil) {
        if (agents.agents.length > 0) {
          const agentIndex = agents.agents.findIndex(
            (eachAgent) => String(eachAgent) === String(user._id)
          );
          if (agentIndex > -1) {
            agents.agents.splice(agentIndex, 1);
          }
        }

        await agents.save();
      }
    }

    // deleting partnered affiliations
    let userPartnerAffil = await partnerAffiliationsModel.find({
      agentId: user._id,
    });

    if (userPartnerAffil && userPartnerAffil.length > 0) {
      for (let agents of userPartnerAffil) {
        await agents.remove();
      }
    }

    // deleting user as a partners
    let userInPartnerAffil = await partnerAffiliationsModel.find({
      partnershipAgentID: user._id,
    });

    if (userInPartnerAffil && userInPartnerAffil.length > 0) {
      for (let agents of userInPartnerAffil) {
        await agents.remove();
      }
    }

    // deleting from admin request access
    const requestAccessPoint = await requestAccessModel.findOne({
      agentRequesting: user._id,
    });

    if (requestAccessPoint) {
      await requestAccessPoint.remove();
    }

    // deleting user from applications
    // ====> as a sender
    const agentSentAppliaction = await applicationModel.findOne({
      "sendingAgent._id": user._id,
    });

    if (agentSentAppliaction && agentSentAppliaction.length > 0) {
      for (let eachApllication of agentSentAppliaction) {
        await eachApllication.remove();
      }
    }

    // ====> as a receiver
    const agentReceivedAppliactions = await applicationModel.findOne({
      "receivingAgent._id": user._id,
    });

    if (agentReceivedAppliactions && agentReceivedAppliactions.length > 0) {
      for (let eachApllication of agentReceivedAppliactions) {
        await eachApllication.remove();
      }
    }

    // deleting user from commissions
    // ====> sendable commissions
    const agentSentCommission = await commissionsModel.find({
      "sendingAgent._id": user._id,
    });
    if (agentSentCommission && agentSentCommission.length > 0) {
      for (let eachCommission of agentSentCommission) {
        await eachCommission.remove();
      }
    }

    // ====> receiveable commission
    const agentReceivedCommissions = await commissionsModel.find({
      "receivingAgent._id": user._id,
    });
    if (agentReceivedCommissions && agentReceivedCommissions.length > 0) {
      for (let eachCommission of agentReceivedCommissions) {
        await eachCommission.remove();
      }
    }

    // check in pending partnerships
    // ====> sending parternship
    const sentPartnerShips = await partnershipModel.find({
      "sendingAgent._id": user._id,
    });
    if (sentPartnerShips && sentPartnerShips.length > 0) {
      for (let everyPartner of sentPartnerShips) {
        await everyPartner.remove();
      }
    }

    // ====> receiving partnership
    const receivdPartnership = await partnershipModel.find({
      "receivingAgent._id": user._id,
    });
    if (receivdPartnership && receivdPartnership.length > 0) {
      for (let everyPartner of receivdPartnership) {
        await everyPartner.remove();
      }
    }
    return true;
  }
}
module.exports = new UserService();

// const agentReviewModel = require("./../models/AgentReviewModel");

// class AgentReviews {
//   async createAReview(data) {
//     const newReview = new agentReviewModel(data);

//     await newReview.save();

//     return newReview;
//   }
//   async fetchAllAgentReviews(param) {
//     let { pageNo, noOfReviews, agentId } = param;
//     pageNo = pageNo ? Number(pageNo) : 1;
//     noOfReviews = noOfReviews ? Number(noOfReviews) : 10;
//     let Reviews;
//     let reviewCount;
//     let availablePages;

//     let reviewList = await agentReviewModel
//       .find({ agent: agentId }, "-__v -updatedAt")
//       .sort({ createdAt: -1 })
//       .populate({
//         path: "student",
//         model: "Student",
//         select: { __v: 0, createdAt: 0, updatedAt: 0 },
//       });

//       Reviews = await agentReviewModel
//       .find({ agent: agentId }, "-__v -updatedAt")
//       .sort({ createdAt: -1 })
//       .limit(noOfReviews)
//       .skip((pageNo - 1) * noOfReviews)
//       .populate({
//         path: "student",
//         model: "Student",
//         select: { __v: 0, createdAt: 0, updatedAt: 0 },
//       });

//       reviewCount = reviewList.length;
//     availablePages = Math.ceil(reviewCount / noOfReviews);

//     return {
//       currentPage: pageNo,
//       availablePages,
//       Reviews,
//     };
//   }
// }

// module.exports = new AgentReviews();

const _ = require('lodash');

class AdminDTO {

  constructor(data) {
    this.adminData = data;
  }

  isVerified() {
    return this.adminData.isVerified;
  }

  getAgent() {
    return this.adminData.agent;
  }
  
  getRequestId() {
    return this.adminData.request_id;
  }

  getUniversityId() {
    return this.adminData.university_id;
  }

  getStatus() {
    return this.adminData.status;
  }

  toJSON() {
    return Object.assign({}, this.adminData);
  }

  init(data) {
    const allowedKeys = [
      'isVerified', 
      'agent', 
      'request_id', 
      'university_id',
      'status'
    ];
    const createData = _.pick(data, allowedKeys);
    return new AdminDTO(createData);
  }
}

module.exports = AdminDTO;

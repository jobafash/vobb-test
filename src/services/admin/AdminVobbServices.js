const User = require("./../../models/UserModel");
const statsModel = require("./../../models/StatisticsModel");
const { NotFoundError, BadRequestError } = require("./../../../lib/appErrors");
const { hashPassword } = require("./../../middlewares/Auth");
const { v4 } = require("uuid");
const randomUUID = v4;
const { requestSuccessMail } = require("./../../utils/mailer");
const requestAccessModel = require("./../../models/RequestingAccessAdmin");
const universityModel = require("./../../models/AgentUniversityModel");
const directAffilliaiteModel = require("./../../models/partnershipModel/DirectAffiliationsModel");
const agentsupportingModel = require("./../../models/AgentsSupportingModel");
const AdminDTO = require("./dto/admin.dto");
const supportService = require("./../../services/SupportService");

class AdminService {
  constructor(logger) {
    this.logger = logger;
  }

  /**
   * Verify the agent
   */
  async verifyAgent(payload) {
    return new Promise(async (resolve, reject) => {
      const requestData = new AdminDTO().init(payload, this.logger);
      if (requestData.isVerified()) {
        const id = randomUUID(Math.random());
        let password = id.substr(id.length - 9);
        const user = await User.findById(requestData.getAgent());
        if (!user) throw new NotFoundError("Invalid request ID");

        const representativeName = user.name;
        const agencyName = user.agencyName;

        const emailData = {
          email: user.email,
          templateId: process.env.REQUEST_ACCEPTED_ID,
          variableData: {
            password,
            representativeName,
            agencyName,
          },
          subject: "VERIFICATION SUCCESSFUL. WELCOME TO VOBB!",
        };
        user.password = await hashPassword(password);
        requestSuccessMail(emailData);

        const stats = new statsModel({ agent: user._id });
        user.stats = stats._id;
        await stats.save();

        const request = await requestAccessModel.findOne({
          agentRequesting: requestData.getAgent(),
        });

        request.processed = true;
        user.isVerified = true;

        await user.save();
        await request.save();

        return resolve({
          status: true,
          message: "Agent verified successfully",
          user,
        });
      } else {
        return reject({
          status: false,
          message: "Agent unverified",
        });
      }
    });
  }

  /**
   * Get all access requests
   */
  async getRequests(params) {
    //Paginate
    return new Promise(async (resolve) => {
      let { pageNo, noOfRequests } = params;
      pageNo = pageNo ? Number(pageNo) : 1;
      noOfRequests = noOfRequests ? Number(noOfRequests) : 10;
      let fetchedRequests;
      let count;
      let availablePages;

      let reqList = await requestAccessModel.find({ processed: false });
      count = reqList.length;
      availablePages = Math.ceil(count / noOfRequests);
      fetchedRequests = await requestAccessModel
        .find({ processed: false })
        .populate({
          path: "agentRequesting",
          model: "Agent",
        })
        .populate({
          path: "universities.university",
          model: "AgentAffiliated_University",
          select: { __v: 0 },
        })
        .sort({ createdAt: 1 })
        .limit(noOfRequests)
        .skip((pageNo - 1) * noOfRequests);

      return resolve({ pageNo, availablePages, fetchedRequests });
    });
  }

  /**
   * Get all support messages
   */
  async getMessages(params) {
    const messages = await supportService.getMessages(params);
    return messages;
  }

  /**
   * Get support message
   */
  async getMessage(id) {
    if (!id) throw new BadRequestError("Invalid request. Provide an ID");
    const message = await supportService.getMessage(id);
    return message;
  }

  /**
   * Update support message
   */
  async updateMessage(id) {
    if (!id) throw new BadRequestError("Invalid request. Provide an ID");
    const message = await supportService.updateMessage(id);
    return message;
  }

  /**
   * Update university verification status
   */
  async updateUniversities(payload) {
    return new Promise(async (resolve) => {
      const requestData = new AdminDTO().init(payload, this.logger);

      const request = await requestAccessModel.findById(
        requestData.getRequestId()
      );

      if (!request) throw new NotFoundError("Invalid request ID");

      // at this point I have found an agent
      // I have to verify the agent and create his partnered allAffiliates

      for (let university of request.universities) {
        if (String(university.university) === requestData.getUniversityId()) {
          university.isVerified = requestData.getStatus();

          const findDirectAffil = await directAffilliaiteModel.findOne({
            agentId: request.agentRequesting,
            university: university.university,
          });
          const agent = await User.findById(request.agentRequesting);
          const agentUniversity = await universityModel.findById(
            university.university
          );

          if (requestData.getStatus() === "accepted") {
            const universityAffil = await universityModel.findOne({
              _id: university.university,
            });
            if (
              universityAffil.agents.indexOf(request.agentRequesting) === -1
            ) {
              universityAffil.agents.push(request.agentRequesting);
              await universityAffil.save();
            }

            const agentSupport = await agentsupportingModel.findOne({
              agentID: request.agentRequesting,
              universityID: university.university,
            });

            if (findDirectAffil) {
              findDirectAffil.isverified = "accepted";
            }
            await findDirectAffil.save();

            agentSupport.isVerified = true;
            await agentSupport.save();

            const emailData = {
              email: agent.email,
              templateId: process.env.REQUEST_ACCEPTED_ID,
              variableData: {
                university: agentUniversity.university,
                agency: agent.agencyName,
              },
              subject: "University Request",
            };

            requestSuccessMail(emailData);
          } else if (requestData.getStatus() === "denied") {
            if (findDirectAffil) {
              findDirectAffil.isverified = "rejected";
            }
            await findDirectAffil.save();
          } else {
            if (findDirectAffil) {
              findDirectAffil.isverified = "pending";
            }
            await findDirectAffil.save();
          }
        }
      }
      await request.save();

      return resolve({
        status: true,
        message: "Agent's universities verified successfully",
        data: request,
      });
    });
  }

  async fetchedVerifiedAgents(param) {
    let { pageNo, noOfRequests } = param;
    pageNo = pageNo ? Number(pageNo) : 1;
    noOfRequests = noOfRequests ? Number(noOfRequests) : 10;

    let fetchedRequests;
    let count;
    let availablePages;

    let listOfAgents = await User.find({ isVerified: true });

    fetchedRequests = await User.find({ isVerified: true })
      .limit(noOfRequests)
      .skip((pageNo - 1) * noOfRequests)
      .sort({ createdAt: -1 });
    count = listOfAgents.length;
    availablePages = Math.ceil(count / noOfRequests);

    return { availablePages, fetchedRequests, count };
  }

  async fetchUnverifiedAgents(param) {
    let { pageNo, noOfRequests } = param;
    pageNo = pageNo ? Number(pageNo) : 1;
    noOfRequests = noOfRequests ? Number(noOfRequests) : 10;

    let fetchedRequests;
    let count;
    let availablePages;

    let listOfAgents = await User.find({ isVerified: false });

    fetchedRequests = await User.find({ isVerified: false })
      .limit(noOfRequests)
      .skip((pageNo - 1) * noOfRequests)
      .sort({ createdAt: -1 });
    count = listOfAgents.length;
    availablePages = Math.ceil(count / noOfRequests);

    return { availablePages, fetchedRequests, count };
  }

  async deleteAgent(agentID) {
    const isUserExisting = await User.findById(agentID)
    if(!isUserExisting) {
      throw new NotFoundError("Agent " + agentID + " does not exist")
    }

   await isUserExisting.remove();

   return isUserExisting

  }
}

module.exports = AdminService;

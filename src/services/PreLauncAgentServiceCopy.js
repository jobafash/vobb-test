const preLaunchModelCopy = require("../models/PreLaunchAgentModelCopy");
const XLSX =require("xlsx")

class PreLaunchAgent {
  async CreatePrelaunchAgents(dataFile) {
    const workbook = XLSX.readFile(dataFile);
    const worksheet = workbook.Sheets[workbook.SheetNames[0]];

    const header = ["name", "email", "country"];

    let result = XLSX.utils.sheet_to_json(worksheet, {
      header,
      raw: true,
    });

    result = result.slice(1);

    try {
      await preLaunchModelCopy.insertMany(result, { ordered: false });
    } catch (error) {
      if (error.code !== 11000) {
        throw error;
      }
    }

    return result;
  }
  async fetchAllAgents(){
      const preLaunchAgents = await preLaunchModelCopy.find({"country": "Nigeria"})
      return preLaunchAgents
  }
}

module.exports = new PreLaunchAgent()
const User = require("./../../models/UserModel");
const applicationModel = require("./../../models/applications/index");
const agentSupportingModel = require("./../../models/AgentsSupportingModel");
const parnterShipAffilliaiteModel = require("./../../models/partnershipModel/PartnershipAffiliationsModel");
const { NotFoundError, BadRequestError } = require("./../../../lib/appErrors");
const commissionModel = require("./../../models/commissions/index");
const { uploadToCloud } = require("./../../../lib/cloudinary");
const {
  applicationStatusUpdate,
  newApplication,
} = require("./../../utils/mailer");
const statsModel = require("./../../models/StatisticsModel");

class AppService {
  constructor(logger) {
    this.logger = logger;
  }

  /**
   * Create a new application
   */
  async create(uploaded_files, user, payload) {
    return new Promise(async (resolve, reject) => {
      const partners = await parnterShipAffilliaiteModel.find({
        agentId: user._id,
      });

      let orignalPartner;

      for (let everyPartner of partners) {
        const index = everyPartner.universityIDs.findIndex(function (
          university
        ) {
          return String(university.university) === payload.university;
        });
        if (index > -1) orignalPartner = everyPartner;
      }

      // check if the partner is still with us.
      const isExisting = await User.findById(orignalPartner.partnershipAgentID);
      if (!isExisting)
        return reject({
          status: false,
          message: "agent no longer Exist on Vobb",
        });

      const application = await applicationModel.findOne({
        passportNumber: payload.passportNumber,
        university: payload.university,
        "sendingAgent._id": user._id,
        "receivingAgent._id": orignalPartner.partnershipAgentID,
      });

      if (
        application &&
        application.passportNumber === payload.passportNumber
      ) {
        return reject({
          message: "Application already created",
        });
      }

      let files = [];
      for (let eachFile of uploaded_files) {
        const uploadeedData = await uploadToCloud(eachFile.path);
        const neededData = {
          original_name: eachFile.originalname,
          path: uploadeedData.secure_url,
        };

        files.push(neededData);
      }

      const data = {
        ...payload,
        receivingAgent: { _id: orignalPartner.partnershipAgentID },
        sendingAgent: { _id: user._id },
        files: files,
      };

      let app;
      try {
        app = new applicationModel(data);
        await app.save();

      } catch (e) {
        return reject({
          message: e.message,
        });
      }

      const emailData = {
        email: isExisting.email,
        templateId: process.env.NEW_APPLICATION,
        variableData: {
          representative: isExisting.name,
          agency: user.agencyName,
        },
        subject: "New Application",
      };

      newApplication(emailData);
      return resolve({
        status: true,
        message: "Application created successfully",
        app,
      });
    });
  }

  /**
   * Get available universities
   */
  async getUniversities(user) {
    return new Promise(async (resolve) => {
      let neededArray = [];
      let neededUniveristy = [];
      const universities = await parnterShipAffilliaiteModel
        .find({
          agentId: user._id,
        })
        .populate({
          path: "universityIDs.university",
          model: "AgentAffiliated_University",
        });

      if (!universities) throw new NotFoundError("User has no partnerships");

      for (let eachUni of universities) {
        neededArray.push(...eachUni.universityIDs);
      }

      for (let eachUni of neededArray) {
        neededUniveristy.push(eachUni.university);
      }

      return resolve(neededUniveristy);
    });
  }

  /**
   * Get all applications
   */
  async get(user, params) {
    //Paginate
    return new Promise(async (resolve, reject) => {
      let { pageNo, noOfApps, type } = params;
      pageNo = pageNo ? Number(pageNo) : 1;
      noOfApps = noOfApps ? Number(noOfApps) : 10;

      let fetchedApps, count, availablePages, reqList;

      if (type == "sent") {
        reqList = await applicationModel.find({ "sendingAgent._id": user._id });
        count = reqList.length;
        availablePages = Math.ceil(count / noOfApps);
        // if(req.query.status){} Query w/ status here or do it in final arr
        fetchedApps = await applicationModel
          .find({ "sendingAgent._id": user._id })
          .populate({
            path: "receivingAgent._id",
            model: "Agent",
            select: { __v: 0, country: 0 },
          })
          .populate({
            path: "sendingAgent._id",
            model: "Agent",
            select: { __v: 0, country: 0 },
          })
          .populate({
            path: "university",
            model: "AgentAffiliated_University",
            select: { __v: 0, country: 0, agents: 0, _id: 0 },
          })
          .limit(noOfApps)
          .skip((pageNo - 1) * noOfApps);

        return resolve({ count, pageNo, availablePages, fetchedApps });
      } else if (type == "received") {
        reqList = await applicationModel.find({
          "receivingAgent._id": user._id,
        });
        availablePages = Math.ceil(count / noOfApps);
        fetchedApps = await applicationModel
          .find({ "receivingAgent._id": user._id })
          .populate({
            path: "sendingAgent._id",
            model: "Agent",
            select: { __v: 0, country: 0 },
          })
          .populate({
            path: "receivingAgent._id",
            model: "Agent",
            select: { __v: 0, country: 0 },
          })
          .populate({
            path: "university",
            model: "AgentAffiliated_University",
            select: { __v: 0, country: 0, agents: 0, _id: 0 },
          })
          .limit(noOfApps)
          .skip((pageNo - 1) * noOfApps);
        let needApplication = fetchedApps.filter(
          (eachApp) => eachApp.receivingAgent.status !== "payable"
        );
        count = fetchedApps.length - needApplication.length;
        fetchedApps = needApplication;
        return resolve({ count, pageNo, availablePages, fetchedApps });
      } else {
        return reject({
          message:
            "Invalid type specified for applications. Allowed types are sent and received",
        });
      }
    });
  }

  /**
   * Get single application
   */
  async getSingle(params) {
    return new Promise(async (resolve, reject) => {
      const id = params.id;
      const application = await applicationModel.findById(id).populate({
        path: "university",
        method: "AgentAffiliated_University",
      });
      if (!application)
        return reject({
          message: "Invalid ID",
        });
      if ((application.sendingAgent.status = "sent")) {
        application.sendingAgent.status = "review";
      }

      await application.save();
      return resolve(application);
    });
  }

  /**
   * Update application status
   */
  async update(user, params, payload) {
    return new Promise(async (resolve, reject) => {
      const applicationID = params.id;

      const application = await applicationModel.findOne({
        _id: applicationID,
        "receivingAgent.id": user._id,
      });

      if (!application)
        return reject({
          message: "Invalid ID",
        });

      const acceptableStatus = [
        "new",
        "submitted",
        "admission",
        "accepted",
        "payable",
      ];

      if (application.receivingAgent.status === "rejected") {
        return reject({
          message: "Application has been rejected",
          status: false,
        });
      }
      const existingStatusIndex = acceptableStatus.indexOf(
        application.receivingAgent.status
      );
      const incomingStatusIndex = acceptableStatus.indexOf(payload.status);

      if (incomingStatusIndex - existingStatusIndex > 1) {
        return reject({
          message:
            "Follow the application process: submit -> accept or reject -> admission -> payable",
          status: false,
        });
      }
      if (incomingStatusIndex > -1 && incomingStatusIndex < existingStatusIndex)
        return reject({
          message: "Cannot Down grade status",
          status: false,
        });
      if (
        (application.receivingAgent.status === "accepted" &&
          payload.status === "rejected") ||
        (application.receivingAgent.status === "rejected" &&
          payload.status === "accepted")
      ) {
        return reject({
          message: `"application is already ${application.receivingAgent.status}"`,
          status: false,
        });
      }
      if (incomingStatusIndex === existingStatusIndex)
        return reject({
          message: "Cannot grade the same status",
          status: false,
        });

      let AgentCommission = await agentSupportingModel.findOne({
        agentID: user._id,
        universityID: application.university,
      });

      if (payload.status !== "accepted" && payload.status !== "payable") {
        if (payload.status == "admission") {
          application.sendingAgent.status = payload.status;
          application.receivingAgent.status = payload.status;
          await application.save();

          AgentCommission.admission += 1;
          await AgentCommission.save();
        }

        application.sendingAgent.status = payload.status;
        application.receivingAgent.status = payload.status;
        await application.save();
      } else if (payload.status == "payable") {
        //Create commissions here and add total aggregate
        const isCommissionExisting = await commissionModel.findOne({
          application: applicationID,
          sendingAgent: { _id: user._id },
          receivingAgent: { _id: application.sendingAgent._id },
        });

        if (!isCommissionExisting) {
          const commission = new commissionModel({
            commission: AgentCommission.commission,
            application: applicationID,
            sendingAgent: { _id: user._id },
            receivingAgent: { _id: application.sendingAgent._id },
          });

          await commission.save();
          if (!commission)
            return reject({
              message: "Unable to create commission",
            });

          application.receivingAgent.status = "payable";
          await application.save();
        }
      } else {
        if (payload.status === "accepted" || payload.status === "submitted") {
          application.sendingAgent.status = payload.status;
          application.receivingAgent.status = payload.status;
          await application.save();
        }
      }

      const senderAgent = await User.findById(application.sendingAgent._id);
      const emailData = {
        email: senderAgent.email,
        templateId: process.env.PARTNERSHIP_REQUEST_STATUS,
        variableData: {
          representative: senderAgent.name,
          agency: user.agencyName,
          status: payload.status,
          studentName: application.name,
        },
        subject: "Application Status",
      };
      applicationStatusUpdate(emailData);
      return resolve({
        application,
      });
    });
  }

  async searchApplication(user, param) {
    let { pageNo, noOfApplications, status, search, type } = param;
    pageNo = pageNo ? Number(pageNo) : 1;
    noOfApplications = noOfApplications ? Number(noOfApplications) : 10;

    const query =
      typeof search !== "undefined" ? search.trim().toLowerCase() : false;
    const rgx = (pattern) => new RegExp(`.*${pattern}.*`);
    const searchRgx = rgx(query);

    let fetchedData;
    let allApplicationData;
    let applicationCount;

    if (status && !query) {
      if (!["received", "sent"].includes(type))
        throw new BadRequestError("Can not fetch with property type");

      if (type === "received") {
        allApplicationData = await applicationModel.find({
          "receivingAgent._id": user._id,
          "receivingAgent.status": status,
        });

        fetchedData = await applicationModel
          .find({
            "receivingAgent._id": user._id,
            "receivingAgent.status": status,
          })
          .populate({
            path: "sendingAgent._id",
            model: "Agent",
          })
          .populate({
            path: "university",
            model: "AgentAffiliated_University",
          })
          .limit(noOfApplications)
          .skip((pageNo - 1) * noOfApplications)
          .sort({ createdAt: -1 });
        applicationCount = allApplicationData.length;
      } else {
        allApplicationData = await applicationModel.find({
          "sendingAgent._id": user._id,
          "sendingAgent.status": status,
        });
        fetchedData = await applicationModel
          .find({
            "sendingAgent._id": user._id,
            "sendingAgent.status": status,
          })
          .populate({
            path: "receivingAgent._id",
            model: "Agent",
          })
          .populate({
            path: "university",
            model: "AgentAffiliated_University",
          })
          .limit(noOfApplications)
          .skip((pageNo - 1) * noOfApplications)
          .sort({ createdAt: -1 });
        applicationCount = allApplicationData.length;
      }

      applicationCount = allApplicationData.length;
      let count = applicationCount;
      let availablePages = Math.ceil(applicationCount / noOfApplications);

      return { count, availablePages, fetchedData };
    } else if (query && !status) {
      if (!["received", "sent"].includes(type))
        throw new BadRequestError("Can not fetch with property type");

      if (type === "received") {
        let clientRequiredArray = [];
        fetchedData = await applicationModel.aggregate([
          {
            $lookup: {
              from: "agentaffiliated_universities",
              as: "agentUniversity",
              let: { universityID: "$university" },
              pipeline: [
                {
                  $match: {
                    $expr: { $and: [{ $eq: ["$_id", "$$universityID"] }] },
                  },
                },
              ],
            },
          },
          {
            $lookup: {
              from: "agents",
              localField: "sendingAgent._id",
              foreignField: "_id",
              as: "agent",
            },
          },
          { $unwind: "$agentUniversity" },
          { $unwind: "$agent" },
          {
            $match: {
              $and: [
                { "receivingAgent._id": user._id },
                {
                  $or: [
                    { name: searchRgx },
                    { "agentUniversity.university": searchRgx },
                    { passportNumber: search },
                    { "agent.name": searchRgx },
                  ],
                },
              ],
            },
          },
        ]);

        for (let apps of fetchedData) {
          clientRequiredArray.push({
            ...apps,
            university: { ...apps.agentUniversity },
            sendingAgent: {
              _id: { ...apps.agent },
              status: apps.sendingAgent.status,
            },
          });
        }

        fetchedData = clientRequiredArray;
      } else {
        let clientRequiredArray = [];
        fetchedData = await applicationModel.aggregate([
          {
            $lookup: {
              from: "agentaffiliated_universities",
              as: "agentUniversity",
              let: { universityID: "$university" },
              pipeline: [
                {
                  $match: {
                    $expr: { $and: [{ $eq: ["$_id", "$$universityID"] }] },
                  },
                },
              ],
            },
          },
          {
            $lookup: {
              from: "agents",
              localField: "receivingAgent._id",
              foreignField: "_id",
              as: "agent",
            },
          },
          { $unwind: "$agentUniversity" },
          { $unwind: "$agent" },
          {
            $match: {
              $and: [
                { "sendingAgent._id": user._id },
                {
                  $or: [
                    { name: searchRgx },
                    { "agentUniversity.university": searchRgx },
                    { passportNumber: search },
                    { "agent.name": searchRgx },
                  ],
                },
              ],
            },
          },
        ]);

        for (let apps of fetchedData) {
          clientRequiredArray.push({
            ...apps,
            university: { ...apps.agentUniversity },
            receivingAgent: {
              _id: { ...apps.agent },
              status: apps.receivingAgent.status,
            },
          });
        }

        fetchedData = clientRequiredArray;
      }

      let count = fetchedData.length;
      return { count, fetchedData };
    } else if (query && status) {
      if (!["received", "sent"].includes(type))
        throw new BadRequestError("Can not fetch with property type");

      if (type === "received") {
        let clientRequiredArray = [];
        fetchedData = await applicationModel.aggregate([
          {
            $lookup: {
              from: "agentaffiliated_universities",
              as: "agentUniversity",
              let: { universityID: "$university" },
              pipeline: [
                {
                  $match: {
                    $expr: { $and: [{ $eq: ["$_id", "$$universityID"] }] },
                  },
                },
              ],
            },
          },
          {
            $lookup: {
              from: "agents",
              localField: "sendingAgent._id",
              foreignField: "_id",
              as: "agent",
            },
          },
          { $unwind: "$agentUniversity" },
          { $unwind: "$agent" },
          {
            $match: {
              $and: [
                { "receivingAgent._id": user._id },
                { "receivingAgent.status": status },
                {
                  $or: [
                    { name: searchRgx },
                    { "agentUniversity.university": searchRgx },
                    { passportNumber: search },
                    { "agent.name": searchRgx },
                  ],
                },
              ],
            },
          },
        ]);
        for (let apps of fetchedData) {
          clientRequiredArray.push({
            ...apps,
            university: { ...apps.agentUniversity },
            sendingAgent: {
              _id: { ...apps.agent },
              status: apps.sendingAgent.status,
            },
          });
        }

        fetchedData = clientRequiredArray;
      } else {
        let clientRequiredArray = [];
        fetchedData = await applicationModel.aggregate([
          {
            $lookup: {
              from: "agentaffiliated_universities",
              as: "agentUniversity",
              let: { universityID: "$university" },
              pipeline: [
                {
                  $match: {
                    $expr: { $and: [{ $eq: ["$_id", "$$universityID"] }] },
                  },
                },
              ],
            },
          },
          {
            $lookup: {
              from: "agents",
              localField: "receivingAgent._id",
              foreignField: "_id",
              as: "agent",
            },
          },
          { $unwind: "$agentUniversity" },
          { $unwind: "$agent" },
          {
            $match: {
              $and: [
                { "sendingAgent._id": user._id },
                { "sendingAgent.status": status },
                {
                  $or: [
                    { name: searchRgx },
                    { "agentUniversity.university": searchRgx },
                    { passportNumber: search },
                    { "agent.name": searchRgx },
                  ],
                },
              ],
            },
          },
        ]);

        for (let apps of fetchedData) {
          clientRequiredArray.push({
            ...apps,
            university: { ...apps.agentUniversity },
            receivingAgent: {
              _id: { ...apps.agent },
              status: apps.receivingAgent.status,
            },
          });
        }

        fetchedData = clientRequiredArray;
      }
      let count = fetchedData.length;
      return { count, fetchedData };
    } else {
      if (!["received", "sent"].includes(type))
        throw new BadRequestError("Can not fetch with property type");

      if (type === "received") {
        allApplicationData = await applicationModel.find({
          "receivingAgent._id": user._id,
        });

        fetchedData = await applicationModel
          .find({
            "receivingAgent._id": user._id,
          })
          .populate({
            path: "sendingAgent._id",
            model: "Agent",
          })
          .populate({
            path: "university",
            model: "AgentAffiliated_University",
          })
          .limit(noOfApplications)
          .skip((pageNo - 1) * noOfApplications)
          .sort({ createdAt: -1 });

        let needApplication = fetchedData.filter(
          (eachApp) => eachApp.receivingAgent.status !== "payable"
        );

        applicationCount = needApplication.length;
        fetchedData = needApplication;
      } else if (type === "sent") {
        allApplicationData = await applicationModel.find({
          "sendingAgent._id": user._id,
        });
        fetchedData = await applicationModel
          .find({
            "sendingAgent._id": user._id,
          })
          .populate({
            path: "receivingAgent._id",
            model: "Agent",
          })
          .populate({
            path: "university",
            model: "AgentAffiliated_University",
          })
          .limit(noOfApplications)
          .skip((pageNo - 1) * noOfApplications)
          .sort({ createdAt: -1 });
        applicationCount = allApplicationData.length;
      }

      let count = applicationCount;
      let availablePages = Math.ceil(
        allApplicationData.length / noOfApplications
      );
      return { count, availablePages, fetchedData };
    }
  }
}

module.exports = AppService;

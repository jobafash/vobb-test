// const universityModel = require("./../models/UniversityModel");
// const adminVobbService = require("./../../services/AdminVobbServices");
const bcrypt = require("bcrypt");
const { User, agentModel, universityModel } = require("./../models/UserModel");
const {
  NotFoundError,
  InvalidError,
  ForbiddenError,
} = require("./../../lib/appErrors");
const AgentStatisticsService = require("./AgentStatisticsService");
const partnershipModel = require("../models/PartnershipModel");
const { request } = require("express");

class University {
  async createUniversity(data) {
    const newUniversity = new universityModel(data);
    await newUniversity.save();

    adminVobbService.numberOfUniversities += 1;
    return newUniversity;
  }

  async findUniversityByName(name) {
    return await universityModel.findOne({ name });
  }
  async findUniversityById(id, country) {
    const University = await universityModel.findById(id);
    if (!University || University.userType !== "University")
      throw new NotFoundError("invalid user. Must be a registered university");
    let allAgents = [];
    University.agents.forEach((agentId) => {
      allAgents.push(agentModel.findById(agentId));
    });

    //Work on req.query.filter by country and search by name
    if (country !== null) {
      allAgents = allAgents.filter((element) => {
        return element.agentCountry == country;
      });
      University.allAgents = allAgents;
      return University;
    }
  }

  async fetchAllUniversities(
    accepted,
    name,
    user,
    pending,
    pageNo,
    noOfUnis,
    country
  ) {
    pageNo = pageNo ? Number(pageNo) : 1;
    noOfUnis = noOfUnis ? Number(noOfUnis) : 10;
    let agentCount;
    let availablePages;

    let allUniversities = await universityModel.find(
      {},
      "-__v -updatedAt -password"
    );
    let Universities = await universityModel
      .find({}, "-__v -updatedAt -password")
      .limit(noOfUnis)
      .skip((pageNo - 1) * noOfUnis);
    if (name) {
      let nameRegex = new RegExp(name);
      let rre = await universityModel.find(
        { name: { $regex: nameRegex, $options: "i" } },
        "-__v -updatedAt -password"
      );
      return {
        availablePages: 1,
        pageNo: 1,
        rre,
      };
    }
    if (accepted) {
      const unis = await partnershipModel.find({
        sender: user._id,
        status: "accepted",
      });
      let approvedUnis = [];
      for (let i = 0; i < unis.length; i++) {
        approvedUnis.push(universityModel.findById(unis[i].receiver));
      }
      agentCount = unis.length;
      availablePages = Math.ceil(agentCount / noOfUnis);
      return {
        pageNo,
        availablePages,
        approvedUnis,
      };
    }
    if (country) {
      allUniversities = allUniversities.filter((element) => {
        return element.country == country;
      });
      agentCount = allUniversities.length;
      availablePages = Math.ceil(agentCount / noOfUnis);
      return {
        pageNo,
        availablePages,
        allUniversities,
      };
    }
    if (pending) {
      const unis = await partnershipModel.find({
        sender: user._id,
        status: "pending",
      });
      let pendingUnis = [];
      for (let i = 0; i < unis.length; i++) {
        pendingUnis.push(universityModel.findById(unis[i].receiver));
      }
      agentCount = unis.length;
      availablePages = Math.ceil(agentCount / noOfUnis);
      return {
        pageNo,
        availablePages,
        pendingUnis,
      };
    }
    agentCount = allUniversities.length;
    availablePages = Math.ceil(agentCount / noOfAgents);

    adminVobbService.numberOfSearches += 1;
    await adminVobbService.save();
    availablePages = Math.ceil(agentCount / noOfUnis);
    return {
      pageNo,
      availablePages,
      Universities,
    };
  }

  // ###########################################################################
  //   if (isVerified !== null) {
  //     allUniversities = allUniversities.filter((element) => {
  //       return element.isVerified == isVerified;
  //     });
  //     return allUniversities;
  //   }
  //   if (!isVerified && !universityName) {
  //     allUniversities = allUniversities.filter((element) => {
  //       return element.isVerified == true;
  //     });
  //     return allUniversities;
  //   }
  // }

  // ###########################################################################
  async fetchApprovedAgents(university) {
    const University = await User.findOne({ _id: university._id });
    if (!University || University.userType !== "University")
      throw new NotFoundError("invalid user. Must be a registered university");
    let approvedAgents = [];
    University.verifiedAgents.forEach((agentId) => {
      approvedAgents.push(agentModel.findById(agentId));
    });
    return approvedAgents;
  }

  async findUniversityById(id) {
    const University = await universityModel.findById(id).select("-password");
    const agentData = [];
    // University.agents.forEach(agentId => {
    //     allAgents.push(agentModel.findById(agentId));
    // });
    if (Array.isArray(University.verifiedAgents)) {
      for (let id = 0; id < University.verifiedAgents.length; id++) {
        let agent = await agentModel.findById(University.verifiedAgents[id]);
        agentData.push(agent);
      }
      let newObject = { agentData };
      const re = { ...University, ...newObject };
      return re;
    }
    // University.verifiedAgents.forEach(async agent => {
    //   let agentD = await User.findById(agent);
    //   agentData.push(agentD);
    //   //console.log(agentData);
    // });

    let newObject = { agentData };
    const re = { ...University, ...newObject };
    return re;
  }

  async fetchApprovedAgents(university) {
    const University = await universityModel.findById({ _id: university._id });
    if (!University) throw new NotFoundError("University not found");
    const approvedAgents = [];

    for (let id = 0; id < University.agents.length; id++) {
      let agent = await agentModel.findById(University.agents[id]);
      approvedAgents.push(agent);
    }
    //Work on req.query.filter by country and search by name
    return approvedAgents;
  }

  // ##################################################################
  //   if (query.organizationName) {
  //     let name = query.organizationName;
  //     approvedAgents = approvedAgents.filter((element) => {
  //       return element.organizationName == name;
  //     });
  //     return approvedAgents;
  //   }

  //   //Work on req.query.filter by country and search by name
  //   return approvedAgents;
  // }
  // ###########################################################################

  async update(university, data) {
    const update = await universityModel.findOneAndUpdate(
      { _id: university._id },
      data,
      { new: true }
    );

    return update;
  }

  async adminUpdate(university, data) {
    const update = await universityModel.findOneAndUpdate(
      { _id: university },
      data,
      { new: true }
    );

    return update;
  }

  async updatePassword(university, data) {
    const passwordMatch = await bcrypt.compare(
      data.currentPassword,
      university.password
    );
    if (!passwordMatch) throw new InvalidError("invalid password");
    if (data.newPassword !== data.confirmPassword)
      throw new ForbiddenError("New passwords do not match");
    // user can change password here now
    university.password = data.newPassword;
    await university.save();

    return university;
  }

  async requestPartnership(agent, code, note) {
    const Agent = await agentModel.findById(agent._id);
    const AgentStat = await AgentStatisticsService.findStatistics(agent._id);
    if (!Agent || !AgentStat) throw new NotFoundError("Invalid Agent");

    const university = universityModel.findOne({ code });
    if (!university) throw new NotFoundError("Invalid University Code");

    //On acceptance of partnership
    university.networkSize += AgentStat.partners;
    university.approvedAgents += 1;
    await university.agents.push(agent._id);
    await Agent.universities.push(university._id);
    await university.save();
    await Agent.save();
    return university;
  }

  async updateRequest(agent, status, universityId, code) {
    const agentRequesting = await agentModel.findById(agent._id);
    const agentId = agent._id;
    if (!agentRequesting) throw new NotFoundError("Agent does not exist");

    const University = universityModel.findOne({ _id: universityId });
    if (!University) throw new NotFoundError("Invalid University");
    const AgentStat = await AgentStatisticsService.findStatistics(agent._id);
    if (!AgentStat) throw new NotFoundError("Invalid Agent");

    if (status === "activate") {
      //Add validation for already activated
      if (University.agents.includes(agentId)) {
        throw new InvalidError("Agent already activated.");
      }
      const university = universityModel.findOne({ code });
      if (!university) throw new NotFoundError("Invalid University Code");
      university.networkSize += AgentStat.partners;
      university.approvedAgents += 1;
      await university.agents.push(agent._id);
      await agentRequesting.universities.push(university._id);
      await university.save();
      await agentRequesting.save();
      return university;
    }
    if (status === "deactivate") {
      //Add validation for already deactivated
      if (!University.agents.includes(agentId)) {
        throw new InvalidError("Agent already deactivated.");
      }
      const index = University.agents.indexOf(agentId);
      if (index > -1) {
        University.agents.splice(index, 1);
      }
      const indx = agentRequesting.universities.indexOf(University._id);
      if (indx > -1) {
        University.agents.splice(indx, 1);
      }
      University.networkSize -= AgentStat.partners;
      University.approvedAgents -= 1;
      await University.save();
      await agentRequesting.save();
      return University;
    }
  }

  async updatePassword(university, data) {
    const passwordMatch = await bcrypt.compare(
      data.currentPassword,
      university.password
    );
    if (!passwordMatch) throw new InvalidError("invalid password");
    if (data.newPassword !== data.confirmPassword)
      throw new ForbiddenError("New passwords do not match");
    // user can change password here now
    university.password = await bcrypt.hash(data.newPassword, 10);
    await university.save();

    return university;
  }

  async requestPartnership(agent, code, note) {
    const Agent = await agentModel.findById(agent._id);
    const AgentStat = await AgentStatisticsService.findStatistics(agent._id);
    if (!Agent || !AgentStat) throw new NotFoundError("Invalid Agent");

    let university = await universityModel.findOne({ code: code });
    if (!university) throw new NotFoundError("Invalid University Code");

    //On acceptance of partnership
    university.networkSize += AgentStat.partners;
    university.approvedAgents += 1;
    await university.agents.push(agent._id);
    await Agent.universities.push(university._id);
    await university.save();
    await Agent.save();
    return university;
  }

  async updateRequest(agent, status, universityId, code) {
    const agentRequesting = await agentModel
      .findById(agent._id)
      .select("-password");
    const agentId = agent._id;
    if (!agentRequesting) throw new NotFoundError("Agent does not exist");

    const University = await universityModel
      .findOne({ _id: universityId })
      .select("-password");
    if (!University) throw new NotFoundError("Invalid University");
    const AgentStat = await AgentStatisticsService.findStatistics(agent._id);
    if (!AgentStat) throw new NotFoundError("Invalid Agent");

    if (status === "activate") {
      //Add validation for already activated
      if (University.agents.includes(agentId)) {
        throw new InvalidError("Agent already activated.");
      }
      const university = await universityModel
        .findOne({ code })
        .select("-password");
      if (!university) throw new NotFoundError("Invalid University Code");
      university.networkSize += AgentStat.partners;
      university.approvedAgents += 1;
      await university.agents.push(agent._id);
      await agentRequesting.universities.push(university._id);
      await university.save();
      await agentRequesting.save();
      return university;
    }
    if (status === "deactivate") {
      //Add validation for already deactivated
      if (!University.agents.includes(agentId)) {
        throw new InvalidError("Agent already deactivated.");
      }
      const index = University.agents.indexOf(agentId);
      if (index > -1) {
        University.agents.splice(index, 1);
      }
      const indx = agentRequesting.universities.indexOf(University._id);
      if (indx > -1) {
        University.agents.splice(indx, 1);
      }
      University.networkSize -= AgentStat.partners;
      University.approvedAgents -= 1;
      await University.save();
      await agentRequesting.save();
      return University;
    }
  }
}

module.exports = new University();

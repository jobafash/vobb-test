const scholarshipModel = require("./../models/ScholarshipModel");

class Scholarship {
    async createScholarshipList(data) {
        const newList = new scholarshipModel(data);
        await newList.save()

        return newList
    }

    async findAllScholarships() {
        return await scholarshipModel.find({})
    }
}

module.exports = new Scholarship();
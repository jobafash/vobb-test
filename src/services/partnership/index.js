const XLSX = require("xlsx");
const countries = require("i18n-iso-countries");
const AgentAffiliatedUniversity = require("./../../models/AgentUniversityModel");
const agentSupportingModel = require("./../../models/AgentsSupportingModel");
const applicationsModel = require("./../../models/applications/index");
const commissionModel = require("./../../models/commissions/index");
const partnershipModel = require("../../models/partnershipModel/index");
const requestAccessModel = require("../../models/RequestingAccessAdmin");
const User = require("./../../models/UserModel");
const {
  partnershipRequestMail,
  invitationLink,
} = require("./../../utils/mailer");
const directAffilliaiteModel = require("../../models/partnershipModel/DirectAffiliationsModel");
const partnershipAffiliateModel = require("./../../models/partnershipModel/PartnershipAffiliationsModel");
const {
  NotFoundError,
  BadRequestError,
  InternalServerError,
  InvalidError,
} = require("./../../../lib/appErrors");
const { seedPartenersIn } = require("./../../utils/SeederScript");

class PartnershipService {
  constructor(logger) {
    this.logger = logger;
  }

  async create(user, payload) {
    return new Promise(async (resolve, reject) => {
      const { universityID, partnerID, note } = payload;
      const checkifAlreadyInvolved = await agentSupportingModel.findOne({universityID, agentID:user._id})
      if(checkifAlreadyInvolved) {
        return reject({
          message: "You can not send partnership request to where you belong",
          status: false
        })
      }
      const recievingAgent = await User.findById(partnerID);
      const universityRequest = await AgentAffiliatedUniversity.findById(
        universityID
      );
      const partnership = await partnershipModel.findOne({
        "sendingAgent._id": user._id,
        "receivingAgent._id": partnerID,
        university: universityID,
      });

      if (partnership)
        return reject({
          status: false,
          message: "request has already been made",
        });

      const isAlreadyPartner = await partnershipAffiliateModel.findOne({
        partnershipAgentID: partnerID,
        agentId: user._id,
      });

      if (isAlreadyPartner) {
        isAlreadyPartner.universityIDs.forEach((university) => {
          if (String(university.university) === universityID) {
            return reject({
              status: false,
              message: "Agent is already a partner",
            });
          }
        });
      }

      // check if the partner's university has been verified
      const checkAgent = await requestAccessModel.findOne({
        agentRequesting: partnerID,
      });
      if (!checkAgent)
        return reject({
          status: false,
          message: "Agent is not valid ",
        });

      for (let agentUni of checkAgent.universities) {
        if (agentUni.university === universityID) {
          if (["pending", "processing", "denied"].includes(agentUni.isVerified))
            return reject({
              status: false,
              message: "Agent with university is not Verified",
            });
        }
      }

      const reachedMaximumUniversity = await partnershipAffiliateModel.find({
        agentId: user._id,
      });
      let allUniversities = [];
      let mergedArray;
      for (let universities of reachedMaximumUniversity) {
        mergedArray = allUniversities.concat(universities.universityIDs);
      }

      if (mergedArray && mergedArray.length >= 5) {
        return reject({
          status: false,
          message:
            "You have partnered with five(5) universities. Delete one and partner with another.",
        });
      }

      const partnershipData = {
        university: universityID,
        note,
        sendingAgent: { _id: user._id },
        receivingAgent: { _id: partnerID },
      };

      const newPartnership = new partnershipModel(partnershipData);

      await newPartnership.save();

      if (!newPartnership)
        throw new InternalServerError("something went wrong sending request");

      await seedPartenersIn(partnerID, universityID, user);

      const emailData = {
        email: user.email,
        templateId: process.env.PARTNERSHIP_REQUEST,
        variableData: {
          representative: user.name,
          agency: recievingAgent.agencyName,
        },
        subject: "Partnership Request",
      };
      const emailData2 = {
        email: recievingAgent.email,
        templateId: process.env.PARTNERSHIP_REQUEST_RECIEVER,
        variableData: {
          representative: recievingAgent.name,
          agency: user.agencyName,
          University: universityRequest.university,
        },
        subject: "Partnership Request",
      };
      partnershipRequestMail(emailData);
      partnershipRequestMail(emailData2);
      return resolve({
        newPartnership,
      });
    });
  }

  /**
   * Get available universities
   */
  async getUniversities(user, params) {
    let { pageNo, noOfUnis, country } = params;
    let fetchedUniversities, count, availablePages;

    pageNo = pageNo ? Number(pageNo) : 1;
    noOfUnis = noOfUnis ? Number(noOfUnis) : 10;

    return new Promise(async (resolve) => {
      // add validation for filter by country
      count = await AgentAffiliatedUniversity.countDocuments({});
      availablePages = Math.ceil(count / noOfUnis);
      fetchedUniversities = await AgentAffiliatedUniversity.find({})
        .populate({
          path: "agents",
          model: "User",
          select: {
            public_link: 0,
            address: 0,
            profileScore: 0,
            score: 0,
            avatar: 0,
            directUniversities: 0,
            partneredUniversities: 0,
            password: 0,
            __v: 0,
            sentRequests: 0,
            role: 0,
            userType: 0,
            receivedRequests: 0,
            universities: 0,
            AffiliatedCountry: 0,
            isVerified: 0,
          },
        })
        .limit(noOfUnis)
        .skip((pageNo - 1) * noOfUnis);

      return resolve({ pageNo, availablePages, fetchedUniversities });
    });
  }

  /**
   * Get all partnerships
   */
  async get(user, params) {
    //Paginate
    return new Promise(async (resolve, reject) => {
      let { pageNo, noOfApps, type } = params;

      pageNo = pageNo ? Number(pageNo) : 1;
      noOfApps = noOfApps ? Number(noOfApps) : 10;

      let fetchedApps, count, availablePages, reqList;

      if (type == "pending") {
        reqList = await partnershipModel.find({ "sendingAgent.id": user._id });
        count = reqList.length;
        availablePages = Math.ceil(count / noOfApps);
        // if(req.query.status){} Query w/ status here or do it in final arr
        fetchedApps = await partnershipModel
          .find({ "sendingAgent.id": user._id })
          .populate({
            path: "receivingAgent._id",
            model: "Agent",
          })
          .populate({
            path: "university",
            model: "AgentAffiliated_University",
          })
          .limit(noOfApps)
          .skip((pageNo - 1) * noOfApps);

        return resolve({ pageNo, availablePages, fetchedApps });
      } else if (type == "incoming") {
        reqList = await partnershipModel.find({
          "receivingAgent.id": user._id,
        });

        count = reqList.length;
        availablePages = Math.ceil(count / noOfApps);

        fetchedApps = await partnershipModel
          .find({ "receivingAgent.id": user._id })
          .populate({
            path: "sendingAgent._id",
            model: "Agent",
          })
          .populate({
            path: "university",
            model: "AgentAffiliated_University",
          })
          .limit(noOfApps)
          .skip((pageNo - 1) * noOfApps);
        return resolve({ pageNo, availablePages, fetchedApps });
      } else {
        return reject({
          message:
            "Invalid type specified for partnerships. Allowed types are pending and incoming",
        });
      }
    });
  }

  // search university by coiuntry and view all the agents under them
  async searchUniversityByCountry(param) {
    let { pageNo, NoOfUniveristy, search } = param;

    pageNo = pageNo ? Number(pageNo) : 1;
    NoOfUniveristy = NoOfUniveristy ? Number(NoOfUniveristy) : 12;

    let universities;
    let universityCount;
    let availablePages;

    let UniversityList = await AgentAffiliatedUniversity.find(
      {},
      "-__v -updatedAt"
    );

    let allUniversities = [];

    if (!search) {
      universities = await AgentAffiliatedUniversity.find({}, "-__v -updatedAt")
        .populate({
          path: "agents",
          model: "Agent",
        })
        .limit(NoOfUniveristy)
        .skip((pageNo - 1) * NoOfUniveristy);

      // filter the  university that have agents in them
      universities = universities.filter(
        (university) => university.agents.length > 0
      );
    } else {
      UniversityList = await AgentAffiliatedUniversity.find(
        { country: search },
        "-__v -updatedAt"
      );
      universities = await AgentAffiliatedUniversity.find(
        { country: search },
        "-__v -updatedAt"
      )
        .populate({
          path: "agents",
          model: "Agent",
        })
        .limit(NoOfUniveristy)
        .skip((pageNo - 1) * NoOfUniveristy);

      universities = universities.filter(
        (university) => university.agents.length > 0
      );
    }

    universities.forEach((university) => {
      allUniversities.push({
        agentCount: university.agents.length,
        university,
      });
    });
    universityCount = UniversityList.length;
    availablePages = Math.ceil(universityCount / NoOfUniveristy);

    return { allUniversities, availablePages };
  }

  async getAgentsInUniversity(user, universityID, param) {
    let { pageNo, NoOfAgents } = param;

    pageNo = pageNo ? Number(pageNo) : 1;
    NoOfAgents = NoOfAgents ? Number(NoOfAgents) : 12;

    let availablePages;
    let neededAgents;
    let agentCount;

    const foundUniversity = await agentSupportingModel
      .find({
        universityID,
        isVerified: true,
      })
      .populate({
        path: "agentID",
        model: "Agent",
      })
      .populate({
        path: "universityID",
        model: "AgentAffiliated_University",
      });

    neededAgents = await agentSupportingModel
      .find({ universityID, isVerified: true }, "-__v -updatedAt")
      .populate({
        path: "agentID",
        model: "Agent",
      })
      .populate({
        path: "universityID",
        model: "AgentAffiliated_University",
      })
      .limit(NoOfAgents)
      .skip((pageNo - 1) * NoOfAgents);

    let newArrayofpartneredAndUnpartnered = [];

    for (let everyAgent of neededAgents) {
      let check = await partnershipAffiliateModel.findOne({
        partnershipAgentID: everyAgent.agentID,
        agentId: user._id,
      });

      if (check) {
        const found = check.universityIDs.some(
          (university) =>
            String(university.university) ===
            String(everyAgent.universityID._id)
        );

        if (found) {
          newArrayofpartneredAndUnpartnered.push({
            everyAgent,
            isAlreadyPartner: true,
          });
        } else {
          newArrayofpartneredAndUnpartnered.push({
            everyAgent,
            isAlreadyPartner: false,
          });
        }
      } else {
        newArrayofpartneredAndUnpartnered.push({
          everyAgent,
          isAlreadyPartner: false,
        });
      }
    }

    neededAgents = newArrayofpartneredAndUnpartnered;
    let agentCountInUni = foundUniversity.length;
    agentCount = foundUniversity.length;
    availablePages = Math.ceil(agentCount / NoOfAgents);

    return { agentCountInUni, neededAgents, availablePages };
  }

  /**
   * Get single university
   */
  async getSingleUniversity(params) {
    return new Promise(async (resolve, reject) => {
      const id = params.id;
      const university = await AgentAffiliatedUniversity.findById(id).populate({
        path: "agents",
        model: "User",
        select: {
          public_link: 0,
          address: 0,
          profileScore: 0,
          score: 0,
          avatar: 0,
          directUniversities: 0,
          partneredUniversities: 0,
          password: 0,
          __v: 0,
          sentRequests: 0,
          role: 0,
          userType: 0,
          receivedRequests: 0,
          universities: 0,
          AffiliatedCountry: 0,
          isVerified: 0,
        },
        //Check this well
        populate: {
          path: "stats",
          model: "Stats",
          select: { agent: 0, createdAt: 0, __v: 0, updatedAt: 0 },
        },
      });
      if (!university)
        return reject({
          message: "Invalid ID",
        });
      return resolve(university);
    });
  }

  /**
   * Get single partnership
   */
  async getSingle(params) {
    return new Promise(async (resolve, reject) => {
      const id = params.id;
      const partnership = await partnershipModel
        .findById(id)
        .populate({
          path: "sendingAgent._id",
          model: "Agent",
        })
        .populate({
          path: "university",
          model: "AgentAffiliated_University",
        });
      if (!partnership)
        return reject({
          message: "Invalid ID",
        });
      return resolve(partnership);
    });
  }

  /**
   * Update partnership status
   */
  async update(user, params, payload) {
    return new Promise(async (resolve, reject) => {
      const { id } = params;
      const { status } = payload;
      const application = await partnershipModel.findById(id);

      if (!application)
        return reject({
          message: "Invalid ID or Application does not exist",
        });

      const agent = await User.findById(application.receivingAgent._id);
      const senderAgent = await User.findById(application.sendingAgent._id);
      const agentSupport = await agentSupportingModel.findOne({
        agentID: user._id,
        universityID: application.university,
      });

      if (status === "accepted") {
        const directAffililations = await directAffilliaiteModel.findOne({
          agentId: user._id,
          university: application.university,
        });

        const partneredAffiliations = await partnershipAffiliateModel.findOne({
          agentId: application.sendingAgent._id,
          partnershipAgentID: user._id,
        });

        const reachedMaximumUniversity = await partnershipAffiliateModel.find({
          agentId: application.sendingAgent._id,
        });

        let allUniversities = [];
        let mergedArray;
        reachedMaximumUniversity.forEach((universities) => {
          if (universities.universityIDs.length > 0) {
            mergedArray = allUniversities.concat(universities.universityIDs);
          }
        });

        if (mergedArray && mergedArray.length >= 5) {
          return reject({
            status: false,
            message:
              "This agent already has more than 5 universities. Cannot accept this partnership",
          });
        }

        directAffililations.commissions = agentSupport.commission;
        if (
          !directAffililations.agents.includes(application.sendingAgent._id)
        ) {
          directAffililations.agents.push(application.sendingAgent._id);
        }

        await directAffililations.save();

        if (!partneredAffiliations) {
          const partnerData = {
            agentId: application.sendingAgent._id,
            partnershipAgentID: user._id,
            university: [
              {
                university: application.university,
                commission: agentSupport.commission,
              },
            ],
          };
          const patnerAffil = new partnershipAffiliateModel(partnerData);
          await patnerAffil.save();
        } else {
          const found = partneredAffiliations.universityIDs.some(
            (el) => el.university === application.university
          );
          if (!found)
            partneredAffiliations.universityIDs.push({
              university: application.university,
              commission: agentSupport.commission,
            });

          await partneredAffiliations.save();
        }
        application.sendingAgent.status = payload.status;
        application.receivingAgent.status = payload.status;
        await application.save();

        await application.remove();
      } else {
        application.sendingAgent.status = payload.status;
        application.receivingAgent.status = payload.status;
        await application.save();
      }

      const emailData = {
        email: senderAgent.email,
        templateId: process.env.PARTNERSHIP_REQUEST_STATUS,
        variableData: {
          representative: senderAgent.name,
          agency: user.agencyName,
          status: payload.status,
        },
        subject: "Partnership Request Status",
      };
      partnershipRequestMail(emailData);
      await application.remove();
      return resolve({
        status: true,
        data: application,
      });
    });
  }

  async getDirectAffiliations(user) {
    const allAffiliates = await directAffilliaiteModel
      .find({
        agentId: user._id,
      })
      .populate({
        path: "university",
        model: "AgentAffiliated_University",
        select: {
          __v: 0,
          createdAt: 0,
          updatedAt: 0,
        },
      })
      .populate({
        path: "agents",
        model: "Agent",
        select: {
          __v: 0,
          createdAt: 0,
          updatedAt: 0,
        },
      });

    let validAffiliates = allAffiliates;

    return validAffiliates;
  }
  async getPartneredAffiliations(user) {
    const allAffiliates = await partnershipAffiliateModel
      .find({
        agentId: user._id,
      })
      .populate({
        path: "partnershipAgentID",
        model: "Agent",
        select: {
          __v: 0,
          createdAt: 0,
          updatedAt: 0,
        },
      })
      .populate({
        path: "universityIDs.university",
        model: "AgentAffiliated_University",
        select: {
          __v: 0,
          createdAt: 0,
          updatedAt: 0,
        },
      });
    const validAffiliates = allAffiliates.filter(
      (uni) => uni.universityIDs.length > 0
    );

    return validAffiliates;
  }

  async deleteDirectAffil(user, partnerID, university) {
    // find the direct affiliate
    const agentAffiliation = await directAffilliaiteModel.findOne({
      agentId: user._id,
      university,
    });
    const partnerAffil = await partnershipAffiliateModel.findOne({
      agentId: partnerID,
      partnershipAgentID: user._id,
    });

    if (!agentAffiliation) throw new NotFoundError("Affiliation not found");

    if (partnerAffil) {
      let filterArray = partnerAffil.universityIDs.filter(
        (agent) => String(agent.university) !== String(university)
      );
      partnerAffil.universityIDs = filterArray;
      await partnerAffil.save();

      if (partnerAffil.universityIDs.length === 0) {
        await partnerAffil.remove();
      }
    }

    const index = agentAffiliation.agents.findIndex(
      (agent) => String(agent) === String(partnerID)
    );

    if (index > -1) {
      agentAffiliation.agents = agentAffiliation.agents.filter(
        (agent) => String(agent) !== String(partnerID)
      );
    }

    await agentAffiliation.save();

    return agentAffiliation;
  }
  async deleteParnterAffili(user, partnershipAgentID, universityID) {
    // find the direct affiliate
    const DirectAffiliate = await directAffilliaiteModel.findOne({
      agentId: partnershipAgentID,
      university: universityID,
    });

    if (DirectAffiliate) {
      const index = DirectAffiliate.agents.findIndex(
        (agent) => String(agent) === String(user._id)
      );
      DirectAffiliate.agents.splice(DirectAffiliate.agents[index], 1);

      await DirectAffiliate.save();
    }

    const agentAffiliation = await partnershipAffiliateModel.findOne({
      agentId: user._id,
      partnershipAgentID,
    });
    if (!agentAffiliation) throw new NotFoundError("Affiliation not found");

    let AllowedPartners;
    let filterArray = agentAffiliation.universityIDs.filter(
      (university) => String(university.university) !== String(universityID)
    );

    agentAffiliation.universityIDs = filterArray;

    await agentAffiliation.save();

    if (filterArray.length === 0) {
      await agentAffiliation.remove();
      AllowedPartners = agentAffiliation;
      return AllowedPartners;
    }
    AllowedPartners = agentAffiliation;

    return AllowedPartners;
  }

  async updateCommission(user, commission, university) {
    // we need to find the university in question
    const findUniversityToUpdate = await directAffilliaiteModel.findOne({
      agentId: user._id,
      university,
    });
    const findPartenerToUpdate = await partnershipAffiliateModel.find({
      partnershipAgentID: user._id,
    });

    const findapplications = await applicationsModel.findOne({
      "receivingAgent._id": user._id,
      university,
    });

    if (!findUniversityToUpdate)
      throw new NotFoundError("You are not affiliated to this university");

    // updating for partnerships
    if (findPartenerToUpdate && findPartenerToUpdate.length > 0) {
      for (let eachAffiliate of findPartenerToUpdate) {
        const uniIndex = eachAffiliate.universityIDs.findIndex(
          (eachUni) => String(eachUni.university) === String(university)
        );

        if (uniIndex > -1) {
          eachAffiliate.universityIDs[uniIndex].commission = commission;
        }

        await eachAffiliate.save();
      }
    }

    // updating commissions page.
    if (findapplications) {
      const findCommissionToUpdate = await commissionModel.findOne({
        application: findapplications._id,
      });

      if (findCommissionToUpdate) {
        findCommissionToUpdate.commission = commission;
      }

      await findCommissionToUpdate.save();
    }

    const agentSupport = await agentSupportingModel.findOne({
      agentID: user._id,
      universityID: university,
    });

    findUniversityToUpdate.commissions = commission;
    agentSupport.commission = commission;

    await findUniversityToUpdate.save();
    await agentSupport.save();

    return findUniversityToUpdate;
  }

  async agentIncomingPartnership(user, param) {
    let { pageNo, NoOfPartner } = param;

    pageNo = pageNo ? Number(pageNo) : 1;
    NoOfPartner = NoOfPartner ? Number(NoOfPartner) : 12;

    let incomingPartners;
    let incomingPartnershipCount;
    let availablePages;

    let incomingList = await partnershipModel.find({
      "receivingAgent._id": user._id,
    });

    incomingPartners = await partnershipModel
      .find({ "receivingAgent._id": user._id })
      .limit(NoOfPartner)
      .skip((pageNo - 1) * NoOfPartner)
      .populate({
        path: "sendingAgent._id",
        model: "Agent",
      })
      .populate({
        path: "university",
        model: "AgentAffiliated_University",
      });

    incomingPartnershipCount = incomingList.length;
    availablePages = Math.ceil(incomingPartnershipCount / NoOfPartner);

    return { incomingPartnershipCount, availablePages, incomingPartners };
  }

  async agentPendingPartnership(user, param) {
    let { pageNo, NoOfPartner } = param;

    pageNo = pageNo ? Number(pageNo) : 1;
    NoOfPartner = NoOfPartner ? Number(NoOfPartner) : 12;

    let PendingPartners;
    let pendingPartnershipCount;
    let availablePages;

    let pendingList = await partnershipModel.find({
      "sendingAgent._id": user._id,
    });

    PendingPartners = await partnershipModel
      .find({ "sendingAgent._id": user._id })
      .limit(NoOfPartner)
      .skip((pageNo - 1) * NoOfPartner)
      .populate({
        path: "receivingAgent._id",
        model: "Agent",
      })
      .populate({
        path: "university",
        model: "AgentAffiliated_University",
      });

    pendingPartnershipCount = pendingList.length;
    availablePages = Math.ceil(pendingPartnershipCount / NoOfPartner);

    return { pendingPartnershipCount, availablePages, PendingPartners };
  }

  async getAgentProfile(user, partnerID) {
    // let us ensure that this partner is Existing
    const isPartnerExisting = await User.findById(partnerID);

    if (!isPartnerExisting)
      throw new InvalidError("Invalid Id provided for partner");

    // fetch partners from dirrect Affiliation and partnered partneredAffiliations
    let cummulativePartners = [];

    const cummDirectAffil = await directAffilliaiteModel.find({
      agentId: partnerID,
    });
    const cummPartnerAffil = await partnershipAffiliateModel.find({
      agentId: partnerID,
    });

    if (cummDirectAffil) {
      for (let everyAgent of cummDirectAffil) {
        cummulativePartners.push(...everyAgent.agents);
      }
    }
    if (cummPartnerAffil) {
      for (let everyAgent of cummPartnerAffil) {
        cummulativePartners.push(everyAgent.partnershipAgentID);
      }
    }

    const TotalPartners = cummulativePartners.length;

    // fetch for AdmissionStats
    let TotalAdmissionStats = 0;
    let affiliatedUniversities = [];
    const admissionStats = await agentSupportingModel
      .find({
        agentID: partnerID,
      })
      .populate({
        path: "universityID",
        model: "AgentAffiliated_University",
      });

    for (let agent of admissionStats) {
      TotalAdmissionStats += agent.admission;
      affiliatedUniversities.push(agent.universityID);
    }

    return {
      enquiries: 0,
      admissions: TotalAdmissionStats,
      partners: TotalPartners,
      affiliatedUniversity: affiliatedUniversities,
      missionStatement: isPartnerExisting.bio,
    };
  }

  async listOfCountries() {
    let onlyCountries = [];
    const Allcountries = await AgentAffiliatedUniversity.find({});

    for (let country of Allcountries) {
      let countryName = countries.getName(country.country, "en", {
        select: "official",
      });
      onlyCountries.push({ countryCode: country.country, countryName });
    }

    const key = "countryCode";

    const arrayUniqueByKey = [
      ...new Map(onlyCountries.map((item) => [item[key], item])).values(),
    ];

    function sortByKey(array, countryCode) {
      return array.sort((a, b) => {
        let x = a[countryCode];
        let y = b[countryCode];

        return x < y ? -1 : x > y ? 1 : 0;
      });
    }
    return sortByKey(arrayUniqueByKey, "countryName");
  }

  async sendpartnerAlink(user, emails) {
    for (let email of emails) {
      const emailData = {
        email: email,
        templateId: process.env.INVITE_LINK_ID,
        variableData: {
          agent: user.name,
          agentID: user._id,
        },
        subject: "Invitation",
      };
      invitationLink(emailData);
    }

    return true;
  }

  async sendMultiplePartnerLinks(user, files) {
    const workbook = XLSX.readFile(files[0].path);
    const worksheet = workbook.Sheets[workbook.SheetNames[0]];
    const header = ["email"];

    let result = XLSX.utils.sheet_to_json(worksheet, {
      header,
      raw: true,
    });

    result = result.slice(1);

    for (let key of result) {
      const emailData = {
        email: key.email,
        templateId: process.env.INVITE_LINK_ID,
        variableData: {
          agent: user.name,
          agentID: user._id,
        },
        subject: "Invitation",
      };
      invitationLink(emailData);
    }

    return true;
  }
}

module.exports = PartnershipService;

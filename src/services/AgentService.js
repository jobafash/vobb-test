const bcrypt = require("bcrypt");
const agentModel = require("./../models/UserModel");
const jwt = require("jsonwebtoken");
const agentSupportingModel = require("./../models/AgentsSupportingModel");
const partnershipModel = require("./../models/partnershipModel/PartnershipAffiliationsModel");
const applicaitonModel = require("./../models/applications/index");
const commissionModel = require("./../models/commissions/index");
const conversationModel = require("./../models/chat/conversation");
const directPartnersModel = require("./../models/partnershipModel/DirectAffiliationsModel");
const statsModel = require("./../models/StatisticsModel");
const AgentuniversityAffiliationModel = require("./../models/AgentUniversityModel");
const { invitationLink } = require("./../utils/mailer");
const {
  BadRequestError,
  NotFoundError,
  InternalServerError,
  UnAuthorizedError,
} = require("./../../lib/appErrors");
const {
  SeedAgentSupport,
  requestUniversityAccess,
} = require("./../utils/SeederScript");
class Agent {
  async createAgent(data) {
    data.password = await bcrypt.hash(data.password, 10);
    const newAgent = new agentModel(data);
    await newAgent.save();
    const token = jwt.sign(
      { email: data.email.toString() },
      process.env.JWT_SECRET,
      { expiresIn: 600 }
    );

    return { newAgent, token };
  }

  async validateAgentToken(token) {
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const isExisting = await this.findAgentByEmail(decoded.email);

    if (!isExisting) throw new NotFoundError("Agent does not exist");

    isExisting.isVerified = true;
    await isExisting.save();

    return true;
  }

  async loginVerifiedAgent(data) {
    const validEmail = await this.findAgentByEmail(data.email);
    if (!validEmail) throw new NotFoundError("Invalid User");
    if (validEmail && validEmail.isVerified !== true)
      throw new UnAuthorizedError(
        "Unauthorised. please verify your email"
      );
    const isMatch = await bcrypt.compare(data.password, validEmail.password);
    if (!isMatch) throw new UnAuthorizedError("Invalid login details");

    const token = await validEmail.generateToken();
    validEmail.isFirstTime += 1;
    await validEmail.save();
    const user = validEmail;
    return { user, token };
  }

  async CreateUniversityAffiliations(user, data) {
    if (Array.isArray(data.affiliations) && data.affiliations.length > 0) {
      let createdAffiliation = [];
      let clientNeededArray = [];

      for (let affiliations of data.affiliations) {
        let foundUniversity;
        foundUniversity = await AgentuniversityAffiliationModel.findOne({
          country: affiliations.country,
          university: affiliations.university,
        });

        if (!foundUniversity) {
          const neededData = {
            country: affiliations.country,
            university: affiliations.university,
          };
          const createdAffiliation = new AgentuniversityAffiliationModel(
            neededData
          );
          await createdAffiliation.save();

          foundUniversity = createdAffiliation;
        }
        // seed data into the supporting agents
        const supportService = await SeedAgentSupport(
          user._id,
          foundUniversity._id,
          affiliations
        );
        
        createdAffiliation.push({ university: foundUniversity._id });
        clientNeededArray.push({
          _id: foundUniversity._id,
          country: foundUniversity.country,
          univeristy: foundUniversity.university,
          commission: supportService.commission,
        });
      }
      
      await requestUniversityAccess(user._id, createdAffiliation);

      return clientNeededArray;
    } else {
      return [];
    }
  }

  async updateAgents(param) {
    const agents = await agentModel.find({});

    for (let i = 0; i < agents.length; i++) {
      let score;

      const stats = await statsModel.findOne({ agent: agents[i]._id });
      agents[i].numRatings = agents[i].numRatings || 0;
      agents[i].rating = agents[i].rating || 0;
      let totalScore =
        Number(7 * stats.profileViews) +
        Number(5 * stats.inquiredRecieved) +
        Number(10 * stats.admissions) +
        Number(5 * stats.partners) +
        Number(10 * agents[i].numRatings) +
        Number(10 * agents[i].rating);

      score = totalScore / 6;
      await agentModel.findByIdAndUpdate(agents[i]._id, { score: score });
    }

    let { pageNo, noOfAgents } = param;
    pageNo = pageNo ? Number(pageNo) : 1;
    noOfAgents = noOfAgents ? Number(noOfAgents) : 10;
    let fetchedAgents;
    let agentCount;
    let availablePages;

    let agentList = await agentModel.find(
      { isVerified: true },
      "-__v -updatedAt -password"
    );

    fetchedAgents = await agentModel
      .find({ isVerified: true }, "-__v -updatedAt -password")
      .sort({ score: -1 })
      .limit(noOfAgents)
      .skip((pageNo - 1) * noOfAgents);

    agentCount = agentList.length;
    availablePages = Math.ceil(agentCount / noOfAgents);
    return {
      currentPage: pageNo,
      availablePages,
      fetchedAgents,
    };
  }
  async findAgents(param) {
    let { pageNo, noOfAgents } = param;
    pageNo = pageNo ? Number(pageNo) : 1;
    noOfAgents = noOfAgents ? Number(noOfAgents) : 10;
    let agents;
    let agentCount;
    let availablePages;

    let agentList = await agentModel.find({}, "-__v -updatedAt -password");

    if (!param) {
      agents = await agentModel
        .find({}, "-__v -updatedAt -password")
        .select("-password");
      availablePages = 0;
      return { agents, availablePages };
    }

    agents = await agentModel
      .find({}, "-__v -updatedAt")
      .select("-password")
      .limit(noOfAgents)
      .skip((pageNo - 1) * noOfAgents);

    agentCount = agentList.length;
    availablePages = Math.ceil(agentCount / noOfAgents);
    return { agents, availablePages };
  }
  async findAgentByEmail(email) {
    return await agentModel.findOne({ email });
  }

  async findAgentById(id) {
    return await agentModel.findById(id);
  }

  async updateCommission(user, neededData) {
    let updatedCommission = [];
    for (let commission of neededData.universityCommission) {
      const foundUniversity = await agentSupportingModel.findOne({
        agentID: user._id,
        universityID: commission.university,
      });
      if (!foundUniversity) throw new NotFoundError("University was not found");

      foundUniversity.commission = commission.commission;
      await foundUniversity.save();

      updatedCommission.push(foundUniversity);
    }

    return updatedCommission;
  }

  async statisticsNav(user) {
    const partners = await partnershipModel.find({ agentId: user._id });
    const directPartners = await directPartnersModel.find({
      agentId: user._id,
    });

    let neededDirectPartners = [];

    for (let partners of directPartners) {
      neededDirectPartners.push(...partners.agents);
    }

    const applications = await applicaitonModel.find({
      "receivingAgent._id": user._id,
    });
    const applications2 = await applicaitonModel.find({
      "sendingAgent._id": user._id,
    });
    const commissions = await commissionModel.find({
      "sendingAgent._id": user._id,
    });
    const commissions2 = await commissionModel.find({
      "receivingAgent._id": user._id,
    });
    const conversations = await conversationModel.find({
      members: { $in: [user._id] },
    });

    const partnerStats = partners.length + neededDirectPartners.length;
    const applicationStats = applications.length + applications2.length;
    const commissionsStats = commissions.length + commissions2.length;
    const MessageStats = conversations.length;

    return { partnerStats, applicationStats, commissionsStats, MessageStats };
  }
}
module.exports = new Agent();

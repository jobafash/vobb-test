const InvoiceModel = require("./../../models/invoices/");
const commissionsModel = require("./../../models/commissions/");
const agntSupportModel = require("./../../models/AgentsSupportingModel");
const UserModel = require("./../../models/UserModel");
const { generateCode } = require("./../../utils/UniqueNumberGen");

class Invoices {
  async generateInvoice(agent, listOfCommissions) {
    const data = {
      companyName: agent.agencyName,
      companyAddress: agent.address,
      unixCreationDate: Date.now(),
      totalAmount: 0,
      invoiceUniqueCode:
        agent.agencyName[0] + agent.address[0] + "-" + generateCode(12),
      vobbTotalAmount: 0,
      commissionList: [],
      agentID: agent._id,
    };

    for (let eachCommissions of listOfCommissions) {
      const agentCommissions = await commissionsModel
        .findById(eachCommissions)
        .populate({
          path: "application",
          model: "Application",
          populate: {
            path: "university",
            model: "AgentAffiliated_University",
          },
        });

      const receivingAgent = await UserModel.findById(
        agentCommissions.receivingAgent._id
      );
      const supportSystem = await agntSupportModel.findOne({
        agentID: agent._id,
        universityID: agentCommissions.application.university._id,
      });
      const universityPaidAmount = supportSystem.universityPaymentMethod.isFixed
        ? supportSystem.universityPaymentMethod.received
        : (agentCommissions.tuitionFees *
            supportSystem.universityPaymentMethod.received) /
          100;

      const price = agentCommissions.commission;
      const vobbAmount = universityPaidAmount * 0.05;
      const title =
        agentCommissions.application.passportNumber +
        "-" +
        receivingAgent.agencyName +
        "-" +
        agentCommissions.application.university.university;

      data.totalAmount += price;
      data.vobbTotalAmount += vobbAmount;

      data.commissionList.push({
        price,
        vobbAmount,
        title,
        quantity: 1,
        reciever: receivingAgent._id,
      });
    }

    const newInvoice = new InvoiceModel(data);
    await newInvoice.save();

    return newInvoice;
  }

  async unprocessedInvoice(agent, param) {
    let { pageNo, noOfInvoice } = param;
    pageNo = pageNo ? Number(pageNo) : 1;
    noOfInvoice = noOfInvoice ? Number(noOfInvoice) : 10;

    let availablePages;
    const invoices = await InvoiceModel.find({
      agentID: agent._id,
      paid: false,
    });

    const neededInvoices = await InvoiceModel.find({
      agentID: agent._id,
      paid: false,
    })
      .populate({
        path: "commissionList.reciever",
        model: "Agent",
      })
      .limit(noOfInvoice)
      .skip((pageNo - 1) * noOfInvoice)
      .sort({ createdAt: -1 });

    availablePages = Math.ceil(invoices.length / noOfInvoice);

    return { availablePages, neededInvoices };
  }

  async processInvoices(agent, param) {
    let { pageNo, noOfInvoice } = param;
    pageNo = pageNo ? Number(pageNo) : 1;
    noOfInvoice = noOfInvoice ? Number(noOfInvoice) : 10;

    let availablePages;
    const invoices = await InvoiceModel.find({
      agentID: agent._id,
      paid: true,
    });

    const neededInvoices = await InvoiceModel.find({
      agentID: agent._id,
      paid: true,
    })
      .populate({
        path: "commissionList.reciever",
        model: "Agent",
      })
      .limit(noOfInvoice)
      .skip((pageNo - 1) * noOfInvoice)
      .sort({ createdAt: -1 });

    availablePages = Math.ceil(invoices.length / noOfInvoice);

    return { availablePages, neededInvoices };
  }
}

module.exports = new Invoices();

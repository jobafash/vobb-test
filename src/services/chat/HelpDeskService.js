const axios = require("axios");
const { UnAuthorizedError } = require("../../../lib/appErrors");

const agentUID = process.env.HELPDESK_USER_UID;

const url = `https://${process.env.HELPDESK_APPID}.api-${process.env.HELPDESK_REGION}.cometchat.io/v3`;

const headers = {
  apikey: process.env.HELPDESK_APIKEY,
};

class HelpDesk {
  async createUser(data) {
    const response = await axios.post(`${url}/users`, data, {
      headers,
    });
    if (response) {
      let gotToken = await this.requestAuthToken(response.data.data.uid);

      if (gotToken) {
        return gotToken;
      } else {
        throw new UnAuthorizedError("Something went wrong getting the token");
      }
    } else if (!response) {
      throw new UnAuthorizedError("Something went wrong Creating the token");
    }
  }

  async createTokenForReturningUsers(uid) {
    const token = await this.requestAuthToken(uid);
    if (!token)
      throw new UnAuthorizedError("Something went wrong Creating the token");

    return token;
  }

  async getAllUsers() {
    const response = await axios.get(`${url}/users`, {
      headers,
    });

    if (!response) throw new UnAuthorizedError("Something went wrong");
    const { data } = response.data;

    const filterAgentData = data.filter((data) => data.uid !== agentUID);

    return filterAgentData;
  }

  async requestAuthToken(uid) {
    return new Promise((resolve, reject) => {
      axios
        .post(`${url}/users/${uid}/auth_tokens`, null, {
          headers,
        })
        .then((response) => {
          resolve(response.data.data);
        })
        .catch((error) => reject(error));
    });
  }
}

module.exports = new HelpDesk();

const partnershipAffill = require("./../../models/partnershipModel/PartnershipAffiliationsModel");
const directAffill = require("./../../models/partnershipModel/DirectAffiliationsModel");

class ChatPartners {
  async allPartners(user) {
    const everyPatner = [];

    const allPartners = await partnershipAffill
      .find({
        agentId: user._id,
      })
      .populate({
        path: "partnershipAgentID",
        model: "Agent",
      });
    for (let eachAgent of allPartners) {
      everyPatner.push(eachAgent.partnershipAgentID);
    }
    const direct = await directAffill
      .find({
        agentId: user._id,
      })
      .populate({
        path: "agents",
        model: "Agent",
      });
      
    for (let eachAgent of direct) {
      everyPatner.push(...eachAgent.agents);
    }

    return [...new Set(everyPatner)];
  }
}

module.exports = new ChatPartners();

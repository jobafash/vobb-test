// const bcrypt = require("bcrypt");
// // const agentModel = require("./../models/AgentModel");
// const adminVobbService = require("./../services/AdminVobbServices")
// // const studentModel = require("./../models/StudentModel");
// const { agentModel, studentModel } = require("./../models/UserModel");
// const jwt = require("jsonwebtoken");
// const { InvalidError, ForbiddenError } = require("./../../lib/appErrors");

// class Student {
//   async create(data) {
//     const newUser = new studentModel(data);
//     await newUser.save();

//     adminVobbService.numberOfStudent += 1
//     await adminVobbService.save()

//     return newUser;
//   }

//   async findStudentByEmail(email) {
//     return await studentModel.findOne({ email });
//   }

//   async verifyAuthToken(data) {
//     const decoded = jwt.verify(data, process.env.JWT_SECRET);

//     return decoded;
//   }

//   async findUser(emailVerifyToken) {
//     return await studentModel.findOne({ emailVerifyToken });
//   }

//   async findStudentById(id) {
//     return await studentModel.findById(id);
//   }
//   async updateProfile(id, data) {
//     const updateStudentProfile = await studentModel.updateOne(
//       { _id: id },
//       data,
//       { new: true }
//     );

//     if (!updateStudentProfile) throw new NotFoundError("User does not exist");

//     return updateStudentProfile;
//   }

//   async checkUpdatePassword(student, data) {
//     const user = await studentModel.findById(student._id);
//     const passwordMatch = await bcrypt.compare(
//       data.currentPassword,
//       user.password
//     );
//     if (!passwordMatch)
//       throw new InvalidError("Please insert the correct password");
//     if (data.newPassword !== data.confirmPassword)
//       throw new ForbiddenError("new passwords do not match");
//     // user can change password here now
//     if (data.newPassword === data.confirmPassword) {
//       user.password = data.newPassword;
//       await user.save();
//     }

//     return user;
//   }

//   async searchForAgent(params) {
//     let pageNo = !params.pageNo ? 1 : parseInt(params.pageNo);
//     let noOfAgents = !params.noOfAgents ? 20 : parseInt(params.noOfAgents);
//     let fetchedAgents;
//     let agentList;

//     let agentCount;
//     let availablePages;
    
//     const rgx = (pattern) => new RegExp(`.*${pattern}.*`);
//     const searchRgx1 = rgx(params.search1);
//     const searchRgx2 = rgx(params.search2);

//     agentList = await agentModel.find(
//       {
//         $and: [
//           { agentCountry: { $regex: searchRgx1, $options: "i" } },
//           { AffiliatedCountry: { $regex: searchRgx2, $options: "i" } },
//         ],
//       },
//       "-__v -createdAt -updatedAt -password"
//     );

//     fetchedAgents = await agentModel
//       .find(
//         {
//           $and: [
//             { agentCountry: { $regex: searchRgx1, $options: "i" } },
//             { AffiliatedCountry: { $regex: searchRgx2, $options: "i" } },
//           ],
//         },
//         "-__v -createdAt -updatedAt -password"
//       )
//       .sort({ createdAt: -1 })
//       .limit(noOfAgents)
//       .skip((pageNo - 1) * noOfAgents);
//     agentCount = agentList.length;
//     availablePages = Math.ceil(agentCount / noOfAgents);

//     adminVobbService.numberOfSearches += 1
//     await adminVobbService.save()

//     return {
//       totalAgent: agentList.length,
//       currentPage: pageNo,
//       agents: fetchedAgents,
//       availablePages,
//       totalAgents: agentCount,
//     };
//   }

//   async findAllStudents() {
//     return await studentModel.find({});
//   }
// }

// module.exports = new Student();

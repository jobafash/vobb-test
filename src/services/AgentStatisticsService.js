const statistics = require("../models/StatisticsModel");

class Statistics {
  async createStatistics(id) {
    const newStatistics = new statistics({
      agent: id,
    });
    return await newStatistics;
  }

  async findStatistics(id) {
    return await statistics.findOne({ agent: id });
  }
}

module.exports = new Statistics();

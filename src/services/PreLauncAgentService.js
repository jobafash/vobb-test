const preLaunchModel = require("../models/PreLaunchAgentModel");
const XLSX =require("xlsx")

class PreLaunchAgent {
  async CreatePrelaunchAgents(dataFile) {
    const workbook = XLSX.readFile(dataFile);
    const worksheet = workbook.Sheets[workbook.SheetNames[0]];

    const header = ["name", "phone"];

    let result = XLSX.utils.sheet_to_json(worksheet, {
      header,
      raw: true,
    });

    result = result.slice(1);

    try {
      await preLaunchModel.insertMany(result, { ordered: false });
    } catch (error) {
      if (error.code !== 11000) {
        throw error;
      }
    }

    return result;
  }
  async fetchAllAgents(){
      const preLaunchAgents = await preLaunchModel.find({})
      return preLaunchAgents
  }
}

module.exports = new PreLaunchAgent()
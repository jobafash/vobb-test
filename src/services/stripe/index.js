const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);
const accountModel = require("./../../models/stripe/index");
const invoiceModel = require("./../../models/invoices/");
const UserModel = require("./../../models/UserModel");
const BillingHistoryModel = require("./../../models/billingHistory/");
const { uploadToCloud } = require("./../../../lib/cloudinary");
const {
  BadRequestError,
  InvalidError,
  ForbiddenError,
} = require("./../../../lib/appErrors");

class StripeConnect {
  async createCustomer(user, country) {
    //we need to check is customer already exist in Vobb bank.
    const listOfSupportedCountries = [
      "AU",
      "AT",
      "BE",
      "BR",
      "BG",
      "CA",
      "CY",
      "CZ",
      "DK",
      "EE",
      "FI",
      "FR",
      "DE",
      "GR",
      "HK",
      "HU",
      "IN",
      "IE",
      "IT",
      "JP",
      "LV",
      "LT",
      "LU",
      "MT",
      "MX",
      "NL",
      "NZ",
      "NO",
      "PL",
      "PT",
      "RO",
      "SG",
      "SK",
      "SI",
      "ES",
      "SE",
      "CH",
      "GB",
      "US",
    ];

    const checkIfCustomerExist = await accountModel.findOne({
      agentID: user._id,
    });

    let customer;
    let url;
    if (!checkIfCustomerExist) {
      if (!listOfSupportedCountries.includes(country.toUpperCase())) {
        throw new BadRequestError(
          "Vobb's Bank does not accept the country you intend to use"
        );
      }
      customer = await stripe.accounts.create({
        type: "express",
        email: user.email,
        country,
        capabilities: {
          card_payments: { requested: true },
          transfers: { requested: true },
        },
        business_type: "company",
      });
      const accountLink = await stripe.accountLinks.create({
        account: customer.id,
        refresh_url: "http://localhost:8080/status",
        return_url: "http://localhost:8080/status",
        type: "account_onboarding",
      });

      if (customer) {
        // create customer in our database
        const newCustomer = new accountModel({
          customerID: customer.id,
          agentID: user._id,
        });
        await newCustomer.save();
        customer = newCustomer;
        url = accountLink.url;
      } else {
        throw new InternalServerError("something went wrong");
      }
    } else if (
      checkIfCustomerExist &&
      typeof checkIfCustomerExist.customerID === "string"
    ) {
      const verifyAgent = await stripe.accounts.retrieve(
        checkIfCustomerExist.customerID
      );
      if (verifyAgent) {
        customer = checkIfCustomerExist;
      }
    }

    return { customer, url };
  }

  async agentPayout(agent, invoiceID) {
    let receipt = [];
    const hasCreatedBillingInfo = await accountModel.findOne({
      agentID: agent._id,
    });
    if (
      !hasCreatedBillingInfo ||
      (hasCreatedBillingInfo && !hasCreatedBillingInfo.customerID)
    )
      throw new UnauthorizedError(
        "You are not authorized to make payments yet. please completed setup on billing page"
      );
    const invoice = await invoiceModel
      .findById(invoiceID)
      .populate({ path: "commissionList.reciever", model: "Agent" })
      .populate({
        path: "agentID",
        model: "Agent",
      });

    if (!invoice) throw new InvalidError("Invoice with Id does not exist");

    for (let eachCommission of invoice.commissionList) {
      const reciever = await UserModel.findById(eachCommission.reciever);

      let recieverAccount = await accountModel.findOne({
        agentID: reciever._id,
      });
      if (
        !recieverAccount ||
        (recieverAccount && !recieverAccount.customerID)
      ) {
        throw new UnauthorizedError(
          `${reciever.name} is not authorized to receive money. Inform user to setup their account details.`
        );
      }

      let priceID;

      const product = await stripe.products.create({
        name: "Application commissions",
      });
      if (product) {
        const price = await stripe.prices.create({
          unit_amount:
            eachCommission.price * 100 + eachCommission.vobbAmount * 100,
          currency: "usd",
          product: product.id,
        });
        if (price) {
          priceID = price.id;
        }
      }
      const session = await stripe.checkout.sessions.create({
        line_items: [
          {
            price: priceID,
            quantity: 1,
          },
        ],
        mode: "payment",
        success_url: "https://example.com/success",
        cancel_url: "https://example.com/failure",
        payment_intent_data: {
          application_fee_amount: eachCommission.vobbAmount * 100,
          transfer_data: {
            destination: recieverAccount.customerID,
          },
        },
      });

      if (!session)
        throw new ForbiddenError(
          "Sessions for this Invoice could not be generated"
        );
      if (session) {
        // first find the billing history for this invoice
        const billHistory = await BillingHistoryModel.findOne({
          agentID: agent._id,
          invoiceNumber: invoiceID,
        });
        if (billHistory) {
          billHistory.sessionArray.push({ sessionID: session.id });
          await billHistory.save();
        } else {
          const saveBilling = new BillingHistoryModel({
            paymentID: session.id,
            sessionArray: [{ sessionID: session.id }],
            amountPaid: eachCommission.price,
            recievingAgent: eachCommission.reciever,
            invoiceNumber: invoiceID,
            agentID: agent._id,
          });

          await saveBilling.save();
          invoice.totalPeopleToPay += 1;
        }

        receipt.push({ receipt: session.id });
      }
    }
    invoice.modeOfPayment = "card";
    await invoice.save();

    return receipt;
  }

  async payoutBankTransfer(agent, invoiceID, file) {
    let receipt;

    const invoice = await invoiceModel.findById(invoiceID);
    if (!invoice)
      throw new BadRequestError("Missing required valid invoice number");

    const uploadedFile = await uploadToCloud(file);
    if (!uploadedFile) throw new ForbiddenError("Could");

    invoice.paid = true;
    invoice.modeOfPayment = "transfer";
    await invoice.save();

    let saveBilling;

    for (let eachCommission of invoice.commissionList) {
      // creating billing history for the invoice
      saveBilling = new BillingHistoryModel({
        tranferReceipt: { receipt: uploadedFile.secure_url },
        recievingAgent: eachCommission.reciever,
        amountPaid: eachCommission.price,
        invoiceNUmber: invoiceID,
        modeOfPayment: "transfer",
        agentID: agent._id,
      });
      await saveBilling.save();
    }

    receipt = uploadedFile.secure_url;

    return receipt;
  }

  async senderBillHistory(agent, param, type = false) {
    let { pageNo, noOfBills } = param;
    pageNo = pageNo ? Number(pageNo) : 1;
    noOfBills = noOfBills ? Number(noOfBills) : 10;

    let fetchBills, availablePages;
    const listOfBills = await BillingHistoryModel.find({
      agentID: agent._id,
      paid: type,
    });

    fetchBills = await BillingHistoryModel.find({
      agentID: agent._id,
      paid: type,
    })
      .populate({
        path: "invoiceNumber",
        model: "Invoice",
      })
      .populate({
        path: "recievingAgent",
        model: "Agent",
      })
      .sort({ createdAt: -1 })
      .limit(noOfBills)
      .skip((pageNo - 1) * noOfBills);

    let count = listOfBills.length;
    availablePages = Math.ceil(count / noOfBills);

    return {availablePages, fetchBills}
  }

  async RecieverBillHistory(agent, param, type = false) {
    let { pageNo, noOfBills } = param;
    pageNo = pageNo ? Number(pageNo) : 1;
    noOfBills = noOfBills ? Number(noOfBills) : 10;

    let fetchBills, availablePages;
    const listOfBills = await BillingHistoryModel.find({
      recievingAgent: agent._id,
      paid: type,
    });

    fetchBills = await BillingHistoryModel.find({
      recievingAgent: agent._id,
      paid: type,
    })
      .populate({
        path: "invoiceNumber",
        model: "Invoice",
      })
      .populate({
        path: "recievingAgent",
        model: "Agent",
      })
      .sort({ createdAt: -1 })
      .limit(noOfBills)
      .skip((pageNo - 1) * noOfBills);

    let count = listOfBills.length;
    availablePages = Math.ceil(count / noOfBills);

    return {availablePages, fetchBills}
  }
}

module.exports = new StripeConnect();

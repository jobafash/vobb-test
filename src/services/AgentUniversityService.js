const agentUniversityModel = require("./../models/AgentUniversityModel");

class AgentUniversity {
    async getAllUniversitiesForRegistration() {
    let neededUni = [];
    const universities = await agentUniversityModel.find({});

    for (let eachUni of universities) {
      neededUni.push(eachUni);
    }
    const key = "country"
    const arrayUniqueByKey = [...new Map(neededUni.map(item =>
      [item[key], item])).values()];

    return arrayUniqueByKey;
  }
}

module.exports = new AgentUniversity();

const { NotFoundError } = require("../../lib/appErrors");
const supportModel = require("./../models/SupportModel")

class ContactSupport {
    async supportMessage(data) {
        const newSupportMessage = new supportModel(data);

        await newSupportMessage.save();

        return newSupportMessage
    }

    async getMessages(params) {
        let { pageNo, noOfmessages } = params;
        pageNo = pageNo ? Number(pageNo) : 1;
        noOfmessages = noOfmessages ? Number(noOfmessages) : 10;
        let fetchedMessages;
        let count;
        let availablePages;

        let messageList = await supportModel.find({ attended: false });
        count = messageList.length;
        availablePages = Math.ceil(count / noOfmessages);
        fetchedMessages = await supportModel
            .find({ attended: false })
            .limit(noOfmessages)
            .skip((pageNo - 1) * noOfmessages);
        const res = { pageNo, availablePages, fetchedMessages, count };
        return res;
    }

    async getMessage(id) {
        const message = await supportModel.findById(id);
        if (!message) throw NotFoundError('Message not found. ');
        return message;
    }

    async updateMessage(id) {
        const message = await supportModel.findById(id);
        if (!message) throw NotFoundError('Message not found. ');
        message.attended = true;
        await message.save();
        return message;
    }
}

module.exports = new ContactSupport();
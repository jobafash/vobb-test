const serviceLocator = require('./lib/service-locator');
const turboLogger = require('turbo-logger').createStream({});
const AdminController = require('./src/controllers/AdminController');
const AdminService = require('./src/services/admin/AdminVobbServices');
const AppController = require('./src/controllers/applications/index');
const AppService = require('./src/services/application/index');
const PartnershipController = require('./src/controllers/partnerships/index');
const PartnershipService = require('./src/services/partnership/index');
const CommissionController = require('./src/controllers/commissions/index');
const CommissionService = require('./src/services/commission/index');

serviceLocator.register('logger', () => {
    return turboLogger;
});

serviceLocator.register('adminService',  () => {
    let logger = serviceLocator.get('logger');

    return new AdminService(logger);
});


serviceLocator.register('adminController', () => {
    let adminService = serviceLocator.get('adminService');
    let logger = serviceLocator.get('logger');

   return new AdminController(logger, adminService);
});

serviceLocator.register('applicationService',  () => {
    let logger = serviceLocator.get('logger');

    return new AppService(logger);
});


serviceLocator.register('applicationController', () => {
    let applicationService = serviceLocator.get('applicationService');
    let logger = serviceLocator.get('logger');

   return new AppController(logger, applicationService);
});

serviceLocator.register('partnershipService',  () => {
    let logger = serviceLocator.get('logger');

    return new PartnershipService(logger);
});


serviceLocator.register('partnershipController', () => {
    let partnershipService = serviceLocator.get('partnershipService');
    let logger = serviceLocator.get('logger');

   return new PartnershipController(logger, partnershipService);
});

serviceLocator.register('commissionService',  () => {
    let logger = serviceLocator.get('logger');

    return new CommissionService(logger);
});


serviceLocator.register('commissionController', () => {
    let commissionService = serviceLocator.get('commissionService');
    let logger = serviceLocator.get('logger');

   return new CommissionController(logger, commissionService);
});

module.exports = serviceLocator;
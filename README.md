# Vobb-api

### INSTALLATION - New(Skip steps 1 - 3 if you already have a copy of the repo)

Step 1: Clone the repository

Step 2: cd into the cloned folder | <code>cd vobb-api </code>

Step 3: git remote add upstream https://github.com/Vobb-llc/vobb-api.git

Step 4: git pull upstream main

Step 5: Check out to the task branch | <code>git checkout -b <NAME_OF_THE_TASK></code>

<code>e.g git checkout -b implemented_notification_api</code>


### Vobb API - Running the project locally

Step 1: npm install

Step 2: Copy env.example to .env and add required variables

Step 3: npm start

Step 4: http://localhost:8080 is the BASE_URL

A welcome message will come up with status code 200

#### Creating a pull request
Run <code>git branch</code> It should show that you are on your current branch

After implementing your task

Step 1: Run: git add .

Step 2: Run: git commit -m "< COMMIT MESSAGE >"

Step 3: git pull upstream main

Step 4: git push origin < BRANCH_NAME >

Go to the repository https://github.com/Vobb-llc/vobb-api

As soon as you get there, you are going to see a green ‘compare and create a pull request’

Click on it, and type your message, click on create pull request.

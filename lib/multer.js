const multer = require("multer");
const fs = require("fs");
const { BadRequestError } = require("./appErrors");
//adjust how files are stored
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    let dir = process.cwd();
    console.log(
      "This is the directory:..........\n...................... ",
      dir
    );
    //Sets destination for fileType
    if (
      file.mimetype === "image/jpeg" ||
      file.mimetype === "image/png" ||
      file.mimetype === "image/jpg"
    ) {
      dir = dir + `/uploads/images`;
    } else {
      dir = dir + `/uploads/pdfs`;
    }

    fs.mkdir(dir, { recursive: true }, (err) => cb(err, dir));
  },
  filename: function (req, file, callback) {
    callback(null, Date.now() + "_" + file.originalname);
  },
});

const fileFilter = function (req, file, callback) {
  if (
    file.mimetype === "image/jpeg" ||
    file.mimetype === "image/png" ||
    file.mimetype === "application/pdf" ||
    file.mimetype === "application/msword" ||
    file.mimetype ===
      "application/vnd.openxmlformats-officedocument.wordprocessingml.document" ||
    file.mimetype ===
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ||
    file.mimetype === "text/csv" ||
    file.mimetype === "application/vnd.ms-excel" ||
    file.mimetype === "application/octet-stream"
  ) {
    callback(null, true);
  } else {
    callback(
      new BadRequestError(
        "Error uploading file, Must either be Jpgs, docx,doc, png or csv"
      ),
      false
    );
  }
};

const fileSize = function () {
  const size = 1024 * 1024 * 15;
  if (
    file.mimetype === "application/pdf" ||
    file.mimetype === "application/msword"
  ) {
    size = 1024 * 1024 * 250;
    return size;
  } else return size;
};

exports.upload = multer({
  storage: storage,
  limits: {
    fileSize: fileSize,
  },
  fileFilter: fileFilter,
});

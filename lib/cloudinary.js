const cloudinary = require("cloudinary").v2;
const { BadRequestError } = require("./appErrors");

cloudinary.config({
	cloud_name: process.env.CLOUDINARY_NAME,
	api_key: process.env.CLOUDINARY_API_KEY,
	api_secret: process.env.CLOUDINARY_API_SECRET,
	secure: true,
});

exports.uploadToCloud = function (filename) {
	const upload = cloudinary.uploader.upload(
		filename,
		{ resource_type: "raw" },
		function (error, result) {
			if (error) throw new BadRequestError("Invalid file Path");
			return result;
		}
	);

	return upload;
};

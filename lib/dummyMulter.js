const multer = require("multer");

const upload = multer({
    limits: {
        fileSize: 30000000
    },

    fileFilter(req, file, cb) {
        console.log(file)
        if(!file.originalname.match(/\.(jpg|jpeg|png)$/gm)) {
            cb(new Error('please insert a valid image format'))
        }
        
        cb(undefined, true);
    }
})

module.exports = {
    upload
}
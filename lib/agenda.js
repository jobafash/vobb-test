const Agenda = require("agenda");

const connectionOpts = {
	db: { address: process.env.DB_URI, collection: "agendaJobs" },
	defaultConcurrency: 1,
};

const agenda = new Agenda(connectionOpts);

agenda.defaultConcurrency(1);

require("./../src/jobs/agentUploads")(agenda);

// if there are jobs in the jobsTypes array set up
agenda.on("ready", async () => {
	await agenda.start();
	await agenda.cancel({ nextRunAt: null });
});

agenda.start(); // Returns a promise, which should be handled appropriately

let graceful = () => {
	agenda.stop(() => process.exit(0));
};

process.on("SIGTERM", graceful);
process.on("SIGINT", graceful);

module.exports = agenda;
